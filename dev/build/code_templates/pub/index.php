<?php

// FIXME: should be done using phing replacement or dropped altogether so we can use this file in production, too
// We use strict error reporting for development
error_reporting(E_ALL | E_STRICT);

// +---------------------------------------------------------------------------+
// | An absolute filesystem path to the agavi/agavi.php script.                |
// +---------------------------------------------------------------------------+

require(__DIR__ . '/../app/init/before_bootstrap.php');

// +---------------------------------------------------------------------------+
// | Initialize the framework. You may pass an environment name to this method.|
// | By default the 'development' environment sets Agavi into a debug mode.    |
// | In debug mode among other things the cache is cleaned on every request.   |
// +---------------------------------------------------------------------------+
Agavi::bootstrap('%%PUBLIC_ENVIRONMENT%%');

require(__DIR__ . '/../app/init/after_bootstrap.php');


// +---------------------------------------------------------------------------+
// | Call the controller's dispatch method on the default context              |
// +---------------------------------------------------------------------------+
AgaviConfig::set('core.default_context', 'web');
AgaviContext::getInstance('web')->getController()->dispatch();

?>