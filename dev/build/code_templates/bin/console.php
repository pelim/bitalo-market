<?php

// FIXME: should be done using phing replacement or dropped altogether so we can use this file in production, too
// We use strict error reporting for development
error_reporting(E_ALL | E_STRICT);

require(__DIR__ . '/../libs/autoload.php');

require(__DIR__ . '/../app/config.php');

Agavi::bootstrap('%%PUBLIC_ENVIRONMENT%%');

AgaviConfig::set('core.default_context', 'console');

AgaviContext::getInstance('console')->getController()->dispatch();