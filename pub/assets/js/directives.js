'use strict';


angular.module('app')

	.directive('ngThumb', ['$window', function($window) {
		var helper = {
			support: !!($window.FileReader && $window.CanvasRenderingContext2D),
			isFile: function(item) {
				return angular.isObject(item) && item instanceof $window.File;
			},
			isImage: function(file) {
				var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
				return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
			}
		};

		return {
			restrict: 'A',
			template: '<canvas/>',
			link: function(scope, element, attributes) {
				if (!helper.support) return;

				var params = scope.$eval(attributes.ngThumb);

				if (!helper.isFile(params.file)) return;
				if (!helper.isImage(params.file)) return;

				var canvas = element.find('canvas');
				var reader = new FileReader();

				reader.onload = onLoadFile;
				reader.readAsDataURL(params.file);

				function onLoadFile(event) {
					var img = new Image();
					img.onload = onLoadImage;
					img.src = event.target.result;
				}

				function onLoadImage() {
					var width = params.width || this.width / this.height * params.height;
					var height = params.height || this.height / this.width * params.width;
					canvas.attr({ width: width, height: height });
					canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
				}
			}
		};
	}])

	.directive('ngMatch', function() {
		return {
			require: 'ngModel',
			link: function (scope, elem, attrs, ctrl) {
				scope.$watch('[' + attrs.ngModel + ', ' + attrs.ngMatch + ']', function(value){
					ctrl.$setValidity('match', value[0] === value[1] );
				}, true);

			}
		}
	})

	.directive('ngServerValidation', function() {
		return {
			restrict: 'A',
			require: '?ngModel',
			link: function(scope, element, attrs, ctrl) {
				element.on('change', function() {

					ctrl.$setValidity(attrs.ngServerValidation, true);
				})
			}
		}
	})

	.directive('ngSpinner', function () {
		return {
			restrict: 'A',
			transclude: true,
			replace: true,
			require: 'ngModel',

			template: '<span class="ui-spinner ui-widget ui-widget-content ui-corner-all">' +
							'<input ng-transclude ng-model="bindModel" required="true" type="number" name="{{name}}" value="{{value}}" class="input-lg spinner form-control ui-spinner-input"  autocomplete="off" >' +
							'<button type="button" class="ui-spinner-button ui-spinner-up ui-corner-tr" tabindex="-1" ng-disabled="value >= max" ng-click="incrementValue(1)">' +
								'<span class="ui-icon ui-icon-triangle-1-n">▲</span>' +
							'</button>' +
							'<button  type="button" class="ui-spinner-button ui-spinner-down ui-corner-br" tabindex="-1" ng-disabled="value <= min" ng-click="incrementValue(-1)">' +
								'<span class="ui-icon ui-icon-triangle-1-s">▼</span>' +
							'</button>' +
						'</span>',
			scope: {
				bindModel:'=ngModel',
				max: '=',
				min: '=',
				name: '@'
			},
			link: function(scope, element, attrs, ngModel) {
				if(!ngModel) return; // do nothing if no ng-model
				scope.$watch('bindModel', function(value) {

					scope.value = scope.bindModel;
					ngModel.$setViewValue(scope.value);
					ngModel.$render();

					if(scope.bindModel > scope.max) {
						ngModel.$setValidity('max', false);
					} else {
						ngModel.$setValidity('max', true);
					}

					if(scope.bindModel < scope.min) {
						ngModel.$setValidity('min', false);
					} else {
						ngModel.$setValidity('min', true);
					}
				});

				ngModel.$parsers.push(function(viewValue){

					return parseInt(viewValue) ? parseInt(viewValue) : undefined;
				});

				scope.value = scope.bindModel;
				ngModel.$setViewValue(scope.value);
				ngModel.$render();

				scope.incrementValue = function(i) {
					scope.value += i;
					ngModel.$setViewValue(scope.value);
					ngModel.$render();
				}

			}
		}
	});