'use strict';


var controllers = angular.module('bitaloControllers', ['ngSanitize', 'ngMessages', 'ui.bootstrap', 'ui.select', 'nya.bootstrap.select', 'angularFileUpload', 'infinite-scroll', 'bitaloServices']);

controllers.controller('BaseController', ['$scope', function($scope) {
	$scope.settings = BITALO_MARKET_ATTRIBUTES;
}]);


controllers.controller('HeaderController', ['$scope', function($scope) {
	$scope.categories = BITALO_MARKET_ATTRIBUTES.categories;

	$scope.sortOptions = BITALO_MARKET_ATTRIBUTES.sortOptions;

	$scope.categegory = undefined;

	$scope.sortOption = 0;
}]);

controllers.controller('AccountController', ['$scope', '$http', function($scope, $http) {

	$scope.account = {};

	$scope.errorFields = {};

	$scope.completed = false;

	$scope.passwordMeter = {

		colors: ['danger', 'info', 'warning', 'success'],

		measureStrength: function (p) {

			var _force = 0;
			var _regex = /[$-/:-?{-~!"^_`\[\]]/g;

			var _lowerLetters = /[a-z]+/.test(p);
			var _upperLetters = /[A-Z]+/.test(p);
			var _numbers = /[0-9]+/.test(p);
			var _symbols = _regex.test(p);

			var _flags = [_lowerLetters, _upperLetters, _numbers, _symbols];
			var _passedMatches = $.grep(_flags, function (el) { return el === true; }).length;

			_force += 2 * p.length + ((p.length >= 10) ? 1 : 0);
			_force += _passedMatches * 20;

			// penality (short password)
			_force = (p.length <= 6) ? Math.min(_force, 10) : _force;

			// penality (poor variety of characters)
			_force = (_passedMatches == 1) ? Math.min(_force, 10) : _force;
			_force = (_passedMatches == 2) ? Math.min(_force, 20) : _force;
			_force = (_passedMatches == 3) ? Math.min(_force, 40) : _force;

			return _force;

		},
		getColor: function (s) {

			var idx = 0;
			if (s <= 10) { idx = 0; }
			else if (s <= 20) { idx = 1; }
			else if (s <= 30) { idx = 1; }
			else if (s <= 40) { idx = 2; }
				else { idx = 3; }

			return { idx: idx + 1, col: this.colors[idx] };

		}
	};

	$scope.validate = function() {
		console.log($scope);
	};

	$scope.register = function($event) {

		$event.preventDefault();

		$scope.submitted = true;

		if($scope.registerForm.$valid) {
			var BIP32 = bitcore.BIP32;

			$scope.account.memomic =  (new bitcore.BIP39).generateMnemonic(256);

			var randomBytes =  (new bitcore.BIP39).mnemonicToSeed($scope.account.memomic )

			var bip32 = BIP32.seed(randomBytes, $scope.$parent.settings.network);

			$scope.account.masterKey =  CryptoJS.AES.encrypt(bip32.extendedPrivateKeyString(), $scope.account.password).toString();

			$scope.account.publicKey = bip32.extendedPublicKeyString();

			$http({
				method  : $event.target.method,
				url     : $event.target.action,
				data    : $scope.account
			}).success(function(data) {

				window.localStorage.setItem('wallet.master_key', $scope.account.masterKey);
				window.localStorage.setItem('wallet.public_key', $scope.account.publicKey);

				$scope.next = data.next;

				$scope.completed = true;

			}).error(function(data) {
				if(data.errors) {
					_.each(data.errors,function(type, field) {

						$scope.registerForm[field].$setValidity(type, false)
					});
				}
			});
		}


	};

	$scope.$watch('account.password', function(password) {
		if(password !== undefined && password) {
			$scope.passwordMeter.display = true;

			var strength = $scope.passwordMeter.measureStrength(password);
			$scope.passwordMeter.class =  'progress-bar-' + $scope.passwordMeter.getColor(strength).col;
			$scope.passwordMeter.width = (strength) + '%';
		} else {
			$scope.passwordMeter.display = false;

		}

	});

	$scope.login = function($event) {
		$event.preventDefault();

		$http({
			method  : $event.target.method,
			url     : $event.target.action,
			data    : $scope.account
		}).success(function(data) {

			window.localStorage.setItem('wallet.master_key', data.master_key);
			window.localStorage.setItem('wallet.public_key', data.public_key);

			top.location = data.redirect;

		}).error(function(data) {
			if(data.errors) {
				_.each(data.errors,function(message, field) {
					$scope.errorFields[field] = true;
				});
			}
		});
	};
}]);

controllers.controller('PurchaseController', ['$scope', '$http', function($scope, $http) {

	$scope.purchase = {};

	$scope.errorFields = {};

	$scope.countries = $scope.$parent.settings.locale.countries;

	$scope.item = $scope.$parent.settings.item;

	$scope.purchase = $scope.$parent.settings.purchase;

	$scope.currency = 'usd';


	$scope.$watch('purchase.quantity', function() {
		console.log($scope);
	});

	$scope.purchaseItem = function($event) {

		$event.preventDefault();

		$scope.errorFields = {};

		if($scope.purchase.shipping) {
			$scope.purchase.shipping.option = $scope.purchase.shipping.id;
		}

		$http({
			method  : $event.target.method,
			url     : $event.target.action,
			data    : $scope.purchase
		}).success(function(data) {
			top.location = data.redirect;

		}).error(function(data) {
			if(data.errors) {
				_.each(data.errors,function(message, field) {
					$scope.errorFields[field] = true;
				});
			}

		});
	};

}]);


controllers.controller('PasswordDialogController', ['$scope', '$modalInstance', function($scope, $modalInstance) {

	$scope.masterKey = window.localStorage.getItem('wallet.master_key');

	$scope.errorFields = {};

	$scope.ok = function () {
		if($scope.password) {
			var decryptedMasterKey = CryptoJS.AES.decrypt($scope.masterKey, $scope.password);
			var BIP32 = bitcore.BIP32;
			var bip32 = new BIP32(decryptedMasterKey.toString(CryptoJS.enc.Utf8), BITALO_MARKET_ATTRIBUTES.network);

			if(bip32.extendedPublicKeyString() == window.localStorage.getItem('wallet.public_key')) {
				$modalInstance.close(bip32);
			} else {
				$scope.errorFields['password'] = true;
			}
		} else {
			$scope.errorFields['password'] = true;

		}

	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
}]);

controllers.controller('ProcessController', ['$scope', '$http', '$modal', function($scope, $http, $modal) {

	$scope.process = {};

	if(typeof $scope.$parent.settings.transaction != 'undefined') {
		$scope.transaction = bitcore.TransactionBuilder.fromObj($scope.$parent.settings.transaction);
		if(typeof $scope.$parent.settings.script != 'undefined') {
			var hashToScriptMap = {};
			hashToScriptMap[$scope.$parent.settings.script.address] = $scope.$parent.settings.script.script;
			$scope.transaction.setHashToScriptMap(hashToScriptMap);
		}
	}


	$scope.signAsShipped = function($event) {

		$event.preventDefault();

		var modalInstance = $modal.open({
			templateUrl: 'processPasswordDialog.html',
			controller: PasswordDialogController,
			size: 'sm'
		});

		modalInstance.result.then(function (bip32) {

			var walletKey = new bitcore.WalletKey({network: bitcore.networks[BITALO_MARKET_ATTRIBUTES.network]});

			walletKey.fromObj({priv: bip32.derive('m/0/' + $scope.$parent.settings.script.derive).eckey.private.toString('hex')});

			console.log(walletKey.storeObj());

			$scope.transaction.sign([walletKey.storeObj().priv]);

			$scope.transaction.build();


			$http({
				method  : 'post',
				url     : $event.target.href,
				data    : {
					output_transaction: JSON.stringify($scope.transaction.toObj()),
					fully_signed: $scope.transaction.isFullySigned(),
					identifier: 'process.shipping'
				}
			}).success(function(data) {

				top.location = data.redirect;

			}).error(function(data) {


			});

		}, function () {

		});

	}


	$scope.signAsReceived = function($event) {

		$event.preventDefault();

		var modalInstance = $modal.open({
			templateUrl: 'processPasswordDialog.html',
			controller: PasswordDialogController,
			size: 'sm'
		});

		modalInstance.result.then(function (bip32) {

			var walletKey = new bitcore.WalletKey({network: bitcore.networks[$scope.$parent.settings.network]});

			walletKey.fromObj({priv: bip32.derive('m/0/' + $scope.$parent.settings.script.derive).eckey.private.toString('hex')});

			$scope.transaction.sign([walletKey.storeObj().priv]);

			$scope.transaction.build();

			$http({
				method  : 'post',
				url     : $event.target.href,
				data    : {
					output_transaction: JSON.stringify($scope.transaction.toObj()),
					fully_signed: $scope.transaction.isFullySigned(),
					identifier: 'process.received'
				}

			}).success(function(data) {

				top.location = data.redirect;

			}).error(function(data) {


			});

		}, function () {

		});

	}
}]);

controllers.controller('ConversationDialogController' ['$scope', '$modalInstance', 'item', 'ConversationService', function($scope, $modalInstance, item, ConversationService) {

	$scope.errorFields = {};

	$scope.item = item;

	$scope.send = function() {
		if($scope.message) {
			var conversation = new ConversationService;
			conversation.message = {
				body: $scope.message
			};
			conversation.user = {
				id: $scope.item.user.id
			};
			conversation.$save();
			$modalInstance.close(conversation);
		} else {
			$scope.errorFields['message'] = true;
		}
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
}]);


controllers.controller('ConversationsController', ['$scope', '$http', 'ConversationService', 'MessageService', function($scope, $http, ConversationService, MessageService) {

	$scope.conversations = ConversationService.query();

	$scope.index = 0;

	$scope.conversation = {
		index: 0
	};

	$scope.show = function(index) {
		$scope.conversation.index = index;
		$scope.index = index;

	}

	$scope.create = function() {
		var conversation = new ConversationService;
		conversation.message.body = $scope.message.body;
		conversation.user.id      = $scope.message.user.id;
		conversation.$save();
	}

	$scope.reply = function() {

		var id = $scope.conversations[$scope.index].id;
		var message = new MessageService({conversation: id});
		message.body = $scope.message;
		message.$save(function(data) {
			$scope.conversations[$scope.index].messages.push(data);
		});
	}
}]);



controllers.controller('ItemsController', ['$scope', '$http', 'ItemService', function($scope, $http, ItemService) {

	$scope.items = [];

	$scope.page = {
		current: 1,
		last: 0,
		loading: false
	}


	$scope.loading = false;

	$scope.noMoreItems = false;

	$scope.order = $scope.$$prevSibling.sortOptions[0].order;

	$scope.sort = $scope.$$prevSibling.sortOptions[0].sort;

	$scope.category = null;

	$scope.loadItems = function() {
		if($scope.page.loading || $scope.page.last == $scope.page.current) {
			return false;
		} else {
			$scope.page.last = $scope.page.current;
			$scope.page.loading  = true;
		}

		ItemService.query({
			page: $scope.page.current,
			category: $scope.category,
			sort: $scope.sort,
			order: $scope.order
		}, function(data) {
			if(data.length) {
				$scope.items = $scope.items.concat(data);
				$scope.page.current++;
			} else {
				console.log('end');
			}
			$scope.page.loading  = false;

		}, function(data) {
			console.log(data);
		});
	}

	$scope.nextPage = function() {
		$scope.loadItems();
	}

	$scope.$watch('$$prevSibling.category', function(category) {
		$scope.category = category;
		$scope.page.current = 1;
		$scope.page.last = 0;
		$scope.items = [];
		$scope.nextPage();
	});

	$scope.$watch('$$prevSibling.sortOption', function(sortOption) {
		$scope.order = $scope.$$prevSibling.sortOptions[sortOption].order;
		$scope.sort = $scope.$$prevSibling.sortOptions[sortOption].sort;
		$scope.page.current = 1;
		$scope.page.last = 0;
		$scope.items = [];

		$scope.nextPage();
	});
}]);

controllers.controller('ItemController', ['$scope', '$modal', function($scope, $modal) {

	$scope.item = $scope.$parent.settings.item;

	$scope.quantity = $scope.item.quantity.min;

	$scope.currency = 'usd';

	$scope.getPrice = function(currency) {
		return $scope.quantity * $scope.item.price[currency];
	}

	$scope.createConversation = function() {
		var modalInstance = $modal.open({
			templateUrl: 'createConversationDialog.html',
			controller: ConversationDialogController,
			size: 'sm',
			resolve: {
				item: function () {
					return $scope.item;
				}
			}
		});

		modalInstance.result.then(function(conversation) {
			console.log(conversation);
		});
	}

}]);

controllers.controller('ItemCreateController', ['$scope', '$http', '$upload', 'ItemService', function($scope, $http, $upload, ItemService) {

	if(!$scope.$parent.settings.item) {
		$scope.item = {
			currency: 'EUR',
			shippingOptions: [],
			images: {
				active: [],
				deleted: [],
				new: []
			}
		};

		$scope.images = [];

	} else {

		$scope.item = $scope.$parent.settings.item;

		$scope.images = [];
	}

	$scope.steps = ['create', 'shipping', 'pictures', 'review'];

	$scope.step = $scope.steps[0];

	$scope.shippingOptions = [];

	$scope.shippingOption = {};

	$scope.currencies = BITALO_MARKET_ATTRIBUTES.currencies;

	$scope.categories = BITALO_MARKET_ATTRIBUTES.categories;


	$scope.onFileSelect = function($files) {
		$scope.item.images.new = $scope.item.images.new.concat($files);
	}

	$scope.removeFile = function(index, existing) {
		if(existing) {
			$scope.item.images.deleted = $scope.item.images.deleted.concat($scope.item.images.active.splice(index, 1));

		} else {
			$scope.item.images.new.splice(index, 1);
		}
	}

	$scope.save = function() {
		var newImages = $scope.item.images.new;

		$scope.upload = $upload.upload({
			url: BITALO_MARKET_ATTRIBUTES.routes.item,
			method: 'POST',
			data: {item: $scope.item},
			file: newImages,
			fileFormDataName: 'images[new]',

			formDataAppender: function(formData, key, val) {
				formData.append(key, new Blob([JSON.stringify(val)], {type:'application/json'}));
				return formData;
			}
		}).progress(function(evt) {

		}).success(function(data, status, headers, config) {
			// file is uploaded successfully
			top.location = data.redirect;
		});
	}


	$scope.$watch('images', function() {
		if($scope.$$childTail && $scope.$$childTail.imageForm) {
			if(!$scope.shippingOptions.length) {
				$scope.$$childTail.imageForm.image.$setValidity('required', true);
			} else {
				$scope.$$childTail.imageForm.image.$setValidity('required', false);
			}
		}
	}, true);


	$scope.addShippingOption = function(form) {
		console.log($scope);

		$scope.item.shippingOptions.push({
			description: $scope.shippingOption.description,
			price: $scope.shippingOption.price
		});

		$scope.shippingOption = {};
	}

	$scope.removeShippingOption = function(index) {
		$scope.item.shippingOptions.splice(index, 1);
	}


	$scope.next = function(form) {
		$scope.submitted = true;

		if(!form || (form && form.$valid)) {
			var pos = $scope.steps.indexOf($scope.step);
			$scope.step = $scope.steps[++pos];
			$scope.submitted = false;
		}
	}

	$scope.previous = function($event) {
		var pos = $scope.steps.indexOf($scope.step);
		$scope.step = $scope.steps[--pos];
	}

}]);