'use strict';

/* Services */

var services = angular.module('bitaloServices', ['ngResource']);

services.factory('ItemService', function($resource) {
	return $resource(
		'/api/items/:item',
		{item: '@id' }
	);
});

services.factory('ConversationService', function($resource) {
	return $resource(
		'/api/conversations/:conversation',
		{
			conversation: '@conversation'
		}
	);
});

services.factory('MessageService', function($resource) {
	return $resource(
		'/api/conversations/:conversation/messages/:message',
		{
			conversation: '@conversation',
			message: '@message'
		}
	);
});




