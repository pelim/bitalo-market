<?php

/**
 *  Payment PubSub Worker
 */

require(__DIR__ . '/../libs/autoload.php');

require(__DIR__ . '/../app/config.php');

// read in agavi environment to bootstrap
if(isset($argv[1])) {
	$environment = $argv[1];
} else {
	throw new InvalidArgumentException('Please provide an environment to bootstrap as first argument!');
}

Agavi::bootstrap($environment);

AgaviConfig::set('core.default_context', 'console-redis');

$sub = AgaviContext::getInstance('console-redis')->getDatabaseConnection('subscribe'); /** @var $sub \Predis\Client */
$pub = AgaviContext::getInstance('console-redis')->getDatabaseConnection('publish'); /** @var $pub \Predis\Client */

$logger = AgaviContext::getInstance('console-worker')->getLoggerManager();

$subscriber = $sub->pubSubLoop();

$subscriber->subscribe('address', 'block', 'confirm');

AgaviContext::getInstance('console-redis')->getLoggerManager()->log('pupbsub worker start', AgaviILogger::INFO);

foreach($subscriber as $message) {

	if($message->channel == 'address' && $message->kind == Predis\PubSub\Consumer::MESSAGE) {
		$subscriber->subscribe($message->payload);
		$logger->log(sprintf("subscribe [%s]",  $message->payload), AgaviILogger::INFO);
	} elseif($message->channel == 'block' && $message->kind == Predis\PubSub\Consumer::MESSAGE) {
		$taskManager = AgaviContext::getInstance('console-redis')->getModel('TaskManager'); /** @var $taskManager \TaskManagerModel */
		$taskManager->schedule('process.block.confirmation', array('blockHash' => $message->payload));
	} elseif($message->kind == Predis\PubSub\Consumer::MESSAGE) {
		$pub->set($message->payload, $message->channel);
		$logger->log(sprintf("publish [%s] => %s",  $message->channel, $message->payload), AgaviILogger::INFO);
	}
}

unset($subscriber);

echo PHP_EOL;

