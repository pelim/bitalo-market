<?php

/**
 *  Task Worker
 *
 */

require(__DIR__ . '/../libs/autoload.php');

require(__DIR__ . '/../app/config.php');

// read in agavi environment to bootstrap
if(isset($argv[1])) {
	$environment = $argv[1];
} else {
	throw new InvalidArgumentException('Please provide an environment to bootstrap as first argument!');
}

AgaviConfig::set('core.default_context', 'console-worker');

Agavi::bootstrap($environment);

require(AgaviConfigCache::checkConfig(AgaviConfig::get('core.config_dir') . '/custom/settings.xml'));

class_alias('Bitalo\Market\Agavi\Config\Config', 'BitaloConfig');

$queue = AgaviContext::getInstance()->getDatabaseConnection('subscribe'); /** @var $sub \Predis\Client */

AgaviContext::getInstance()->getLoggerManager()->log('task worker start', AgaviILogger::INFO);

while(true) {


	$taskItem = $queue->blpop('queue.priority.high'
		, 'queue.priority.normal'
		, 'queue.priority.low'
		, 10);

	if($taskItem) {
		$taskDetails = json_decode($taskItem[1], true);

		$task = AgaviContext::getInstance()->getModel($taskDetails['model']);

		try {
			AgaviContext::getInstance('console-worker')->getLoggerManager()->log(sprintf("%s [%s]", $taskDetails['model'], print_r($taskDetails['arguments'], true)), AgaviILogger::INFO);

			$task->perform($taskDetails['arguments']);

		} catch(Exception $e) {
			AgaviContext::getInstance('console-worker')->getLoggerManager()->log($e->getMessage() . $e->getTraceAsString(), AgaviILogger::ERROR);
		}
	}
}

unset($queue);

echo PHP_EOL;

