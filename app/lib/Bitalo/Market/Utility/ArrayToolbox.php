<?php

/**
 * Array Helper Functions
 *
 * @author Thomas Bachem
 */

namespace Bitalo\Market\Utility;

use InvalidArgumentException;

class ArrayToolbox {

	/**
	 * Determines whether the given array is associative or sequential.
	 *
	 * NOTE: Treats sparse arrays as non-associative arrays.
	 *
	 * Examples:
	 *    array(0, 1, 2)  FALSE
	 *    array(0, 2)     FALSE
	 *    array(0, 'x')   TRUE
	 *
	 * @link http://stackoverflow.com/questions/173400/php-arrays-a-good-way-to-check-if-an-array-is-associative-or-sequential/5969617#5969617
	 * @param array $arr
	 * @return bool
	 */
	public static function isAssoc(array $arr) {
		for(reset($arr); is_int(key($arr)); next($arr));
		return is_null(key($arr));
	}

	/**
	 * Searches for the given array value and insert a new element before.
	 *
	 * @author bk at quicknet dot nl
	 * @link http://de.php.net/manual/de/function.array-push.php#56479
	 *
	 * @param array $src
	 * @param mixed $in
	 * @param mixed $pos
	 * @return array
	 */
	public static function pushBefore(array $src, $in, $pos) {
		$in = array($in);

		if(is_int($pos)) {
			$R = array_merge(array_slice($src, 0, $pos), $in, array_slice($src, $pos));
		} else {
			foreach($src as $k => $v) {
				if($k == $pos) $R = array_merge($R, $in);
				$R[$k] = $v;
			}
		}

		return $R;
	}


	/**
	 * Searches for the given array value and insert a new element after.
	 *
	 * @author bk at quicknet dot nl
	 * @link http://de.php.net/manual/de/function.array-push.php#56479
	 *
	 * @param array $src
	 * @param mixed $in
	 * @param int $pos
	 * @return array
	 */
	public static function pushAfter($src, $in, $pos){
		$in = array($in);

		if(is_int($pos)) {
			$R = array_merge(array_slice($src, 0, $pos + 1), $in, array_slice($src, $pos + 1));
		} else {
			foreach($src as $k => $v) {
				$R[$k] = $v;
				if($k == $pos) $R = array_merge($R, $in);
			}
		}

		return $R;
	}


	/**
	 * array_multisort() for the column of a two-dimensional array
	 *
	 * @author php a-t-the-r-a-t-e chir.ag
	 * @see array_multisort()
	 * @link http://de.php.net/manual/de/function.array-multisort.php#60401
	 * @return array
	 */
	public static function multisortColumn() {
		$n  = func_num_args();
		$ar = func_get_arg($n - 1);
		if(!is_array($ar)) return false;

		for($i = 0; $i < $n - 1; $i++) {
			$col[$i] = func_get_arg($i);
		}

		foreach($ar as $key => $val) {
			foreach($col as $kkey => $vval) {
				if(is_string($vval)) {
					${'subar' . $kkey}[$key] = $val[$vval];
				}
			}
		}

		$arv = array();
		$arv[] =& $ar;
		foreach($col as $key => $val) {
			$arv[] = (is_string($val) ? ${'subar' . $key} : $val);
		}


		call_user_func_array('array_multisort', $arv);
		return $ar;
	}


	/**
	 * array_sum() for the column of a two-dimensional array
	 *
	 * @see array_sum()
	 * @param array $arr
	 * @param mixed $key
	 * @return int
	 */
	public static function getColumnSum(array $arr, $key) {
		if(!is_array($arr)) return false;

		foreach($arr as $elem) {
			$sum += $elem[$key];
		}

		return $sum;
	}


	/**
	 * Recursive array_change_key_case() for multidimensional arrays
	 *
	 * @see array_change_key_case()
	 * @param array $input
	 * @param int $case
	 * @return array
	 */
	public static function changeKeyCaseRecursive(array $input, $case = CASE_LOWER){
		foreach($input as $key => $elem) {
			if($case == CASE_UPPER) {
				$key = strtoupper($key);
			} else {
				$key = strtolower($key);
			}
			if(is_array($elem)) {
				$out[$key] = array_change_key_case_recursive($elem, $case);
			} else {
				$out[$key] = $elem;
			}
		}

		return $out;
	}


	/**
	 * Extracts a column out of a multidimensional array
	 *
	 * @param array $arr
	 * @param mixed $key
	 * @return array
	 */
	public static function extractColumn(array $arr, $key) {
		$newArr = array();
		foreach($arr as $element) {
			$newArr[] = $element[$key];
		}

		return $newArr;
	}

	public static function extractIdColumn(array $arr, $key) {
		$newArr = array();
		foreach($arr as $element) {
			array_push($newArr, (int) $element[$key]);
		}

		return $newArr;
	}


	/**
	 * Compares each array entry with an instanceof expression
	 * and returns the array without all dismatches. Preserves
	 * keys.
	 *
	 * @param array $arr
	 * @param string $className
	 * @param bool $invert
	 * @return array
	 */
	public static function filterByInstanceof(array $arr, $className, $invert = false) {
		if(!$invert) {
			foreach($arr as $key => $value) {
				if(!($value instanceof $className)) {
					unset($arr[$key]);
				}
			}
		} else {
			foreach($arr as $key => $value) {
				if($value instanceof $className) {
					unset($arr[$key]);
				}
			}
		}

		return $arr;
	}


	/**
	 * Removes all elements that contain the given value in the
	 * given column.
	 *
	 * @param string $column
	 * @param mixed $needle
	 * @param array $haystack
	 * @param bool $strict
	 * @return array
	 */
	public static function filterInColumn($column, $needle, array $haystack, $strict = false) {
		if($strict) {
			foreach($haystack as $key => $value) {
				if($value[$column] === $needle) {
					unset($haystack[$key]);
				}
			}
		} else {
			foreach($haystack as $key => $value) {
				if($value[$column] == $needle) {
					unset($haystack[$key]);
				}
			}
		}

		return $haystack;
	}


	/**
	 * in_array() for array keys
	 *
	 * @see in_array()
	 * @param mixed $needle
	 * @param array $hackstack
	 * @param bool strict
	 * @return bool
	 */
	public static function containsKey($needle, array $haystack, $strict = false) {
		if($strict) {
			foreach($haystack as $key => $value) {
				if($key === $needle) {
					return true;
				}
			}
		} else {
			foreach($haystack as $key => $value) {
				if($key == $needle) {
					return true;
				}
			}
		}

		return false;
	}


	/**
	 * Like array_search(), but for a given column. Returns the
	 * index of the matching row.
	 *
	 * @param mixed $needle
	 * @param array $haystack
	 * @param mixed $column
	 * @param bool strict
	 * @return int|false
	 */
	public static function searchInColumn($needle, $column, array $haystack, $strict = false) {
		if($strict) {
			foreach($haystack as $i => $element) {
				if($element[$column] === $needle) {
					return $i;
				}
			}
		} else {
			foreach($haystack as $i => $element) {
				if($element[$column] == $needle) {
					return $i;
				}
			}
		}

		return false;
	}

	/**
	 * Returns whether the given array contains the given value
	 * in the given column.
	 *
	 * @param mixed $needle
	 * @param array $haystack
	 * @param mixed $column
	 * @param bool strict
	 * @return bool
	 */
	public static function containsValueInColumn($needle, $column, array $haystack, $strict = false) {
		return (static::searchInColumn($needle, $column, $haystack, $strict) !== false);
	}

	/**
	 * Like array_unique(), but decides whether an array element is unique
	 * based on the values of a certain column.
	 *
	 * @param string $column
	 * @param array $haystack
	 * @return array
	 */
	public static function removeDuplicatesInColumn($column, array $haystack) {
		$columnValues     = self::extractColumn($haystack, $column);
		$uniqueValueFound = array_flip(array_unique($columnValues));

		$newArray = array();
		foreach($haystack as $key => $value) {
			$columnValue = $value[$column];
			if($uniqueValueFound[$columnValue] !== true) {
				$newArray[$key] = $value;
				$uniqueValueFound[$columnValue] = true;
			}
		}

		return $newArray;
	}


	/**
	 * Recursive implode() for multidimensional arrays
	 *
	 * @author gregrahkin
	 * @see implode()
	 * @link http://de.php.net/manual/de/function.implode.php#33217
	 * @param mixed $glue
	 * @param array $pieces
	 * @return array
	 */
	public static function implodeRecursive($glue, array $pieces){
		$out = '';
		foreach($pieces as $piece) {
			if(is_array($piece)) {
				$out .= self::implodeRecursive($glue, $piece);
			} else {
				$out .= $glue . $piece;
			}
		}

		return $out;
	}


	/**
	 * Like preg_grep(), but checks array keys instead of values.
	 *
	 * @param string $pattern
	 * @param array $input
	 * @param int $flags PREG_GREP_INVERT or none
	 * @return array
	 */
	public static function pregGrepKey($pattern, array $input, $flags = null) {
		$filteredArray = array();

		if($flags & PREG_GREP_INVERT) {
			foreach($input as $key => $value) {
				if(!preg_match($pattern, $key)) {
					$filteredArray[$key] = $value;
				}
			}
		} else {
			foreach($input as $key => $value) {
				if(preg_match($pattern, $key)) {
					$filteredArray[$key] = $value;
				}
			}
		}

		return $filteredArray;
	}

	/**
	 * @link http://php.net/manual/en/function.array-unique.php#95203
	 * @param array $arr
	 * @param bool $preserveKeys
	 * @return array
	 */
	public static function findDuplicates(array $arr, $preserveKeys = false) {
		$duplicates = array_unique(array_diff_assoc($arr, array_unique($arr)));
		return $preserveKeys ? $duplicates : array_values($duplicates);
	}

	public static function merge() {
		$inputArrays = func_get_args();
		$result = array();
		foreach($inputArrays as $array) {
			foreach($array as $key => $value) {
				$result[$key] = $value;
			}
		}

		return $result;
	}
	/**
	 * Blends two or more arrays into each other, alterning their values.
	 * Accepts an arbitrary number of arrays as input and an optional last
	 * boolean argument stating whether to preserve keys or not (default: false).
	 *
	 * @link http://www.php.net/manual/en/function.array-merge.php#98025
	 * @return array
	 */
	public static function zip() {
		$preserveKeys = false;
		$inputArrays = func_get_args();
		if(is_bool($inputArrays[count($inputArrays) - 1])) {
			$preserveKeys = array_pop($inputArrays);
		}

		if(count($inputArrays) <= 1) {
			throw new InvalidArgumentException('Please provide at least to input arrays!');
		}

		$output = array();

		if($preserveKeys) {
			$inputKeys = array();
			$lastCount = -1;
			foreach($inputArrays as $inputArray) {
				if($lastCount != -1 && count($inputArray) != $lastCount) {
					throw new InvalidArgumentException('All arrays must share the same length!');
				}
				$lastCount = count($inputArray);

				$inputKeys[] = array_keys($inputArray);
			}

			for($i = 0; $i < count($inputArray); $i++) {
				foreach($inputArrays as $j => $inputArray) {
					$output[$inputKeys[$j][$i]] = $inputArray[$inputKeys[$j][$i]];
				}
			}
		} else {
			$maxCount = 0;
			foreach($inputArrays as &$inputArray) {
				$inputArray = array_values($inputArray);
				$count = count($inputArray);
				if($count > $maxCount) {
					$maxCount = $count;
				}
			}
			unset($inputArray);

			for($i = 0; $i < $maxCount; $i++) {
				foreach($inputArrays as $inputArray) {
					if(array_key_exists($i, $inputArray)) {
						$output[] = $inputArray[$i];
					}
				}
			}
		}

		return $output;
	}

}

?>
