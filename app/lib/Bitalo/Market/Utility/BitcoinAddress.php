<?php

namespace Bitalo\Market\Utility;

/**
 * Class BitcoinAddress
 *
 * @author:
 * @url: http://pastebin.com/vmRQC7ha
 */
class BitcoinAddress {

	const ADDRESS_VERSION = "00";


	public static function decodeHex($hex) {
		$hex = strtoupper($hex);
		$chars = "0123456789ABCDEF";
		$return = "0";
		for($i = 0; $i < strlen($hex); $i++) {
			$current = (string)strpos($chars,$hex[$i]);
			$return = (string)bcmul($return,"16",0);
			$return = (string)bcadd($return,$current,0);
		}
		return $return;
	}

	public static function encodeHex($dec) {
		$chars = "0123456789ABCDEF";
		$return = "";
		while(bccomp($dec,0) == 1) {
			$dv = (string)bcdiv($dec,"16",0);
			$rem = (integer)bcmod($dec,"16");
			$dec = $dv;
			$return = $return . $chars[$rem];
		}
		return strrev($return);
	}

	public static function decodeBase58($base58) {
		$origbase58 = $base58;

		$chars = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
		$return = "0";
		for($i = 0; $i < strlen($base58); $i++) {
			$current = (string)strpos($chars,$base58[$i]);
			$return = (string)bcmul($return,"58",0);
			$return = (string)bcadd($return,$current,0);
		}

		$return = self::encodeHex($return);

		//leading zeros
		for($i = 0; $i < strlen($origbase58) && $origbase58[$i] == "1"; $i++) {
			$return = "00" . $return;
		}

		if(strlen($return) % 2 != 0) {
			$return = "0" . $return;
		}

		return $return;
	}

	public static function encodeBase58($hex) {
		if(strlen($hex) % 2 != 0) {
			die("encodeBase58: uneven number of hex characters");
		}
		$orighex = $hex;

		$chars = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
		$hex = self::decodeHex($hex);
		$return = "";
		while(bccomp($hex,0) == 1) {
			$dv = (string)bcdiv($hex,"58",0);
			$rem = (integer)bcmod($hex,"58");
			$hex = $dv;
			$return = $return . $chars[$rem];
		}
		$return = strrev($return);

		//leading zeros
		for($i = 0; $i < strlen($orighex) && substr($orighex,$i,2) == "00"; $i += 2) {
			$return = "1" . $return;
		}

		return $return;
	}

	public static function hash160ToAddress($hash160,$addressversion = self::ADDRESS_VERSION) {
		$hash160 = $addressversion . $hash160;
		$check = pack("H*",$hash160);
		$check = hash("sha256",hash("sha256",$check,true));
		$check = substr($check,0,8);
		$hash160 = strtoupper($hash160 . $check);
		return self::encodeBase58($hash160);
	}

	public static function addressToHash160($addr) {
		$addr = self::decodeBase58($addr);
		$addr = substr($addr,2,strlen($addr) - 10);
		return $addr;
	}

	public static function checkAddress($addr,$addressversion = self::ADDRESS_VERSION) {
		$addr = self::decodeBase58($addr);
		if(strlen($addr) != 50) {
			return false;
		}
		$version = substr($addr,0,2);
		if(hexdec($version) > hexdec($addressversion)) {
			return false;
		}
		$check = substr($addr,0,strlen($addr) - 8);
		$check = pack("H*",$check);
		$check = strtoupper(hash("sha256",hash("sha256",$check,true)));
		$check = substr($check,0,8);
		return $check == substr($addr,strlen($addr) - 8);
	}

	public static function hash160($data) {
		$data = pack("H*",$data);
		return strtoupper(hash("ripemd160",hash("sha256",$data,true)));
	}

	public static function pubKeyToAddress($pubkey) {
		return self::hash160ToAddress(hash160($pubkey));
	}

	public static function remove0x($string) {
		if(substr($string,0,2) == "0x" || substr($string,0,2) == "0X") {
			$string = substr($string,2);
		}
		return $string;
	}

}
