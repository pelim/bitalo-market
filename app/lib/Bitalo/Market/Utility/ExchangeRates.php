<?php

namespace Bitalo\Market\Utility;

class ExchangeRates {

	const EXCHANGE_RATES_CACHE_TIME = 300; // 60 * 5

	const EXCHANGE_RATES_CACHE_KEY = 'payment:currency:exchange';


	public static function updateExchangeRates() {

		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, 'http://blockchain.info/ticker');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$result = (array) json_decode(curl_exec($curl));

		$redis = \AgaviContext::getInstance()->getDatabaseConnection('redis');


		foreach($result as $currency => $rates) {
			$redis->hmset(self::EXCHANGE_RATES_CACHE_KEY, strtolower($currency), $rates->last);
		}

		$redis->expire(self::EXCHANGE_RATES_CACHE_KEY, self::EXCHANGE_RATES_CACHE_TIME);


		return $result;
	}


	public static function getExchangeRate($currency) {
		$redis = \AgaviContext::getInstance()->getDatabaseConnection('redis');
		if(!$redis->exists(self::EXCHANGE_RATES_CACHE_KEY)) {
			self::updateExchangeRates();
		}
		return $redis->hget(self::EXCHANGE_RATES_CACHE_KEY, strtolower($currency));

	}

}