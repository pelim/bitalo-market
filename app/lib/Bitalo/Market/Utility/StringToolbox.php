<?php

/**
 * Helper Functions for Strings
 *
 * @author Thomas Bachem
 */

namespace Bitalo\Market\Utility;

class StringToolbox {

	/**
	 * Static functions only
	 */
	private function __construct() { }


	/**
	 * UTF-8 compatible wordwrap()
	 *
	 * @link http://framework.zend.com/apidoc/core/Zend_Text/Zend_Text_MultiByte.html#wordWrap
	 *
	 * @param string $string
	 * @param int $width
	 * @param string  $break
	 * @param bool $cut
	 * @return string
	 */
	public static function wordwrapUtf8($string, $width = 75, $break = "\n", $cut = false) {
		$charset = 'UTF-8';

		$result     = array();
		$breakWidth = iconv_strlen($break, $charset);

		while(($stringLength = iconv_strlen($string, $charset)) > 0) {
			$breakPos = iconv_strpos($string, $break, 0, $charset);

			if($breakPos !== false && $breakPos < $width) {
				if($breakPos === $stringLength - $breakWidth) {
					$subString = $string;
					$cutLength = null;
				} else {
					$subString = iconv_substr($string, 0, $breakPos, $charset);
					$cutLength = $breakPos + $breakWidth;
				}
			} else {
				$subString = iconv_substr($string, 0, $width, $charset);

				if($subString === $string) {
					$cutLength = null;
				} else {
					$nextChar = iconv_substr($string, $width, 1, $charset);

					if($breakWidth === 1) {
						$nextBreak = $nextChar;
					} else {
						$nextBreak = iconv_substr($string, $breakWidth, 1, $charset);
					}

					if($nextChar === ' ' || $nextBreak === $break) {
						$afterNextChar = iconv_substr($string, $width + 1, 1, $charset);

						if($afterNextChar === false) {
							$subString .= $nextChar;
						}

						$cutLength = iconv_strlen($subString, $charset) + 1;
					} else {
						$spacePos = iconv_strrpos($subString, ' ', $charset);

						if($spacePos !== false) {
							$subString = iconv_substr($subString, 0, $spacePos, $charset);
							$cutLength = $spacePos + 1;
						} else if($cut === false) {
							$spacePos = iconv_strpos($string, ' ', 0, $charset);

							if($spacePos !== false) {
								$subString = iconv_substr($string, 0, $spacePos, $charset);
								$cutLength = $spacePos + 1;
							} else {
								$subString = $string;
								$cutLength = null;
							}
						} else {
							$subString = iconv_substr($subString, 0, $width, $charset);
							$cutLength = $width;
						}
					}
				}
			}

			$result[] = $subString;

			if($cutLength !== null) {
				$string = iconv_substr($string, $cutLength, $stringLength - $cutLength, $charset);
			} else {
				break;
			}
		}

		return implode($break, $result);
	}


	/**
	 * UTF-8 compatible ucfirst()
	 *
	 * @see ucfirst()
	 * @param string $str
	 * @return string
	 */
	public static function ucfirstUtf8($str) {
		return mb_strtoupper(mb_substr($str, 0, 1)) . mb_strtolower(mb_substr($str, 1, mb_strlen($str)));
	}


	/**
	 * UTF-8 compatible ucwords()
	 *
	 * @see ucwords()
	 * @param string $str
	 * @return string
	 */
	public static function ucwordsUtf8($str) {
		// "mb_convert_case($str, MB_CASE_TITLE)" behaves differently than ucwords()
		// as it forces all non-capital letters to be lowercase, so we use our own
		// implementation
		// (note: "\b" doesn't work properly in Unicode mode, see http://stackoverflow.com/questions/2432868/php-regex-word-boundary-matching-in-utf-8/2449017#2449017)
		return preg_replace_callback('/(?<![\pL\pN])(\p{L})/u', function($m) {
			return mb_strtoupper($m[1]);
		}, $str);
	}


	/**
	 * Replaces all \n and \r breaks with spaces
	 *
	 * @param string $text
	 * @return string
	 */
	public static function stripBreaks($text) {
		$text = str_replace("\n", ' ', $text);
		$text = str_replace("\r", '', $text);

		return $text;
	}


	/**
	 * Generates a random string out of the given char pool.
	 *
	 * @param int $length
	 * @param string $pool
	 * @return string
	 */
	public static function randomize($length, $pool = 'ABCDEFGHIJKLMNOPQRSTRUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') {
		$str = '';
		for($i = 0; $i < $length; $i++) {
			$str .= substr($pool, mt_rand(0, mb_strlen($pool) - 1), 1);
		}

		return $str;
	}


	/**
	 * Reversed strrchr()
	 *
	 * @author sekati at gmail dot com
	 * @link http://de.php.net/manual/de/function.strrchr.php#64157
	 *
	 * @param string $haystack
	 * @param string $needle
	 * @param bool $include_needle
	 * @return string
	 */
	public static function strrchrReverse($haystack, $needle, $includeNeedle = true) {
		if($includeNeedle) {
			return substr($haystack, 0, strrpos($haystack, $needle) + strlen($needle));
		} else {
			return substr($haystack, 0, strrpos($haystack, $needle));
		}
	}



	/**
	 * Checks if $haystack contains any of the strings in $needles
	 *
	 * @param string $haystack
	 * @param string|array $needles
	 * @return bool
	 */
	public static function contains($haystack, $needles) {
		foreach((array)$needles as $needle) {
			if(strpos($haystack, $needle) === false) {
				return false;
			}
		}

		return true;
	}


	/**
	 * Deletes all breaks and tabs and insert spaces where appropriate.
	 *
	 * @param string $text
	 * @return string
	 */
	public static function flatten(&$text) {
		$newText = str_replace("\r", '', $text);
		$newText = preg_replace('#( |\n|\t)+#', ' ', $newText);

		return $newText;
	}


	/**
	 * Reversed preg_quote()
	 *
	 * @param string $str
	 * @return string
	 */
	public static function pregUnquote($str) {
		$search  = array("\\.", "\\\\", "\\+", "\\*", "\\?", "\\[", "\\^", "\\]", '\\$', "\\(", "\\)", "\\{", "\\}", "\\=", "\\!", "\\<", "\\>", "\\|", "\\:");
		$replace = array(".", "\\", "+", "*", "?", "[", "^", "]", '$', "(", ")", "{", "}", "=", "!", "<", ">", "|", ":");
		return str_replace($search, $replace, $str);
	}


	/**
	 * Converts a string from Camelcase format (fooBarBaz) into
	 * underscore format (foo_bar_baz)
	 *
	 * @param string
	 * @return string
	 */
	public static function convertCamelcaseToUnderscore($string) {
		$string = preg_replace('/(?<!^)([A-Z])/', '_$1', $string);
		return strtolower($string);
	}


	/**
	 * Converts a string from underscore format (foo_bar_baz) into
	 * Camel case format (fooBarBaz)
	 *
	 * @param string
	 * @return string
	 */
	public static function convertUnderscoreToCamelcase($string) {
		return preg_replace_callback('/(?<!^)(_[a-z])/', function($value) { return strtoupper($value[1][1]); }, $string);
	}


	/**
	 * Converts a string from Camelcase format (fooBarBaz) into
	 * dash format (foo-bar-baz)
	 *
	 * @param string
	 * @return string
	 */
	public static function convertCamelcaseToDash($string) {
		$string = preg_replace('/(?<!^)([A-Z])/', '-$1', $string);
		return strtolower($string);
	}


	/**
	 * Converts a string from dash format (foo-bar-baz) into
	 * Camel case format (fooBarBaz)
	 *
	 * @param string
	 * @return string
	 */
	public static function convertDashToCamelcase($string) {
		return preg_replace_callback('/(?<!^)(-[a-z])/', function($value) { return strtoupper($value[1][1]); }, $string);
	}

	/**
	 * @param $string
	 *
	 * @return mixed
	 */
	public static function convertDotToCamelcase($string) {
		return  str_replace(' ', '', ucwords(str_replace('.', ' ', $string)));
	}

	/**
	 * Converts the given string to UTF-8. If the string is already valid UTF-8 no processing
	 * is done, otherwise it tries to convert the string from $assumedInputEncoding to UTF-8.
	 *
	 * @param string $string
	 * @param string $assumedInputEncoding
	 * @return string
	 */
	public static function makeValidUtf8($string, $assumedInputEncoding = 'ISO-8859-1') {
		$expectedEncoding = 'UTF-8';
		if(!mb_check_encoding($string, $expectedEncoding)) {
			$string = iconv($assumedInputEncoding, $expectedEncoding, $string);
		}

		return $string;
	}


	/**
	 * Takes an UTF-8 string and returns an array of ints representing the
	 * Unicode characters. Astral planes are supported ie. the ints in the
	 * output can be > 0xFFFF. Occurrances of the BOM are ignored. Surrogates
	 * are not allowed.
	 *
	 * Returns false if the input string isn't a valid UTF-8 octet sequence.
	 *
	 * See http://iki.fi/hsivonen/php-utf8/
	 *
	 * @param $str string
	 * @return array
	 */
	public static function convertUtf8ToUnicode($str) {
		$mState = 0; // cached expected number of octets after the current octet
		// until the beginning of the next UTF8 character sequence
		$mUcs4  = 0; // cached Unicode character
		$mBytes = 1; // cached expected number of octets in the current sequence

		$out = array();

		$len = strlen($str);
		for($i = 0; $i < $len; $i++) {
			$in = ord($str{$i});
			if(0 == $mState) {
				// When mState is zero we expect either a US-ASCII character or a
				// multi-octet sequence.
				if(0 == (0x80 & ($in))) {
					// US-ASCII, pass straight through.
					$out[] = $in;
					$mBytes = 1;
				} elseif(0xC0 == (0xE0 & ($in))) {
					// First octet of 2 octet sequence
					$mUcs4 = ($in);
					$mUcs4 = ($mUcs4 & 0x1F) << 6;
					$mState = 1;
					$mBytes = 2;
				} elseif(0xE0 == (0xF0 & ($in))) {
					// First octet of 3 octet sequence
					$mUcs4 = ($in);
					$mUcs4 = ($mUcs4 & 0x0F) << 12;
					$mState = 2;
					$mBytes = 3;
				} elseif(0xF0 == (0xF8 & ($in))) {
					// First octet of 4 octet sequence
					$mUcs4 = ($in);
					$mUcs4 = ($mUcs4 & 0x07) << 18;
					$mState = 3;
					$mBytes = 4;
				} elseif(0xF8 == (0xFC & ($in))) {
					/* First octet of 5 octet sequence.
					 *
					 * This is illegal because the encoded codepoint must be either
					 * (a) not the shortest form or
					 * (b) outside the Unicode range of 0-0x10FFFF.
					 * Rather than trying to resynchronize, we will carry on until the end
					 * of the sequence and let the later error handling code catch it.
					 */
					$mUcs4 = ($in);
					$mUcs4 = ($mUcs4 & 0x03) << 24;
					$mState = 4;
					$mBytes = 5;
				} elseif(0xFC == (0xFE & ($in))) {
					// First octet of 6 octet sequence, see comments for 5 octet sequence.
					$mUcs4 = ($in);
					$mUcs4 = ($mUcs4 & 1) << 30;
					$mState = 5;
					$mBytes = 6;
				} else {
					/* Current octet is neither in the US-ASCII range nor a legal first
					 * octet of a multi-octet sequence.
					 */
					return false;
				}
			} else {
				// When mState is non-zero, we expect a continuation of the multi-octet
				// sequence
				if(0x80 == (0xC0 & ($in))) {
					// Legal continuation.
					$shift = ($mState - 1) * 6;
					$tmp = $in;
					$tmp = ($tmp & 0x0000003F) << $shift;
					$mUcs4 |= $tmp;

					if(0 == --$mState) {
						/* End of the multi-octet sequence. mUcs4 now contains the final
						 * Unicode codepoint to be output
						 *
						 * Check for illegal sequences and codepoints.
						 */
						// From Unicode 3.1, non-shortest form is illegal
						if(((2 == $mBytes) && ($mUcs4 < 0x0080)) ||
							((3 == $mBytes) && ($mUcs4 < 0x0800)) ||
							((4 == $mBytes) && ($mUcs4 < 0x10000)) ||
							(4 < $mBytes) ||
							// From Unicode 3.2, surrogate characters are illegal
							(($mUcs4 & 0xFFFFF800) == 0xD800) ||
							// Codepoints outside the Unicode range are illegal
							($mUcs4 > 0x10FFFF)) {
							return false;
						}
						if(0xFEFF != $mUcs4) {
							// BOM is legal but we don't want to output it
							$out[] = $mUcs4;
						}
						// initialize UTF8 cache
						$mState = 0;
						$mUcs4  = 0;
						$mBytes = 1;
					}
				} else {
					/* ((0xC0 & (*in) != 0x80) && (mState != 0))
					 *
					 * Incomplete multi-octet sequence.
					 */
					return false;
				}
			}
		}
		return $out;
	}


	/**
	 * Takes an array of ints representing the Unicode characters and returns
	 * a UTF-8 string. Astral planes are supported ie. the ints in the
	 * input can be > 0xFFFF. Occurrances of the BOM are ignored. Surrogates
	 * are not allowed.
	 *
	 * Returns false if the input array contains ints that represent
	 * surrogates or are outside the Unicode range.
	 *
	 * See http://iki.fi/hsivonen/php-utf8/
	 *
	 * @param $arr array
	 * @return string
	 */
	public static function convertUnicodeToUtf8(array $arr) {
		$dest = '';
		foreach($arr as $src) {
			if($src < 0) {
				return false;
			} elseif($src <= 0x007f) {
				$dest .= chr($src);
			} elseif($src <= 0x07ff) {
				$dest .= chr(0xc0 | ($src >> 6));
				$dest .= chr(0x80 | ($src & 0x003f));
			} elseif($src == 0xFEFF) {
				// nop -- zap the BOM
			} elseif($src >= 0xD800 && $src <= 0xDFFF) {
				// found a surrogate
				return false;
			} elseif($src <= 0xffff) {
				$dest .= chr(0xe0 | ($src >> 12));
				$dest .= chr(0x80 | (($src >> 6) & 0x003f));
				$dest .= chr(0x80 | ($src & 0x003f));
			} elseif($src <= 0x10ffff) {
				$dest .= chr(0xf0 | ($src >> 18));
				$dest .= chr(0x80 | (($src >> 12) & 0x3f));
				$dest .= chr(0x80 | (($src >> 6) & 0x3f));
				$dest .= chr(0x80 | ($src & 0x3f));
			} else {
				// out of range
				return false;
			}
		}
		return $dest;
	}

	/**
	 * Turns a (binary) string into hex representation (every byte is converted to its hex version)
	 *
	 * @param string $string
	 * @return string
	 */
	public static function toHex($str) {
		$out = '';
		for($i = 0, $c = strlen($str); $i < $c; ++$i) {
			$out .= str_pad(dechex(ord($str[$i])), 2, '0', STR_PAD_LEFT);
		}
		return $out;
	}

	/**
	 * Turns the hex representation of a string (created by toHex()) into the binary (original) representation
	 * @param string $encodedString
	 * @return string
	 */
	public static function fromHex($str) {
		$out = '';
		for($i = 0, $c = strlen($str) - 1; $i < $c; $i += 2) {
			$out .= chr(hexdec($str[$i] . $str[$i + 1]));
		}
		return $out;
	}

	/**
	 * Explodes the string and trims the resulting array's values
	 *
	 * @static
	 * @param string $str
	 * @param string $delimiter
	 * @return array
	 */
	public static function trimExplode($str, $delimiter = ',') {
		$arr = explode($delimiter, $str);
		return array_map(function($el) {return trim($el);}, $arr);
	}

	public static function destroyMultiWhitespaces($string){
		return preg_replace("/([ ]+[ ]{1})|([ ]{1}[ ]+)/"," ",$string);
	}

	public static function slugifyText($text) {
		
		// normalize
		$normalizeChars = array(
        'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A',
        'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
        'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
        'Û'=>'U', 'Ý'=>'Y', 'Þ'=>'B','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a',
        'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
        'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ø'=>'o', 'ù'=>'u',
        'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f', '/'=>' ', '_'=>' ', '('=>'',
        ')'=>'');

		$text = strtr($text, $normalizeChars);

		// lower chars
		$text = strtolower($text);

		// german special chars normalize
		$text = str_replace(array('ß', 'ä', 'ö', 'ü', '&', 'Ä', 'Ö', 'Ü'), array('ss', 'ae', 'oe', 'ue', '-und-', 'Ae', 'Oe', 'Ue'), $text);

		// transliterate
		if(function_exists('iconv')) {
			$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		}
		
		// destroy multi whitespaces
		$text = preg_replace('/\W+/', '-', $text);
		$text = str_replace('-', ' ', $text);
		$text = self::destroyMultiWhitespaces($text);
		$text = str_replace(' ', '-', trim($text));

		return $text;

	}

}

?>
