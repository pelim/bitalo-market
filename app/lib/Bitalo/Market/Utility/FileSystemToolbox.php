<?php

/**
 * Filesystem-related Helper Functions
 *
 * @author Thomas Bachem
 */

namespace Bitalo\Market\Utility;

use Exception;

class FileSystemToolbox {

	/**
	 * Will be used for faster MIME type matching based on
	 * file extension in determineMimeType().
	 */
	protected static $popularMimeTypes = array(
		'.png'  => 'image/png',
		'.jpg'  => 'image/jpeg',
		'.jpeg' => 'image/jpeg',
		'.gif'  => 'image/gif',
		'.svg'  => 'image/svg+xml',
	);


	/**
	 * Static functions only
	 */
	private function __construct() { }


	/**
	 * Executes the given command in background, without waiting
	 * for it to finish.
	 *
	 * @author Thomas Bachem
	 * @see exec()
	 *
	 * @param string $command
	 */
	public static function execAsync($command) {
		if(self::isWindows()) {
			pclose(popen('start /b ' . $command, 'r'));
		} else {
			exec($command . ' &> /dev/null & echo $!');
		}
	}

	protected static function isWindows() {
		return (substr(PHP_OS, 0, 3) == 'WIN');
	}

	public static function removeRecursive($pattern) {
		try {
			if(self::isWindows()) {
				exec(sprintf('del /s %s', $pattern));
			} else {
				exec(sprintf('rm -r %s', $pattern));
			}
			return true;
		} catch(Exception $e) {
			return false;
		}
	}


	/**
	 * Recursive scandir()
	 *
	 * @author malmsteenforce at tlen dot pl
	 * @see scandir()
	 * @link http://de.php.net/manual/de/function.scandir.php#68252
	 *
	 * @param string $path
	 * @param string $basepath
	 * @return ArrayIterator
	 */
	public static function scandirRecursive($path, $basepath = NULL) {
		if(!is_dir($path)) return false;

		// Stammpfad (erster Aufruf) merken
		if(!$basepath) $basepath = $path;

		$path_relative = str_replace($basepath, '', $path);
		$list = array();
		$directory = @opendir($path);

		while($file = @readdir($directory)) {
			if($file != '.' and $file != '..') {
				$f = $file;
				$f = preg_replace('/(\/){2,}/', '/', $f); // replace double slashes
				if(is_file($path . $f)) $list[] = $path_relative . $f;
				if(is_dir($path . $f)) {
					$list = array_merge($list, self::scandirRecursive($path . $f . '/', $basepath));
				}
			}
		}
		@closedir($directory);
		return $list;
	}

	public static function scandir($path) {
		if(!is_dir($path)) return false;
		$list = array();
		$directory = @opendir($path);

		while($file = @readdir($directory)) {
			if($file != '.' and $file != '..') {
				$f = $file;
				$f = preg_replace('/(\/){2,}/', '/', $f); // replace double slashes
				if(is_file($path . $f)) $list[] =  $f;
			}
		}
		@closedir($directory);
		return $list;
	}


	/**
	 * Creates all folders of the given path that do not exist yet.
	 * Made to handle concurrency issues by retrying multiple times.
	 *
	 * @param string $path
	 * @param int $mode
	 * @param int $retries
	 */
	public static function mkdirRecursive($path, $mode = 0777, $retries = 3) {
		$run = 0;
		do {
			$success = @mkdir($path, $mode, true);
		} while(++$run < $retries && $success && !file_exists($path) && (usleep(50000) || true));

		if(!file_exists($path)) {
			throw new Exception('Unable to create path "' . $path . '".');
		}
	}


	/**
	 * Derived from http://de.php.net/manual/de/function.realpath.php#97885
	 *
	 * @param string $path
	 * @param string $compareTo
	 * @return string
	 */
	public static function relativizePath($path, $compareTo) {
		// Clean arguments by removing trailing and prefixing slashes
		if(substr($path, -1) == '/') {
			$path = substr($path, 0, -1);
		}
		if(substr($path, 0, 1) == '/') {
			$path = substr($path, 1);
		}

		if(substr($compareTo, -1) == '/') {
			$compareTo = substr($compareTo, 0, -1);
		}
		if(substr($compareTo, 0, 1) == '/') {
			$compareTo = substr($compareTo, 1);
		}

		// Simple case: $compareTo is in $path
		if(strpos($path, $compareTo) === 0) {
			$offset = strlen($compareTo) + 1;
			return substr($path, $offset);
		}

		$relative  = array();
		$pathParts = explode('/', $path);
		$compareToParts = explode('/', $compareTo);

		foreach($compareToParts as $index => $part) {
			if(isset($pathParts[$index]) && $pathParts[$index] == $part) {
				continue;
			}

			$relative[] = '..';
		}

		foreach($pathParts as $index => $part) {
			if(isset($compareToParts[$index]) && $compareToParts[$index] == $part) {
				continue;
			}

			$relative[] = $part;
		}

		return implode('/', $relative);
	}

	/**
	 * @param string $path
	 * @return string|null
	 */
	public static function determineMimeType($path) {
		$mimeType = null;
		$fileExtension = strrchr($path, '.');
		if(isset(static::$popularMimeTypes[$fileExtension])) {
			$mimeType = static::$popularMimeTypes[$fileExtension];
		} elseif(extension_loaded('finfo')) {
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$mimeType = finfo_file($finfo, $path);
			finfo_close($finfo);
		}

		return $mimeType;
	}

	/**
	 * @param string $data
	 * @return string|null
	 */
	public static function determineMimeTypeByData($data) {
		$mimeType = null;
		if(extension_loaded('finfo')) {
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$mimeType = finfo_buffer($finfo, $data);
			finfo_close($finfo);
		}

		return $mimeType;
	}
}

?>
