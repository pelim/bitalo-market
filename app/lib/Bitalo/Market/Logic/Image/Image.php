<?php

namespace Bitalo\Market\Logic\Image;

use Bitalo\Market\Logic\Object\BasePersistenceObject;
use Bitalo\Market\Utility\FileSystemToolbox;
use Imagine\Image\ManipulatorInterface;
use Imagine\Imagick\Imagine;
use Imagine\Image\Box;
use AgaviUploadedFile;
use AgaviConfig;

class Image extends BasePersistenceObject {

	const ORIENTATION_PORTRAIT = 'portrait';

	const ORIENTATION_LANDSCAPE = 'landscape';

	const ORIENTATION_SQUARE = 'square';

	public function getEntityName() {
		return '\Bitalo\Market\Orm\Image';
	}

	/**
	 * @param AgaviUploadedFile $file
	 *
	 * @return Image
	 */
	public static function createFromUploadedImage(AgaviUploadedFile $file) {
		// generate resource name
		$resourceName = md5(uniqid(mt_rand(), true)) . strtolower(strrchr($file->getName(), '.'));

		$image = new self();
		$image->setResourceName($resourceName);

		$size = getimagesize($file->getTmpName());

		if($size && isset($size[0]) && isset($size[1])) {
			$image->setWidth($size[0]);
			$image->setHeight($size[1]);
		}

		$image->setMimeType($file->getType());
		$image->save();
		// move image to base path
		$file->move($image->getFilePath());

		return $image;
	}

	/**
	 * @param null $format
	 *
	 * @return string
	 */
	public function getFilePath($format = null) {
		if(!$format) {
			return AgaviConfig::get('core.app_dir') . '/../pub/contents/items/' . $this->getResourceName();
		} else {
			$pathInfo = pathinfo($this->getResourceName());

			return AgaviConfig::get('core.app_dir') . '/../pub/contents/items/' . $pathInfo['filename'] . '/'. $format . '.' . $pathInfo['extension'] ;
		}
	}

	/**
	 * @param null $format
	 *
	 * @return string
	 */
	public function getSrc($format = null)  {
		if(!$format) {
			return  '/contents/items/' . $this->getResourceName();
		} else {
			$pathInfo = pathinfo($this->getResourceName());
			return  '/contents/items/' . $pathInfo['filename'] . '/'. $format . '.' . $pathInfo['extension'] ;
		}
	}

	/**
	 * @param $width
	 * @param $height
	 *
	 * @return string
	 */
	public function getThumbnail($width, $height) {
		$imagine = new Imagine();
		$size    = new Box($width, $height);
		$format  = sprintf('%sx%s', $width, $height);
		$path    = $this->getFilePath($format);

		if(!file_exists($path)) {
			FileSystemToolbox::mkdirRecursive(dirname($path));
			// optimize thumbnail for square crop
			if($width == $height) {
				$orientation = $this->getOrientation();
				if($orientation == self::ORIENTATION_SQUARE || $orientation == self::ORIENTATION_PORTRAIT) {
					$mode = ManipulatorInterface::THUMBNAIL_OUTBOUND;
				} else {
					$mode = ManipulatorInterface::THUMBNAIL_INSET;
				}
			} else {
				$mode = ManipulatorInterface::THUMBNAIL_OUTBOUND;
			}
			$imagine->open($this->getFilePath())->thumbnail($size, $mode)->save($path);
		}

		return $this->getSrc($format);
	}

	/**
	 * @return string
	 */
	public function getOrientation() {
		if($this->getWidth() > $this->getHeight()) {
			return self::ORIENTATION_LANDSCAPE;
		} elseif($this->getWidth() < $this->getHeight()) {
			return self::ORIENTATION_PORTRAIT;
		} elseif($this->getWidth() == $this->getHeight()) {
			return self::ORIENTATION_SQUARE;
		}
	}
}