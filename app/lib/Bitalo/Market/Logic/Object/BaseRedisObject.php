<?php

namespace Bitalo\Market\Logic\Object;

use AgaviContext;

abstract class BaseRedisObject  {


	/**
	 * @param null|string $name
	 *
	 * @return \Predis\Client
	 */
	protected function getRedisConnection($name = 'redis') {

		return AgaviContext::getInstance()->getDatabaseConnection($name);
	}


	public abstract function getKeyPattern();

}