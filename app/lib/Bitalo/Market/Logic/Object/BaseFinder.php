<?php

namespace Bitalo\Market\Logic\Object;


abstract class BaseFinder {

	const DEFAULT_OFFSET = 0;

	const DEFAULT_LIMIT = 10;

	const SORT_ASC = 'ASC';

	const SORT_DESC = 'DESC';


	/**
	 * @param null $name
	 *
	 * @return \Doctrine\ORM\EntityManager
	 */
	protected static function getEntityManager($name = null) {
		return \AgaviContext::getInstance()->getDatabaseConnection($name);
	}

	/**
	 * @param null $name
	 *
	 * @return  \Doctrine\ORM\Query\Expr
	 */
	protected static function exp($name = null) {
		return self::getEntityManager($name)->getExpressionBuilder();
	}

	/**
	 * @param null $name
	 *
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	protected static function query($name = null) {
		return self::getEntityManager($name)->createQueryBuilder();
	}

	/**
	 * @param $array
	 * @param $wrapperClassName
	 *
	 * @return array
	 * @throws \InvalidArgumentException
	 */
	public static function wrapArrayValues($array, $wrapperClassName) {
		if(!is_array($array) && !($array instanceof \Traversable)) {
			throw new \InvalidArgumentException(sprintf('$array parameter needs to be either an array or implement the Traversable interface. "%s" was given', is_object($array) ? get_class($array) : gettype($array)));
		}
		$newArray = array();
		foreach($array as $key => $value) {
			$newArray[$key] = new $wrapperClassName($value);
		}
		return $newArray;
	}

}