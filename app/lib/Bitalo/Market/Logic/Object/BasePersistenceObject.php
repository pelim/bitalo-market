<?php

namespace Bitalo\Market\Logic\Object;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\UnitOfWork;
use AgaviContext;

abstract class BasePersistenceObject  {

	/**
	 * @var object
	 */
	protected $entity;

	protected $id;

	/**
	 * @return mixed
	 */
	abstract function getEntityName();


	public function __construct($entity = null) {
		$entityName= $this->getEntityName();

		if($entity instanceof $entityName) {
			$this->entity = $entity;
			$this->id = $entity->getId();

		} elseif($entity !== null) {
			$this->id = $entity;
			$this->entity = $this->getEntityManager()->getRepository($entityName)->find($entity);
		} else {
			$this->entity = new $entityName();
		}
	}

	/**
	 * @param bool $flush
	 *
	 * @return bool
	 */
	public function save($flush = true) {
		if($this->isNew()) {
			$this->getEntityManager()->persist($this->entity);
		}

		if($flush) {
			//try {
				$this->getEntityManager()->flush($this->entity);
				$this->id = $this->entity->getId();
				return true;
			//} catch(\Exception $e) {
			//	AgaviContext::getInstance()->getLoggerManager()->log($e->getMessage() . $e->getTraceAsString(), \AgaviILogger::ERROR);

			//	return false;
			//}
		}

	}

	/**
	 * @return bool
	 */
	public function delete() {
		$this->getEntityManager()->remove($this->entity);

		try {
			$this->getEntityManager()->flush($this->entity);
			return true;
		} catch(\Exception $e) {
			return false;
		}
	}

	/**
	 * @return bool
	 */
	protected function isNew() {
		return !($this->getEntityManager()->getUnitOfWork()->getEntityState($this->entity) == UnitOfWork::STATE_MANAGED);
	}

	/**
	 * @param null $name
	 *
	 * @return \Doctrine\ORM\EntityManager
	 */
	protected function getEntityManager($name = null) {
		return AgaviContext::getInstance()->getDatabaseConnection($name);
	}

	public function __call($name, array $arguments) {
		if(method_exists($this->entity, $name)) {
			foreach($arguments as &$argument) {
				if($argument instanceof self) {
					$argument = $argument->entity;
				}
			}

			return call_user_func_array(array( $this->entity, $name), $arguments);
		}
	}

	public function __sleep() {
		return array('id');
	}

	public function __wakeup() {
		$this->entity = $this->getEntityManager()->getRepository($this->getEntityName())->find($this->id);
	}

}
?>