<?php

namespace Bitalo\Market\Logic\Object;

interface BaseProcess  {

	const PROCESS_OWNER_APPLICATION = 'application';

	const PROCESS_OWNER_PURCHASER= 'purchaser';

	const PROCESS_OWNER_VENDOR = 'vendor';


	public function getProcessOwner();

	public function getNextProcessStep();

	public static function getProcessChain();
}