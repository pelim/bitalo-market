<?php

namespace Bitalo\Market\Logic\Object;

interface BaseNotification  {

	public function getNotificationKey();

	public function getNotificationType();

}