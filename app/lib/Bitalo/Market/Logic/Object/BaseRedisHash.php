<?php

namespace Bitalo\Market\Logic\Object;

use AgaviContext;

abstract class BaseRedisHash extends BaseRedisObject  {

	protected $id;

	public function __construct($id) {
		$this->id = $id;
	}

	public function getId() {
		return $this->id;
	}

	public function getKey() {
		return sprintf($this->getKeyPattern(), $this->id);
	}

	public function __set($name, $value) {
		$this->getRedisConnection()->hset($this->getKey(), $name, $value);
	}

	public function __get($name) {
		return $this->getRedisConnection()->hget($this->getKey(), $name);
	}

}