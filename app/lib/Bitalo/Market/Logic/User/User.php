<?php

namespace Bitalo\Market\Logic\User;

use AgaviContext;
use Bitalo\Market\Logic\Object\BasePersistenceObject;
use Bitalo\Market\Logic\Payment\PaymentAddress;
use BitWasp\BitcoinLib\BIP32;
use DateTime;

/**
 * Class Item
 *
 * @package Bitalo\Market\Logic\Item
 */
class User  extends BasePersistenceObject {

	const STATUS_EMAIL_VALIDATION_REQUIRED = 0;

	const STATUS_ACTIVE = 1;

	const STATUS_BLOCKED = 2;

	const STATUS_DELETED = 3;

	const WALLET_DERIVE_KEY = 'user:%s:wallet:derive';

	const CONVERSATION_LIST_KEY = 'user:%s:conversations';

	/**
	 * @return mixed|string
	 */
	public function getEntityName() {
		return '\Bitalo\Market\Orm\User';
	}

	/**
	 * @return string
	 */
	public function getPayoutAddress() {
		if($this->entity->getPayoutAddress()) {
			return $this->entity->getPayoutAddress()->getAddress();
		} else {
			false;
		}
	}

	/**
	 * @param $payoutAddress
	 *
	 * @return bool
	 */
	public function setPayoutAddress($payoutAddress) {
		if($this->getPayoutAddress() != $payoutAddress) {
			$payoutAddressObject = PaymentAddress::createPayoutAddress($payoutAddress, $this);
			$this->entity->setPayoutAddress($payoutAddressObject->entity);
			$this->save();
		}
	}

	/**
	 * @return mixed
	 */
	public function incrementWalletDerive() {
		return \AgaviContext::getInstance()->getDatabaseConnection('redis')->incr(sprintf(self::WALLET_DERIVE_KEY, $this->getId()));
	}

	/**
	 * @return int
	 */
	public function getWalletDerive() {
		if(!$walletDerive = \AgaviContext::getInstance()->getDatabaseConnection('redis')->get(sprintf(self::WALLET_DERIVE_KEY, $this->getId()))) {
			\AgaviContext::getInstance()->getDatabaseConnection('redis')->set(sprintf(self::WALLET_DERIVE_KEY, $this->getId()), 1);
			$walletDerive = 1;
		}

		return $walletDerive;
	}

	/**
	 * @return FALSE
	 */
	public function getActualPublicKey() {
		$derive = sprintf('0/%s', $this->getWalletDerive());
		$masterPublicKey = BIP32::build_key($this->getPublicKey(), $derive);
		$publicKey = BIP32::extract_public_key($masterPublicKey);

		return $publicKey;
	}

	/**
	 * @param User $fromUser
	 * @param      $body
	 *
	 * @return Conversation
	 */
	public function startConversation(User $fromUser, $body) {

		$redis = AgaviContext::getInstance()->getDatabaseConnection('redis');

		$message = new Message();
		$message->setUser($fromUser);
		$message->setBody($body);
		$message->setCreatedAt(new DateTime());

		$conversation = new Conversation();
		$conversation->appendMessage($message);

		$redis->lpush(sprintf(self::CONVERSATION_LIST_KEY, $this->getId()), $conversation->getId());
		$redis->lpush(sprintf(self::CONVERSATION_LIST_KEY, $fromUser->getId()), $conversation->getId());


		return $conversation;

		//@TODO: increment notification counter;
	}

	/**
	 * @param     $offset
	 * @param int $limit
	 *
	 * @return mixed
	 */
	public function getConversations($offset = 0, $limit = 25) {
		$conversationIds =  AgaviContext::getInstance()->getDatabaseConnection('redis')->lrange(sprintf(self::CONVERSATION_LIST_KEY, $this->getId()), $offset, $limit);
		$conversations = array();
		foreach($conversationIds as $id) {
			$conversations[] = new Conversation($id);
		}

		return $conversations;
	}

}