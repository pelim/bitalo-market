<?php

namespace Bitalo\Market\Logic\User;

use Bitalo\Market\Logic\Object\BasePersistenceObject;

class UserAddress extends BasePersistenceObject {

	public function getEntityName() {
		return '\Bitalo\Market\Orm\UserAddress';
	}
}