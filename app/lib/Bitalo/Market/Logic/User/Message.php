<?php

namespace Bitalo\Market\Logic\User;

use AgaviContext;
use Bitalo\Market\Utility\Guid;
use DateTime;

class Message {

	const MESSAGE_KEY = 'message:%s';

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var int
	 */
	protected $user;

	/**
	 * @var string
	 */
	protected $subject;

	/**
	 * @var string
	 */
	protected $body;

	/**
	 * @var \DateTime
	 */
	protected $createdAt;

	public function setUser(User $user) {
		$this->user = $user->getId();
	}

	public function getUser() {
		return new User($this->user);
	}

	public function setBody($body) {
		$this->body = $body;
	}

	public function getBody() {
		return $this->body;
	}

	public function setCreatedAt(DateTime $createdAt) {
		$this->createdAt = $createdAt;
	}

	public function getCreatedAt() {
		if($this->createdAt instanceof DateTime) {
			return $this->createdAt;
		} else {
			return new DateTime();
		}
	}

}