<?php
/**
 * Created by PhpStorm.
 * User: plh
 * Date: 06.12.13
 * Time: 11:01
 */

namespace Bitalo\Market\Logic\User;


use Bitalo\Market\Logic\Object\BaseFinder;

class UserFinder extends BaseFinder {

	public static function findForEmail($email) {
		if($user = self::getEntityManager()->getRepository('\Bitalo\Market\Orm\User')->findOneBy(array('email' => $email))) {
			return new User($user);
		}

	}

	public static function findForEmailAndHashedPassword($email, $hashedPassword) {
		if($user = self::getEntityManager()->getRepository('\Bitalo\Market\Orm\User')->findOneBy(array('email' => $email, 'password' => $hashedPassword))) {
			return new User($user);
		}
	}
} 