<?php

namespace Bitalo\Market\Logic\User;


use AgaviContext;
use Bitalo\Market\Utility\Guid;

class Conversation {

	const CONVERSATION_KEY = 'conversation:%s';

	protected $id;

	public function __construct($id = null) {
		$this->id = $id;
	}

	public function appendMessage(Message $message) {
		if(!$this->id) {
			$this->id = Guid::createGuid();
		}

		AgaviContext::getInstance()->getDatabaseConnection('redis')->lpush(sprintf(self::CONVERSATION_KEY, $this->id), serialize($message));

		return $this->id;
	}

	public function getId() {
		return $this->id;
	}

	public function getMessages($offset = 0, $limit = 25) {
		$messages =  AgaviContext::getInstance()->getDatabaseConnection('redis')->lrange(sprintf(self::CONVERSATION_KEY, $this->id), $offset, $limit);
		foreach($messages as &$message) {
			$message = unserialize($message);
		}

		return $messages;
	}

	public function getLastMessage() {
		$messages = $this->getMessages(0, 1);
		return array_pop($messages);
	}
}