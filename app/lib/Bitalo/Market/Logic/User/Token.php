<?php

namespace Bitalo\Market\Logic\User;


use Bitalo\Market\Logic\Object\BaseRedisHash;
use Bitalo\Market\Utility\Guid;

class Token extends BaseRedisHash {

	const TOKEN_KEY = 'token:%s';

	const TOKEN_TYPE_EMAIL = 'email';

	const TOKEN_MAX_USE_COUNT = 'maxUseCount';

	const TOKEN_USE_COUNT = 'useCount';

	public function getKeyPattern() {
		return self::TOKEN_KEY;
	}

	public static function createToken(User $user, $type, $ttl = null, $attributes = null) {
		$id = Guid::createGuid();
		$token = new self($id);
		$token->user = $user->getId();
		$token->type = $type;

		if(!isset($attributes[self::TOKEN_MAX_USE_COUNT])) {
			$attributes[self::TOKEN_MAX_USE_COUNT] = 1;
		}

		if(!isset($attributes[self::TOKEN_USE_COUNT])) {
			$attributes[self::TOKEN_USE_COUNT] = 0;
		}

		foreach($attributes as $key => $value) {
			$token->$key = $value;
		}

		if($ttl) {
			$token->getRedisConnection()->expire($token->getKey(), $ttl);
		}

		return $token;
	}

	/**
	 * @return bool
	 */
	public function isValid() {
		if($this->{self::TOKEN_USE_COUNT} < $this->{self::TOKEN_MAX_USE_COUNT}) {
			return true;
		}
	}

	public function isValidForUser(User $user) {
		if($this->isValid() && $user->getId() == $this->user) {
			return true;
		} else {
			return false;
		}
	}

	public function getUser() {
		return new User($this->user);
	}

	/**
	 * @param User $user
	 *
	 * @return int
	 */
	public function process(User $user) {
		if($this->isValidForUser($user)) {
			return $this->incrementUseCount();

		}
	}

	/**
	 * @return int
	 */
	protected function incrementUseCount() {
		return $this->getRedisConnection()->hincrby($this->getKey(), self::TOKEN_USE_COUNT, 1);
	}

}