<?php

namespace Bitalo\Market\Logic\Payment;

use Bitalo\Market\Logic\Object\BaseFinder;
use Bitalo\Market\Logic\Item\ItemPurchase;


class PaymentTransactionFinder extends BaseFinder {

	public static function findPaymentTransactionForItemPurchase(ItemPurchase $itemPurchase) {
		$result =  self::getEntityManager()->getRepository('\Bitalo\Market\Orm\PaymentTransaction')->findOneBy(array('itemPurchase' => $itemPurchase->getId()));
		return new PaymentTransaction($result);
	}
}