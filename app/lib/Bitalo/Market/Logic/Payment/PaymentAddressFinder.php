<?php

namespace Bitalo\Market\Logic\Payment;

use Bitalo\Market\Logic\Object\BaseFinder;


class PaymentAddressFinder extends BaseFinder {

	public static function findPaymentAddressForHex($paymentAddress) {
		$result =  self::getEntityManager()->getRepository('\Bitalo\Market\Orm\PaymentAddress')->findOneBy(array('address' => $paymentAddress));
		return new PaymentAddress($result);
	}
}