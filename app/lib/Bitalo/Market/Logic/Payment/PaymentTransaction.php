<?php

namespace Bitalo\Market\Logic\Payment;

use Bitalo\Market\Logic\Object\BasePersistenceObject;

use Bitalo\Market\Logic\Item\ItemPurchase;

use Bitalo\Market\Agavi\Config\Config as BitaloConfig;

class PaymentTransaction extends BasePersistenceObject
{

	const TYPE_DIRECT_PAYMENT = 0;

	const TYPE_ESCROW_PAYMENT = 1;

	const STATUS_INITIAL = 0;

	const STATUS_PAYMENT_REQUESTED = 1;

	const STATUS_PAYMENT_VERIFIED = 2;

	const STATUS_PAYMENT_RELEASED = 3;

	const STATUS_PAYMENT_INCIDENT = 4;

	const PAYMENT_CURRENCY_BTC = 'BTC';


	public function getEntityName() {
		return 'Bitalo\\Market\\Orm\\PaymentTransaction';
	}



}