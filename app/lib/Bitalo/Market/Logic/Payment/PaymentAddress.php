<?php

namespace Bitalo\Market\Logic\Payment;

use Bitalo\Market\Logic\Object\BasePersistenceObject;
use Bitalo\Market\Agavi\Config\Config as BitaloConfig;
use AgaviContext;
use Bitalo\Market\Logic\User\User;
use BitWasp\BitcoinLib\BitcoinLib;
use BitWasp\BitcoinLib\RawTransaction;

class PaymentAddress extends BasePersistenceObject {

	const TYPE_DEFAULT_ADDRESS = 0;

	const TYPE_MULTI_SIGNATURE_ADDRESS = 1;

	const TYPE_USER_PAYOUT_ADDRESS = 2;

	const STATUS_INITIAL = 0;

	const OUTPUT_TRANSACTION_KEY = 'payment:%s:tx:out';

	public static $addressVersions = [
		'testnet' => [
			'pub' =>'6f',
			'p2sh' => 'c4'
		],
		'mainnet' => [
			'pub' => '00',
			'p2sh' => '05'
		]
	];

	/** @TODO: configure address versions */

	public function getEntityName() {
		return 'Bitalo\\Market\\Orm\\PaymentAddress';
	}

	/**
	 * @param       $number
	 * @param array $users
	 *
	 * @internal param array $keys
	 *
	 * @return \Bitalo\Market\Logic\Payment\PaymentAddress
	 */
	public static function createMultiSignatureAddress($number, array $users) {
		$address = new self();
		$keys = array();
		$pubs = array();
		foreach($users as $user) {
			$pub = $user->getActualPublicKey();
			$keys[] = [
				'key' => $user->getActualPublicKey(),
				'user' => $user->getId(),
				'derive' => $user->getWalletDerive()
			];

			$pubs[] = $pub;
		}

		$pubs[] =  BitaloConfig::get('bitalo.public.key');

		$keys[] = [
			'key' => BitaloConfig::get('bitalo.public.key'),
			'application' => true
		];

		$addressVersion = self::$addressVersions[BitaloConfig::get('bitalo.network')]['p2sh'];

		$multiSignatureAddress = RawTransaction::create_multisig(2, $pubs, $addressVersion);
		$multiSignatureAddress['hash'] = BitcoinLib::hash160($multiSignatureAddress['redeemScript']);

		$address->setType(self::TYPE_MULTI_SIGNATURE_ADDRESS);

		$address->setAdditionalAttributes(array(
			'keys'    => $keys,
			'number'  => $number,
			'hash'    => $multiSignatureAddress['hash'],
			'script'  => $multiSignatureAddress['redeemScript']
		));

		$address->setAddress($multiSignatureAddress['address']);
		$address->setBalance(0);
		$address->setUnconfirmedBalance(0);
		$address->setConfirmationCount(0);

		$address->save(false);

		return $address;
	}

	public static function createPayoutAddress($payoutAddress, User $user) {
		$address = new self();

		$address->setType(self::TYPE_USER_PAYOUT_ADDRESS);

		$address->setAdditionalAttributes(array(
			'user'    => $user->getId()
		));

		$address->setAddress($payoutAddress);
		$address->setBalance(0);
		$address->setConfirmationCount(0);

		$address->save(false);

		return $address;
	}

	public function getOutputTransaction() {
		return AgaviContext::getInstance()->getDatabaseConnection('redis')->get(sprintf(self::OUTPUT_TRANSACTION_KEY, $this->getId()));
	}

	public function setOutputTransaction($transactionData) {
		return AgaviContext::getInstance()->getDatabaseConnection('redis')->set(sprintf(self::OUTPUT_TRANSACTION_KEY, $this->getId()), $transactionData);
	}

}