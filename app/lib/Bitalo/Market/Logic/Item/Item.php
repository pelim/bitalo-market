<?php

namespace Bitalo\Market\Logic\Item;

use AgaviContext;
use Bitalo\Market\Logic\Image\Image;
use Bitalo\Market\Logic\Object\BasePersistenceObject;
use Bitalo\Market\Logic\Payment\PaymentAddress;
use Bitalo\Market\Logic\Payment\PaymentTransaction;
use Bitalo\Market\Logic\User\User;
use Bitalo\Market\Orm\ItemStock;
use Bitalo\Market\Utility\ExchangeRates;
use Bitalo\Market\Agavi\Config\Config as BitaloConfig;

/**
 * Class Item
 *
 * @package Bitalo\Market\Logic\Item
 */
class Item  extends BasePersistenceObject {

	const STATUS_ACTIVE = 0;

	const STATUS_INACTIVE = 1;

	const STATUS_BLOCKED = 2;

	const STATUS_DELETED = 3;

	const TYPE_DIRECT_SALE = 1;

	const TYPE_AUCTION = 2;

	const TYPE_BARGAIN = 3;

	const TYPE_ITEM_REQUEST = 4;

	const COMMENT_KEY = 'item:%s:comment';

	public function getEntityName() {
		return '\Bitalo\Market\Orm\Item';
	}

	public function getPriceBtc() {
		return $this->getPrice() / ExchangeRates::getExchangeRate($this->getCurrency());
	}

	public function getPriceForCurrency($currency) {
		return $this->getPriceBtc() *  ExchangeRates::getExchangeRate($currency);
	}

	public static function createItemForUser(User $user) {
		$item = new self();
		$item->setUser($user);
		$item->setStatus(self::STATUS_ACTIVE);
		$item->setType(self::TYPE_DIRECT_SALE);

		return $item;
	}

	/**
	 * @return mixed
	 */
	public function getAvailableQuantity() {
		return ItemFinder::countAvailableQuantity($this);
	}

	/**
	 * @return int
	 */
	public function getQuantity() {
		return ItemFinder::sumItemStockQuantity($this);
	}

	/**
	 * @param $value
	 */
	public function addQuantity($value) {
		if($value !== 0) {
			$stock = new ItemStock();
			$stock->setQuantity($value);
			$stock->setItem($this->entity);
			$this->getEntityManager()->persist($stock);

			$this->entity->addStock($stock);
		}
	}

	/**
	 * @param $value
	 */
	public function setQuantity($value) {
		$this->entity->setQuantity($value);
		$this->addQuantity($value);
	}

	/**
	 * @return Image
	 */
	public function getImage() {
		$images = $this->entity->getImages(); /** @var $images \Doctrine\ORM\PersistentCollection */
		return new Image($images->first());
	}

	/**
	 * @return array
	 */
	public function getImages() {
		$images = array();
		foreach($this->entity->getImages() as $image) {
			$images[] = new Image($image);
		}
		return $images;
	}

	/**
	 * @return User
	 */
	public function getUser() {
		return new User($this->entity->getUser());
	}

	/**
	 * @param ItemComment $comment
	 */
	public function addComment(ItemComment $comment) {
		AgaviContext::getInstance()->getDatabaseConnection('redis')->lpush(sprintf(self::COMMENT_KEY, $this->getId()), serialize($comment));
	}

	/**
	 * @param int $offset
	 * @param int $limit
	 *
	 * @return mixed
	 */
	public function getComments($offset = 0, $limit = 25) {
		$comments =  AgaviContext::getInstance()->getDatabaseConnection('redis')->lrange(sprintf(self::COMMENT_KEY, $this->getId()), $offset, $limit);
		foreach($comments as &$comment) {
			$comment = unserialize($comment);
		}

		return $comments;
	}

	public function getLastVersion() {
		$logRepository = $this->getEntityManager()->getRepository('Bitalo\\Market\\Orm\\LogEntry'); /** @var $logRepository  \Gedmo\Loggable\Entity\Repository\LogEntryRepository */
		$logEntries = $logRepository->getLogEntries($this->entity);
		if(count($logEntries)) {
			$lastLogEntry = array_shift($logEntries);
			return $lastLogEntry->getVersion();
		} else {
			return 0;
		}

	}
}