<?php

namespace Bitalo\Market\Logic\Item;

use Bitalo\Market\Logic\Object\BasePersistenceObject;
use Bitalo\Market\Logic\Payment\PaymentTransaction;
use Bitalo\Market\Logic\User\User;

/**
 * Class Item
 *
 * @package Bitalo\Market\Logic\Item
 */
class Category  extends BasePersistenceObject {


	public function getEntityName() {
		return '\Bitalo\Market\Orm\Category';
	}

}