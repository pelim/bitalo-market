<?php

namespace Bitalo\Market\Logic\Item;

use Doctrine\Common\Proxy\Exception\InvalidArgumentException;

class ItemShippingOption {

	/**
	 * @var string
	 */
	protected $description;

	/**
	 * @var float
	 */
	protected $price;


	/**
	 * @var string
	 */
	protected $currency;


	/**
	 * @param $array
	 *
	 * @throws \Doctrine\Common\Proxy\Exception\InvalidArgumentException
	 */
	public function __construct($array) {
		if(isset($array['description']) && isset($array['price'])) {
			if(is_string($array['description'])) {
				$this->description = $array['description'];
			} else {
				throw new InvalidArgumentException('awaiting string');
			}

			if(is_numeric($array['price'])) {
				$this->price = $array['price'];
			} else {
				throw new InvalidArgumentException('awaiting number');
			}

			if(isset($array['id'])) {
				$this->id = (int) $array['id'];
			}
		} else {
			throw new InvalidArgumentException('missing parameter');
		}
	}

	/**
	 * @return mixed
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @return mixed
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}
}