<?php

namespace Bitalo\Market\Logic\Item;

use Bitalo\Market\Logic\Object\BaseNotification;
use Bitalo\Market\Logic\Object\BasePersistenceObject;
use Bitalo\Market\Logic\Object\BaseProcess;
use Bitalo\Market\Logic\Payment\PaymentAddress;
use Bitalo\Market\Logic\Payment\PaymentTransaction;
use Bitalo\Market\Logic\User\User;
use Bitalo\Market\Utility\ExchangeRates;

/**
 * Class ItemPurchase
 *
 * @package Bitalo\Market\Logic\Item
 */
class ItemPurchase extends BasePersistenceObject implements BaseProcess, BaseNotification {


	const STATUS_INITIAL = 0;

	const STATUS_PURCHASED = 1;

	const STATUS_PAYMENT_AWAITING = 1;

	const STATUS_PAYED = 2;

	const STATUS_SHIPPED = 3;

	const STATUS_RECEIVED = 4;

	const STATUS_RELEASED = 5;

	const STATUS_ARCHIVED = 6;

	const STATUS_CANCELED = 20;

	const STATUS_PAYMENT_TIMEOUT = 30;

	const STATUS_ITEM_OUT_OF_STOCK = 40;

	const STATUS_INCIDENT = 50;

	const EVENT_ITEM_PURCHASED = 'item.purchase.status.purchased';

	const EVENT_ITEM_RECEIVED  = 'item.purchase.status.received';

	const EVENT_ITEM_PAYED     = 'item.purchase.status.payed';

	const EVENT_ITEM_SHIPPED   = 'item.purchase.status.shipped';


	/**
	 * @return string
	 */
	public function getEntityName() {
		return 'Bitalo\\Market\\Orm\\ItemPurchase';
	}


	public function getNotificationKey() {
		return $this->getId();
	}

	public function getNotificationType() {
		return 'purchase';
	}


	/**
	 * @return array
	 */
	public static function getProcessChain() {
		return array(
			self::STATUS_INITIAL   =>  self::PROCESS_OWNER_PURCHASER,
			self::STATUS_PURCHASED =>  self::PROCESS_OWNER_PURCHASER,
			self::STATUS_PAYED     =>  self::PROCESS_OWNER_PURCHASER,
			self::STATUS_SHIPPED   =>  self::PROCESS_OWNER_VENDOR,
			self::STATUS_RECEIVED  =>  self::PROCESS_OWNER_PURCHASER
		);
	}

	/**
	 * @return array
	 */
	public static function getProcessEvents() {
		return array(
			self::STATUS_PURCHASED =>  self::EVENT_ITEM_PURCHASED,
			self::STATUS_PAYED     =>  self::EVENT_ITEM_PAYED,
			self::STATUS_SHIPPED   =>  self::EVENT_ITEM_SHIPPED,
			self::STATUS_RECEIVED  =>  self::EVENT_ITEM_RECEIVED
		);
	}

	/**
	 * @return \Bitalo\Market\Logic\User\User;
	 * @throws \InvalidArgumentException
	 */
	public function getProcessOwner() {
		return $this->getProcessOwnerForStatus($this->getStatus());
	}

	/**
	 * @param $status
	 *
	 * @return \Bitalo\Market\Logic\User\User;
	 * @throws \InvalidArgumentException
	 */
	protected function getProcessOwnerForStatus($status) {
		$processChain = self::getProcessChain();

		if($processChain[$status] == self::PROCESS_OWNER_PURCHASER) {
			return $this->getUser();
		} elseif($processChain[$status] == self::PROCESS_OWNER_VENDOR) {

			return $this->getItem()->getUser();
		} elseif($processChain[$status] == self::PROCESS_OWNER_APPLICATION) {

			return self::PROCESS_OWNER_APPLICATION;
		} else {

			throw new \InvalidArgumentException('Invalid process owner');
		}
	}

	/**
	 * @return bool|mixed
	 */
	public function getNextProcessStep() {
		$processChain = $this->getProcessChain();
		$next = false;
		foreach($processChain as $status => $owner) {
			if($next == true) {
				return $status;
			}
			if($status == $this->getStatus()) {
				$next = true;
			}
		}

		return false;
	}

	/**
	 * @param User $user
	 *
	 * @return bool
	 */
	public function isProcessAllowedByUser(User $user) {

		if($next = $this->getNextProcessStep()) {
			if($processOwner = $this->getProcessOwnerForStatus($next)) {
				if($processOwner instanceof User && $processOwner->getId() == $user->getId()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param null $actualNode
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	protected function getActualStatusNode($actualNode = null) {
		if(!$actualNode) {
			$actualNode = self::$PROCESS_CHAIN[self::STATUS_INITIAL];
			if($this->getStatus() == self::STATUS_INITIAL) {
				return $actualNode;
			}
		}
		foreach($actualNode as $status => $node) {
			if($status == $this->getStatus()) {
				return $node;
			} else {
				if($node !== false) {
					return $this->getActualStatusNode($node);
				}
			}
		}

		throw new \Exception('actual status not found in process chain');
	}

	/**
	 * @param User $user
	 *
	 * @throws \Exception
	 * @internal param $statusIdentifier
	 *
	 * @return bool
	 */
	public function processStep(User $user) {
		if($this->isProcessAllowedByUser($user)) {
			$status = $this->getNextProcessStep();
			$this->setStatus($status);
			$this->save();

			$events = $this->getProcessEvents();
			if(isset($events[$status])) {
				$eventManager = \AgaviContext::getInstance()->getEventManager(); /** @var $eventManager \EventManagerModel */
				$eventManager->fire($user, $events[$status], $user, $this);
			}
			return true;

		} else {
			throw new \Exception('User not authorized to process actual step');
		}
	}

	/**
	 * @return bool
	 */
	public function isSuccessFullFinished() {
		return ($this->getNextProcessStep() === false);
	}

	/**
	 * @return mixed
	 */
	public function getPurchaseTotal() {
		if($shippingOption = $this->getShippingOption()) {
			return ($this->getItem()->getPrice() * $this->getAmount()) + $shippingOption->getPrice();
		} else {
			return ($this->getItem()->getPrice() * $this->getAmount());
		}
	}

	/**
	 * @return ItemShippingOption
	 */
	public function getShippingOption() {
		$shippingOptions = $this->getItem()->getShippingOptions();
		if(is_array($shippingOptions) && count($shippingOptions) && isset($shippingOptions[$this->entity->getShippingOption()])) {
			return new ItemShippingOption($shippingOptions[$this->entity->getShippingOption()]);
		}
	}

	/**
	 * @TODO: check btc precision and create btc calculation manager
	 */

	/**
	 * @return float
	 */
	public function getPurchaseTotalBtc() {
		return $this->getPurchaseOutputBtc() + $this->getPurchaseFeeBtc();
	}

	/**
	 * @return float
	 */
	public function getPurchaseOutputBtc() {
		return round($this->getPurchaseTotal() / ExchangeRates::getExchangeRate($this->getItem()->getCurrency()), 8);
	}

	/**
	 * @return float
	 */
	public function getPurchaseFeeBtc() {
		return $this->getAmount() * PaymentTransaction::getTransactionFee();
	}


	/**
	 * @return bool
	 */
	public function isProcessing() {

	}

	/**
	 * @return Item
	 */
	public function getItem() {

		$logRepository = $this->getEntityManager()->getRepository('Bitalo\\Market\\Orm\\LogEntry'); /** @var $logRepository  \Gedmo\Loggable\Entity\Repository\LogEntryRepository */
		$itemEntity = $this->entity->getItem();

		if($itemVersion = $this->getItemVersion()) {
			$logRepository->revert($itemEntity, $itemVersion);
		}

		return new Item($itemEntity);

	}

	/**
	 * @return User
	 */
	public function getUser() {
		return new User($this->entity->getUser());
	}

	public function getShippingAddressField($field) {
		$address = $this->getShippingAddress();
		if(isset($address[$field])) {
			return $address[$field];
		}
	}

	/**
	 * @return PaymentAddress
	 */
	public function getWallet() {
		return new PaymentAddress($this->entity->getWallet());
	}
}