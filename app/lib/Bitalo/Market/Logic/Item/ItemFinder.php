<?php

namespace Bitalo\Market\Logic\Item;


use Bitalo\Market\Logic\User\User;

use Bitalo\Market\Logic\Object\BaseFinder;
use Doctrine\ORM\Query\ResultSetMapping;

class ItemFinder extends BaseFinder {

	const ORDER_BY_CREATION = 'Item.createdAt';

	const ORDER_BY_PRICE = 'Item.price';

	/**
	 * @param int $offset
	 * @param int $limit
	 *
	 * @return array
	 */
	public static function findActiveItems($offset = self::DEFAULT_OFFSET, $limit = self::DEFAULT_LIMIT) {
		$result = self::createAvailableItemsQuery($offset, $limit)
			->getQuery()->getResult();
		return self::wrapArrayValues($result, __NAMESPACE__ . '\\Item');
	}

	/**'
	 * @param string $order
	 * @param string $sort
	 * @param int    $offset
	 * @param int    $limit
	 *
	 * @return array
	 */
	public static function findActiveItemsOrderBy($order = self::ORDER_BY_CREATION, $sort = self::SORT_ASC, $offset = self::DEFAULT_OFFSET, $limit = self::DEFAULT_LIMIT) {
		$result = self::createAvailableItemsQuery($offset, $limit)
			->orderBy($order, $sort)
			->getQuery()->getResult();

		return self::wrapArrayValues($result, __NAMESPACE__ . '\\Item');
	}

	/**
	 * @param Category $category
	 * @param int      $offset
	 * @param int      $limit
	 *
	 * @return array
	 */
	public static function findActiveItemsForCategory(Category $category, $offset = self::DEFAULT_OFFSET, $limit = self::DEFAULT_LIMIT) {
		$result = self::createAvailableItemsQuery($offset, $limit)
			->andWhere('Item.category = :category')
			->setParameter('category', $category->getId())
			->getQuery()->getResult();
		return self::wrapArrayValues($result, __NAMESPACE__ . '\\Item');
	}

	/**
	 * @param Category $category
	 * @param string   $order
	 * @param string   $sort
	 * @param int      $offset
	 * @param int      $limit
	 *
	 * @return array
	 */
	public static function findActiveItemsForCategoryOrderBy(Category $category, $order = self::ORDER_BY_CREATION, $sort = self::SORT_ASC, $offset = self::DEFAULT_OFFSET, $limit = self::DEFAULT_LIMIT) {
		$result = self::createAvailableItemsQuery($offset, $limit)
			->andWhere('Item.category = :category')
			->orderBy($order, $sort)
			->setParameter('category', $category->getId())
			->getQuery()->getResult();
		return self::wrapArrayValues($result, __NAMESPACE__ . '\\Item');
	}

	/**
	 * @param int $offset
	 * @param int $limit
	 *
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	protected static function createAvailableItemsQuery($offset = self::DEFAULT_OFFSET, $limit = self::DEFAULT_LIMIT) {
		$query = self::query()->select('Item')->from('\Bitalo\Market\Orm\Item', 'Item')
			->where('(' . self::createItemStockSumQuery() . ') > (' .
				self::createItemCountQuery()->getDQL() . ')')
			->andWhere('Item.status = :status')
			->setParameter('status', Item::STATUS_ACTIVE)
			->setFirstResult($offset)->setMaxResults($limit);

		return $query;
	}

	protected static function createItemCountQuery() {
		return self::query()
			->select('count(ItemPurchase.id)')
			->from('\Bitalo\Market\Orm\ItemPurchase', 'ItemPurchase')
			->where('ItemPurchase.item = Item.id');
	}

	protected static function createItemStockSumQuery() {
		return self::query()
			->select('sum(ItemStock.quantity)')
			->from('\Bitalo\Market\Orm\ItemStock', 'ItemStock')
			->where('ItemStock.item = Item.id');
	}

	/**
	 * @param User $user
	 * @param int  $offset
	 * @param int  $limit
	 *
	 * @return array
	 */
	public static function findItemsForUser(User $user, $offset = self::DEFAULT_OFFSET, $limit = self::DEFAULT_LIMIT) {
		$result =  self::getEntityManager()->getRepository('\Bitalo\Market\Orm\Item')
			->findBy(array('user' => $user->getId()));
		return self::wrapArrayValues($result, __NAMESPACE__ . '\\Item');
	}

	public static function sumItemStockQuantity(Item $item) {
		$rsm = new ResultSetMapping();
		$rsm->addScalarResult('q', 'sumQuantity');

		$query = self::getEntityManager()->createNativeQuery(
			'SELECT SUM(item_stock.quantity) AS Q FROM item_stock WHERE item_id = :item', $rsm);
		$query->setParameter('item', $item->getId());
		return (int) $query->getSingleScalarResult();
	}

	/**
	 * @param Item $item
	 *
	 * @return mixed
	 */
	public static function countAvailableQuantity(Item $item) {
		$rsm = new ResultSetMapping();
		$rsm->addScalarResult('q', 'availableQuantity');

		$query = self::getEntityManager()->createNativeQuery(
			'SELECT (SELECT SUM(item_stock.quantity) FROM item_stock WHERE item_id = item.id) - (SELECT COUNT(*) FROM item_purchase WHERE item_id = item.id) AS Q
				FROM item WHERE item.id = :item', $rsm);
		$query->setParameter('item', $item->getId());
		return (int) $query->getSingleScalarResult();

	}

}