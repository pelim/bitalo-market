<?php

namespace Bitalo\Market\Logic\Item;

use Bitalo\Market\Logic\User\User;

class ItemComment {

	const MIN_POINTS = 0;

	const MAX_POINTS = 5;

	protected $id;

	protected $user;

	protected $subject;

	protected $body;

	protected $score;

	protected $createdAt;

	public function setUser(User $user) {
		$this->user = $user->getId();
	}

	public function getUser() {
		return new User($this->user);
	}

	public function setBody($body) {
		$this->body = $body;
	}

	public function getBody() {
		return $this->body;
	}

	public function setScore($score) {
		$this->score = $score;
	}

	public static function createComment(User $user, $text) {
		$comment = new self();

		$comment->setUser($user);
		$comment->setBody($text);

		return $comment;
	}

}