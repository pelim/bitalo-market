<?php

namespace Bitalo\Market\Logic\Item;


use Bitalo\Market\Logic\Object\BaseFinder;
use Doctrine\ORM\Query\ResultSetMapping;

class CategoryFinder extends BaseFinder {

	/**'
	 * @return array
	 */
	public static function findCategories() {
		$result =  self::getEntityManager()->getRepository('\Bitalo\Market\Orm\Category')->findAll();
		return self::wrapArrayValues($result, __NAMESPACE__ . '\\Category');
	}

}