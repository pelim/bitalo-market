<?php

namespace Bitalo\Market\Logic\Item;


use Bitalo\Market\Logic\Object\BaseFinder;
use Bitalo\Market\Logic\User\User;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Bitalo\Market\Logic\Payment\PaymentAddress;


class ItemPurchaseFinder extends BaseFinder {

	/**
	 * @param User $user
	 *
	 * @return array
	 */
	public static function findPurchasesForUser(User $user, $offset = self::DEFAULT_OFFSET, $limit = self::DEFAULT_LIMIT) {
		$result =  self::getEntityManager()->getRepository('\Bitalo\Market\Orm\ItemPurchase')->findBy(array('user' => $user->getId()));
		return self::wrapArrayValues($result, __NAMESPACE__ . '\\ItemPurchase');
	}

	public static function findActivePurchasesForUser(User $user, $offset = self::DEFAULT_OFFSET, $limit = self::DEFAULT_LIMIT) {
		$result = self::query()->select('ItemPurchase')->from('\Bitalo\Market\Orm\ItemPurchase', 'ItemPurchase')
			->where('ItemPurchase.user = :user')
			->andWhere(self::exp()->in('ItemPurchase.status', array(
					ItemPurchase::STATUS_PAYED,
					ItemPurchase::STATUS_SHIPPED,
					ItemPurchase::STATUS_RECEIVED,)))
			->setParameter('user', $user)
			->setFirstResult($offset)->setMaxResults($limit)
			->getQuery()->getResult();

		return self::wrapArrayValues($result, __NAMESPACE__ . '\\ItemPurchase');
	}

	public static function findActivePurchasesForVendor(User $user, $offset = self::DEFAULT_OFFSET, $limit = self::DEFAULT_LIMIT) {

		$result = self::query()->select('ItemPurchase')->from('\Bitalo\Market\Orm\ItemPurchase', 'ItemPurchase')
			->where('EXISTS (' .
				self::query()->select('Item.id')
					->from('\Bitalo\Market\Orm\Item', 'Item')
					->where('Item.user = :user')->setParameter('user', $user)->getDQL() . ')')
			->andWhere(self::exp()->in('ItemPurchase.status', array(
				ItemPurchase::STATUS_PAYED,
				ItemPurchase::STATUS_SHIPPED,
				ItemPurchase::STATUS_RECEIVED,
				ItemPurchase::STATUS_RELEASED)))
			->setFirstResult($offset)->setMaxResults($limit)
			->getQuery()->getResult();

		return self::wrapArrayValues($result, __NAMESPACE__ . '\\ItemPurchase');
	}


	/**
	 * @param User $user
	 *
	 * @return array
	 */
	public static function findPurchasesForItemUser(User $user) {

		$rsm = new ResultSetMappingBuilder(self::getEntityManager());
		$rsm->addRootEntityFromClassMetadata('\Bitalo\Market\Orm\ItemPurchase', 'item_purchase');
		$query = self::getEntityManager()->createNativeQuery(
			'SELECT purchase.* FROM item_purchase AS purchase
				JOIN item ON item.id = purchase.item_id
			WHERE item.user_id = :user', $rsm);

		$query->setParameter('user', $user->getId());

		$result = $query->getResult();

		return self::wrapArrayValues($result, __NAMESPACE__ . '\\ItemPurchase');
	}


	/**
	 * @param PaymentAddress $address
	 *
	 * @return ItemPurchase
	 */
	public static function findPurchaseByPaymentAddress(PaymentAddress $address) {
		$result =  self::getEntityManager()->getRepository('\Bitalo\Market\Orm\ItemPurchase')->findOneBy(array('wallet' => $address->getId()));
		return new ItemPurchase($result);
	}



}