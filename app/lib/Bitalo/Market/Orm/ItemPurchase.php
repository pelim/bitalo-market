<?php

namespace Bitalo\Market\Orm;

use AgaviContext;
use Doctrine\ORM\Mapping as ORM;

/**
 * ItemPurchase
 */
class ItemPurchase
{
    /**
     * @var integer
     */
    private $status;

    /**
     * @var float
     */
    private $paymentAmount;

    /**
     * @var float
     */
    private $quantity;

    /**
     * @var array
     */
    private $shippingAddress;

    /**
     * @var integer
     */
    private $shippingOption;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Bitalo\Market\Orm\PaymentAddress
     */
    private $wallet;

    /**
     * @var \Bitalo\Market\Orm\Item
     */
    private $item;

    /**
     * @var \Bitalo\Market\Orm\User
     */
    private $user;


    /**
     * Set status
     *
     * @param integer $status
     * @return ItemPurchase
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set paymentAmount
     *
     * @param float $paymentAmount
     * @return ItemPurchase
     */
    public function setPaymentAmount($paymentAmount)
    {
        $this->paymentAmount = $paymentAmount;

        return $this;
    }

    /**
     * Get paymentAmount
     *
     * @return float 
     */
    public function getPaymentAmount()
    {
        return $this->paymentAmount;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     * @return ItemPurchase
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return float 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set shippingAddress
     *
     * @param array $shippingAddress
     * @return ItemPurchase
     */
    public function setShippingAddress($shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    /**
     * Get shippingAddress
     *
     * @return array 
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * Set shippingOption
     *
     * @param integer $shippingOption
     * @return ItemPurchase
     */
    public function setShippingOption($shippingOption)
    {
        $this->shippingOption = $shippingOption;

        return $this;
    }

    /**
     * Get shippingOption
     *
     * @return integer 
     */
    public function getShippingOption()
    {
        return $this->shippingOption;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ItemPurchase
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return ItemPurchase
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wallet
     *
     * @param \Bitalo\Market\Orm\PaymentAddress $wallet
     * @return ItemPurchase
     */
    public function setWallet(\Bitalo\Market\Orm\PaymentAddress $wallet = null)
    {
        $this->wallet = $wallet;

        return $this;
    }

    /**
     * Get wallet
     *
     * @return \Bitalo\Market\Orm\PaymentAddress 
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * Set item
     *
     * @param \Bitalo\Market\Orm\Item $item
     * @return ItemPurchase
     */
    public function setItem(\Bitalo\Market\Orm\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Bitalo\Market\Orm\Item 
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set user
     *
     * @param \Bitalo\Market\Orm\User $user
     * @return ItemPurchase
     */
    public function setUser(\Bitalo\Market\Orm\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Bitalo\Market\Orm\User 
     */
    public function getUser()
    {
        return $this->user;
    }

	public function processStatus($event) {
		$em = AgaviContext::getInstance()->getDatabaseConnection(); /** @var $em \Doctrine\ORM\EntityManager */
		$cs = $em->getUnitOfWork()->getEntityChangeSet($this);
		if(isset($cs['status'])) {

		}
	}
    /**
     * @var integer
     */
    private $itemVersion;


    /**
     * Set itemVersion
     *
     * @param integer $itemVersion
     * @return ItemPurchase
     */
    public function setItemVersion($itemVersion)
    {
        $this->itemVersion = $itemVersion;

        return $this;
    }

    /**
     * Get itemVersion
     *
     * @return integer 
     */
    public function getItemVersion()
    {
        return $this->itemVersion;
    }
}
