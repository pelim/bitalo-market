<?php

namespace Bitalo\Market\Orm;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 */
class User
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $nick;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var integer
     */
    private $id;

	/**
	 * @var integer
	 */
	private $publicKey;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $purchases;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $items;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $addresses;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->purchases = new \Doctrine\Common\Collections\ArrayCollection();
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
        $this->addresses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set nick
     *
     * @param string $nick
     * @return User
     */
    public function setNick($nick)
    {
        $this->nick = $nick;

        return $this;
    }

    /**
     * Get nick
     *
     * @return string 
     */
    public function getNick()
    {
        return $this->nick;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return User
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getPublicKey()
	{
		return $this->publicKey;
	}


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function setPublicKey($publicKey)
	{
		 $this->publicKey = $publicKey;
	}

    /**
     * Add purchases
     *
     * @param \Bitalo\Market\Orm\ItemPurchase $purchases
     * @return User
     */
    public function addPurchase(\Bitalo\Market\Orm\ItemPurchase $purchases)
    {
        $this->purchases[] = $purchases;

        return $this;
    }

    /**
     * Remove purchases
     *
     * @param \Bitalo\Market\Orm\ItemPurchase $purchases
     */
    public function removePurchase(\Bitalo\Market\Orm\ItemPurchase $purchases)
    {
        $this->purchases->removeElement($purchases);
    }

    /**
     * Get purchases
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPurchases()
    {
        return $this->purchases;
    }

    /**
     * Add items
     *
     * @param \Bitalo\Market\Orm\Item $items
     * @return User
     */
    public function addItem(\Bitalo\Market\Orm\Item $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \Bitalo\Market\Orm\Item $items
     */
    public function removeItem(\Bitalo\Market\Orm\Item $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Add addresses
     *
     * @param \Bitalo\Market\Orm\UserAddress $addresses
     * @return User
     */
    public function addAddress(\Bitalo\Market\Orm\UserAddress $addresses)
    {
        $this->addresses[] = $addresses;

        return $this;
    }

    /**
     * Remove addresses
     *
     * @param \Bitalo\Market\Orm\UserAddress $addresses
     */
    public function removeAddress(\Bitalo\Market\Orm\UserAddress $addresses)
    {
        $this->addresses->removeElement($addresses);
    }

    /**
     * Get addresses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAddresses()
    {
        return $this->addresses;
    }
    /**
     * @var string
     */
    private $masterKey;


    /**
     * Set masterKey
     *
     * @param string $masterKey
     * @return User
     */
    public function setMasterKey($masterKey)
    {
        $this->masterKey = $masterKey;

        return $this;
    }

    /**
     * Get masterKey
     *
     * @return string 
     */
    public function getMasterKey()
    {
        return $this->masterKey;
    }
    /**
     * @var \Bitalo\Market\Orm\PaymentAddress
     */
    private $payout_address;


    /**
     * Set payout_address
     *
     * @param \Bitalo\Market\Orm\PaymentAddress $payoutAddress
     * @return User
     */
    public function setPayoutAddress(\Bitalo\Market\Orm\PaymentAddress $payoutAddress = null)
    {
        $this->payout_address = $payoutAddress;

        return $this;
    }

    /**
     * Get payout_address
     *
     * @return \Bitalo\Market\Orm\PaymentAddress 
     */
    public function getPayoutAddress()
    {
        return $this->payout_address;
    }
}
