<?php

namespace Bitalo\Market\Orm;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentTransaction
 */
class PaymentTransaction
{
    /**
     * @var integer
     */
    private $type;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $fromAddress;

    /**
     * @var string
     */
    private $toAddress;

    /**
     * @var array
     */
    private $additionalAttributes;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Bitalo\Market\Orm\ItemPurchase
     */
    private $itemPurchase;


    /**
     * Set type
     *
     * @param integer $type
     * @return PaymentTransaction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return PaymentTransaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return PaymentTransaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return PaymentTransaction
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set fromAddress
     *
     * @param string $fromAddress
     * @return PaymentTransaction
     */
    public function setFromAddress($fromAddress)
    {
        $this->fromAddress = $fromAddress;

        return $this;
    }

    /**
     * Get fromAddress
     *
     * @return string 
     */
    public function getFromAddress()
    {
        return $this->fromAddress;
    }

    /**
     * Set toAddress
     *
     * @param string $toAddress
     * @return PaymentTransaction
     */
    public function setToAddress($toAddress)
    {
        $this->toAddress = $toAddress;

        return $this;
    }

    /**
     * Get toAddress
     *
     * @return string 
     */
    public function getToAddress()
    {
        return $this->toAddress;
    }

    /**
     * Set additionalAttributes
     *
     * @param array $additionalAttributes
     * @return PaymentTransaction
     */
    public function setAdditionalAttributes($additionalAttributes)
    {
        $this->additionalAttributes = $additionalAttributes;

        return $this;
    }

    /**
     * Get additionalAttributes
     *
     * @return array 
     */
    public function getAdditionalAttributes()
    {
        return $this->additionalAttributes;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PaymentTransaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return PaymentTransaction
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemPurchase
     *
     * @param \Bitalo\Market\Orm\ItemPurchase $itemPurchase
     * @return PaymentTransaction
     */
    public function setItemPurchase(\Bitalo\Market\Orm\ItemPurchase $itemPurchase = null)
    {
        $this->itemPurchase = $itemPurchase;

        return $this;
    }

    /**
     * Get itemPurchase
     *
     * @return \Bitalo\Market\Orm\ItemPurchase 
     */
    public function getItemPurchase()
    {
        return $this->itemPurchase;
    }
    /**
     * @var string
     */
    private $output_address;

    /**
     * @var \Bitalo\Market\Orm\PaymentAddress
     */
    private $PaymentWallet;


    /**
     * Set output_address
     *
     * @param string $outputAddress
     * @return PaymentTransaction
     */
    public function setOutputAddress($outputAddress)
    {
        $this->output_address = $outputAddress;

        return $this;
    }

    /**
     * Get output_address
     *
     * @return string 
     */
    public function getOutputAddress()
    {
        return $this->output_address;
    }

    /**
     * Set PaymentWallet
     *
     * @param \Bitalo\Market\Orm\PaymentAddress $paymentWallet
     * @return PaymentTransaction
     */
    public function setPaymentWallet(\Bitalo\Market\Orm\PaymentAddress $paymentWallet = null)
    {
        $this->PaymentWallet = $paymentWallet;

        return $this;
    }

    /**
     * Get PaymentWallet
     *
     * @return \Bitalo\Market\Orm\PaymentAddress 
     */
    public function getPaymentWallet()
    {
        return $this->PaymentWallet;
    }
}
