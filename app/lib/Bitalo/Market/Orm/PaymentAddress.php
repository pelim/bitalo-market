<?php

namespace Bitalo\Market\Orm;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentAddress
 */
class PaymentAddress
{
    /**
     * @var integer
     */
    private $type;

    /**
     * @var string
     */
    private $address;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set type
     *
     * @param integer $type
     * @return PaymentAddress
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return PaymentAddress
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PaymentAddress
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return PaymentAddress
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var array
     */
    private $additionalAttributes;


    /**
     * Set additionalAttributes
     *
     * @param array $additionalAttributes
     * @return PaymentAddress
     */
    public function setAdditionalAttributes($additionalAttributes)
    {
        $this->additionalAttributes = $additionalAttributes;

        return $this;
    }

    /**
     * Get additionalAttributes
     *
     * @return array 
     */
    public function getAdditionalAttributes()
    {
        return $this->additionalAttributes;
    }
    /**
     * @var integer
     */
    private $balance;

    /**
     * @var integer
     */
    private $confirmation_count;


    /**
     * Set balance
     *
     * @param integer $balance
     * @return PaymentAddress
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return integer 
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set confirmation_count
     *
     * @param integer $confirmationCount
     * @return PaymentAddress
     */
    public function setConfirmationCount($confirmationCount)
    {
        $this->confirmation_count = $confirmationCount;

        return $this;
    }

    /**
     * Get confirmation_count
     *
     * @return integer 
     */
    public function getConfirmationCount()
    {
        return $this->confirmation_count;
    }
    /**
     * @var float
     */
    private $unconfirmed_balance;


    /**
     * Set unconfirmed_balance
     *
     * @param float $unconfirmedBalance
     * @return PaymentAddress
     */
    public function setUnconfirmedBalance($unconfirmedBalance)
    {
        $this->unconfirmed_balance = $unconfirmedBalance;

        return $this;
    }

    /**
     * Get unconfirmed_balance
     *
     * @return float 
     */
    public function getUnconfirmedBalance()
    {
        return $this->unconfirmed_balance;
    }
}
