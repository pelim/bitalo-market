<?php

namespace Bitalo\Market\Agavi\Config;


class Config extends \AgaviConfig {
	
	const CONFIG_NAMESPACE = 'bitalo';
	
	
	/**
	 * @param string $name
	 * @param mixed $default
	 * @return mixed
	 */
	public static function get($name, $default = null) {
		return parent::get(static::CONFIG_NAMESPACE . '.' . $name, $default);
	}
	
	/**
	 * Check if a configuration directive has been set.
	 *
	 * @param string $name
	 * @return bool
	 */
	public static function has($name) {
		return parent::has(static::CONFIG_NAMESPACE . '.' . $name);
	}
	
	/**
	 * Set a configuration value.
	 *
	 * @param string $name
	 * @param mixed $value
	 * @param bool $overwrite Whether or not an existing value should be overwritten.
	 * @return bool Whether or not the configuration directive has been set.
	 */
	public static function set($name, $value, $overwrite = true, $readonly = false) {
		return parent::set(static::CONFIG_NAMESPACE . '.' . $name, $value, $overwrite, $readonly);
	}
	
	/**
	 * Remove a configuration value.
	 *
	 * @param string $name
	 * @return bool true, if removed successfully, false otherwise.
	 */
	public static function remove($name) {
		return parent::remove(static::CONFIG_NAMESPACE . '.' . $name);
	}
	
	/**
	 * Import a list of configuration directives.
	 *
	 * @param array $data An array of configuration directives.
	 * @return void
	 */
	public static function fromArray(array $data) {
		$import = array();
		foreach($data as $key => $value) {
			$import[static::CONFIG_NAMESPACE . '.' . $key] = $value;
		}
		return parent::fromArray($import);
	}
	
	/**
	 * Get all configuration directives and values.
	 *
	 * @return array An associative array of configuration values.
	 */
	public static function toArray() {
		$retval = array();
		foreach(parent::toArray() as $key => $value) {
			$keyPrefix = static::CONFIG_NAMESPACE . '.';
			if(strpos($key, $keyPrefix) === 0) {
				$retval[substr($key, strlen($keyPrefix))] = $value;
			}
		}
		return $retval;
	}
	
	/**
	 * Clear the configuration.
	 */
	public static function clear() {
		foreach(array_keys(self::toArray()) as $key) {
			self::remove($key);
		}
	}
	
}

?>