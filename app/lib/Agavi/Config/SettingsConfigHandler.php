<?php

namespace Bitalo\Market\Agavi\Config;

use AgaviXmlConfigHandler;
use AgaviXmlConfigDomDocument;
use AgaviToolkit;

class SettingsConfigHandler extends AgaviXmlConfigHandler {
	
	const CONFIG_CLASS  = '\Bitalo\Market\Agavi\Config\Config';
	const XML_NAMESPACE = 'http://bitalo/config/parts/settings/1.1';
	
	
	/**
	 * @param \AgaviXmlConfigDomDocument $document
	 * @return string
	 */
	public function execute(AgaviXmlConfigDomDocument $document) {
		// set up our default namespace
		$document->setDefaultNamespace(self::XML_NAMESPACE, 'settings');
		
		// init our data array
		$data = array();
		
		foreach($document->getConfigurationElements() as $cfg) {
			// loop over <setting> elements; there can be many of them
			foreach($cfg->get('settings') as $setting) {
				$this->parseSetting($setting, $data);
			}
		}
		
		$code = sprintf(static::CONFIG_CLASS . '::fromArray(%s);', var_export($data, true));

		return $this->generate($code, $document->documentURI);
	}
	
	public function parseSetting($setting, &$data, $prefix = '') {
		if($prefix) {
			$prefix .= '.';
		}
		
		$name = $prefix . $setting->getAttribute('name');
		if($setting->has('settings')) {
			foreach($setting->get('settings') as $child) {
				$this->parseSetting($child, $data, $name);
			}
		} else{
			$value = $setting->hasAgaviParameters() ? $setting->getAgaviParameters() : AgaviToolkit::literalize($setting->getValue());
			$this->setRuntimeConfig($name, $value);
			$data[$name] = $value;
		}
	}
	
	protected function setRuntimeConfig($name, $value) {
		$cfgClass = static::CONFIG_CLASS;
		return $cfgClass::set($name, $value);
	}
	
}

?>