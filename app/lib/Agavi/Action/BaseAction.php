<?php

namespace Bitalo\Market\Agavi\Action;

use AgaviRequestDataHolder;
use AgaviUser;
use Bitalo\Market\Agavi\Core\BaseTrait;

/**
 * The base action from which all project actions inherit.
 */
class BaseAction extends \AgaviAction {

	use BaseTrait;

	public function isSecure() {
		return true;
	}

	public function isJavascriptValidated() {
		return false;
	}

	/**
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return array|mixed
	 */
	public function handleError(AgaviRequestDataHolder $rd) {
		return array('Default', 'ValidationError');
	}

	/**
	 * @param \AgaviExecutionContainer $container
	 */
	public function initialize(\AgaviExecutionContainer $container) {
		parent::initialize($container);

		if($this->isJavascriptValidated() && $container->getRequestMethod() == 'read') {
			$this->getContainer()->getResponse()->setAttribute('messages', $this->getErrorMessagesForRequestMethodMatchingParameterName(), 'bitalo.javascript.validation');
		}
	}


	/**
	 * @param string $requestMethod
	 *
	 * @param string $parameterName
	 *
	 * @return array
	 */
	protected  function getErrorMessagesForRequestMethodMatchingParameterName($requestMethod = 'write', $parameterName = 'ng-name') {
		$container = clone $this->getContainer();

		$container->setRequestMethod($requestMethod);
		$container->registerValidators();

		$errorMessages = array();
		foreach($container->getValidationManager()->getChilds() as $child) { /** @var $child \AgaviValidator */
			if($child->hasParameter($parameterName)) {
				$reflect = new \ReflectionClass($child);

				$errorMessageProperty = $reflect->getProperty('errorMessages');
				$errorMessageProperty->setAccessible(true);

				if(isset($errorMessages[$child->getParameter($parameterName)])) {
					$errorMessages[$child->getParameter($parameterName)] = array_merge($errorMessages[$child->getParameter($parameterName)], $errorMessageProperty->getValue($child));

				} else {
					$errorMessages[$child->getParameter($parameterName)] = $errorMessageProperty->getValue($child);

				}
			}

		}


		return $errorMessages;
	}

	/**
	 * @param      $name
	 * @param      $value
	 * @param bool $merge
	 */
	public function setJavascriptAttribute($name, $value, $merge = false) {
		$response = $this->getContainer()->getResponse();
		if($merge) {
			$curValue = $response->getAttribute($name, 'bitalo.javascript.attributes');
			if(is_array($curValue)) {
				$value = array_merge($curValue, $value);
			}
		}
		$response->setAttribute($name, $value, 'bitalo.javascript.attributes');
	}

	/**
	 * @param $attributes
	 */
	public function setJavascriptAttributes($attributes) {
		$response = $this->getContainer()->getResponse();
		$response->setAttributes($attributes, 'bitalo.javascript.attributes');
	}
}