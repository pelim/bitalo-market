<?php

namespace Bitalo\Market\Agavi\Core;

use Exception;

use AgaviContext;
use AgaviToolkit;
use AgaviConfig;
use AgaviDisabledModuleException;
use AgaviAutoloadException;
use AgaviException;

class Context extends AgaviContext {


	/**
	 * @param       $modelName
	 * @param null  $moduleName
	 * @param array $parameters
	 *
	 * @return \AgaviModel
	 * @throws \AgaviAutoloadException
	 */
	public function getModel($modelName, $moduleName = null, array $parameters = null) {
		$origModelName = $modelName;
		$modelName = AgaviToolkit::canonicalName($modelName);
		$class = str_replace('/', '_', $modelName) . 'Model';
		$file = null;
		$rc = null;

		if($moduleName === null) {
			// global model
			// let's try to autoload that baby
			if(!class_exists($class)) {
				// it's not there. the hunt is on
				$file = AgaviConfig::get('core.model_dir') . '/' . $modelName . 'Model.php';
			}
		} else {
			try {
				$this->controller->initializeModule($moduleName);
			} catch(AgaviDisabledModuleException $e) {
				// swallow, this will load the modules autoload but throw an exception
				// if the module is disabled.
			}
			// module model
			// alternative name
			$class = $moduleName . '_' . $class;
			// let's try to autoload the baby
			if(!class_exists($class)) {
				// it's not there. the hunt is on
				$file = AgaviConfig::get('core.module_dir') . '/' . $moduleName . '/models/' . $modelName . 'Model.php';
			}
		}

		if(null !== $file && is_readable($file)) {
			require($file);
		}

		if(!class_exists($class)) {
			// it's not there.
			throw new AgaviAutoloadException(sprintf("Couldn't find class for Model %s", $origModelName));
		}

		return parent::getModel($modelName, $moduleName, $parameters);
	}

	/**
	 * @param null|string $name
	 *
	 * @return \Predis\Client
	 */
	protected function getRedisConnection($name = 'redis') {

		return $this->getDatabaseConnection($name);
	}


}