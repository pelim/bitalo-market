<?php

namespace Bitalo\Market\Agavi\Core;

use AgaviController;
use AgaviWebRequestDataHolder;

class Controller extends AgaviController {

	public function createExecutionContainerWithWebRequestData($moduleName, $actionName, array $requestData, $outputType = null, $requestMethod = null) {

		$container = $this->getContext()->createInstanceFor('execution_container'); /** @var $container \AgaviExecutionContainer */
		$container->setModuleName($moduleName);
		$container->setActionName($actionName);
		$container->setRequestData(new AgaviWebRequestDataHolder(array(AgaviWebRequestDataHolder::SOURCE_PARAMETERS => $requestData)));
		$container->setOutputType($this->getContext()->getController()->getOutputType($outputType));
		if($requestMethod === null) {
			$requestMethod = $this->context->getRequest()->getMethod();
		}
		$container->setRequestMethod($requestMethod);

		return $container;
	}
}