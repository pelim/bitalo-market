<?php

namespace Bitalo\Market\Agavi\Core;


trait BaseTrait {

	/**
	 * @return \PaymentManagerModel
	 */
	public function getPaymentManger() {
		return $this->getModel('PaymentManager');
	}

	/**
	 * @return \TaskManagerModel
	 */
	public function getTaskManager() {
		return $this->getModel('TaskManager');
	}

	/**
	 * @return \NotificationManagerModel
	 */
	public function getNotificationManager() {
		return $this->getModel('NotificationManager');
	}


	/**
	 * @return \EventManagerModel
	 */
	public function getEventManager() {
		return $this->getModel('EventManager');
	}

	/**
	 * @param       $modelName
	 * @param null  $moduleName
	 * @param array $parameters
	 *
	 * @return \Bitalo\Market\Agavi\Model\BaseModel
	 */
	public function getModel($modelName, $moduleName = null, array $parameters = null) {
		return $this->getContext()->getModel($modelName, $moduleName, $parameters);
	}

	/**
	 * @return \Bitalo\Market\Logic\User\User;
	 */
	public function getAuthenticatedUser() {
		return $this->getContext()->getUser()->getUserObject();
	}


}