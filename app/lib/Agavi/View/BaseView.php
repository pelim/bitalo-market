<?php

namespace Bitalo\Market\Agavi\View;


use AgaviRequestDataHolder;
use AgaviViewException;
use AgaviView;
use Bitalo\Market\Logic\Item\CategoryFinder;

/**
 * The base view from which all project views inherit.
 */
class BaseView extends AgaviView {
	const SLOT_LAYOUT_NAME = 'slot';


	/**
	 * @var        \AgaviRouting
	 */
	protected $ro;

	/**
	 * @var        \AgaviRequest
	 */
	protected $rq;

	/**
	 * @var        \AgaviTranslationManager
	 */
	protected $tm;

	/**
	 * @var        \Bitalo\Market\Agavi\User\SessionUser
	 */
	protected $us;

	/**
	 * @param \AgaviExecutionContainer $container
	 */
	public function initialize(\AgaviExecutionContainer $container) {
		parent::initialize($container);

		$this->ro = $this->getContext()->getRouting();
		$this->rq = $this->getContext()->getRequest();
		$this->tm = $this->getContext()->getTranslationManager();
		$this->us = $this->getContext()->getUser();

		$this->setCommonJavascriptAttributes();

	}

	/**
	 *
	 */
	public function setCommonJavascriptAttributes() {

		$categories = array();
		foreach(CategoryFinder::findCategories() as $category) {
			$categories[] = array(
				'id' => $category->getId(),
				'identifier' => $category->getIdentifier(),
				'title' => $category->getTitle(),
			);
		}

		$currencies = [
			'EUR' => ['label' => 'EUR', 'symbol' => '€'],
			'USD' => ['label' => 'USD', 'symbol' => '$'],
			'GBP' => ['label' => 'GBP', 'symbol' => '£']
		];


		$this->setJavascriptAttributes(array(
			'categories' => $categories,
			'currencies' => $currencies,
			'network' => 'testnet',
		));

	}

	/**
	 * Handles output types that are not handled elsewhere in the view. The
	 * default behavior is to simply throw an exception.
	 *
	 * @param \AgaviRequestDataHolder $rd
	 *
	 * @throws \AgaviViewException if the output type is not handled.
	 * @internal param \Bitalo\Market\Agavi\View\The $AgaviRequestDataHolder request data associated with
	 *                                    this execution.
	 *
	 * @return \AgaviExecutionContainer|void
	 */
	public final function execute(AgaviRequestDataHolder $rd)
	{
		throw new AgaviViewException(sprintf(
			'The view "%1$s" does not implement an "execute%3$s()" method to serve '.
			'the output type "%2$s", and the base view "%4$s" does not implement an '.
			'"execute%3$s()" method to handle this situation.',
			get_class($this),
			$this->container->getOutputType()->getName(),
			ucfirst(strtolower($this->container->getOutputType()->getName())),
			get_class()
		));
	}

	/**
	 * Prepares the HTML output type.
	 *
	 * @param \AgaviRequestDataHolder $rd
	 * @param null|string             $layoutName
	 */
	public function setupHtml(AgaviRequestDataHolder $rd, $layoutName = null)
	{
		if($layoutName === null && $this->getContainer()->getParameter('is_slot', false)) {
			// it is a slot, so we do not load the default layout, but a different one
			// otherwise, we could end up with an infinite loop
			$layoutName = self::SLOT_LAYOUT_NAME;
		}

		// now load the layout
		// this method returns an array containing the parameters that were declared on the layout (not on a layer!) in output_types.xml
		// you could use this, for instance, to automatically set a bunch of CSS or Javascript includes based on layout parameters -->
		$this->loadLayout($layoutName);
	}

	/**
	 * @return array
	 */
	public function getCategoryList() {
		return CategoryFinder::findCategories();
	}

	/**
	 * @param      $name
	 * @param      $value
	 * @param bool $merge
	 */
	public function setJavascriptAttribute($name, $value, $merge = false) {
		$response = $this->getContainer()->getResponse();
		if($merge) {
			$curValue = $response->getAttribute($name, 'bitalo.javascript.attributes');
			if(is_array($curValue)) {
				$value = array_merge($curValue, $value);
			}
		}
		$response->setAttribute($name, $value, 'bitalo.javascript.attributes');
	}

	/**
	 * @param $attributes
	 */
	public function setJavascriptAttributes($attributes) {
		$response = $this->getContainer()->getResponse();
		$response->setAttributes($attributes, 'bitalo.javascript.attributes');
	}

	/**
	 * @return string
	 */
	public function renderJavascriptAttributes() {
		$response = $this->getContainer()->getResponse();
		return json_encode($response->getAttributes('bitalo.javascript.attributes'));
	}

	public function getJavascriptValidation() {
		$response = $this->getContainer()->getResponse();
		return $response->getAttribute('messages', 'bitalo.javascript.validation');
	}

	/**
	 * @return string
	 */
	public function hash() {
		/**
		 * @TODO: need real asset hashing
		 */
		return md5(mt_rand());
	}
}