<?php

namespace Bitalo\Market\Agavi\Database;

use AgaviDatabase;
use AgaviDatabaseManager;
use Predis;

class RedisDatabase extends AgaviDatabase {

	/** @var \Predis\Client */
	protected $connection;

	public function connect() {

		$this->connection = new Predis\Client([
			'scheme'             => $this->getParameter('scheme', 'tcp'),
			'host'               => $this->getParameter('host', '127.0.0.1'),
			'port'               => $this->getParameter('port', 6379),
			'prefix'             => $this->getParameter('prefix'),
			'read_write_timeout' => $this->getParameter('read_write_timeout', null)
		]);
	}

	public function shutdown() {
		if($this->connection) $this->connection->disconnect();
	}

}