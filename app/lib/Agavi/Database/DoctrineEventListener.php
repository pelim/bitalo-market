<?php

namespace Bitalo\Market\Agavi\Database;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;

class DoctrineEventListener implements EventSubscriber {

	public $preFooInvoked = false;



	public function getSubscribedEvents() {
		return array(Events::onFlush);
	}


}
