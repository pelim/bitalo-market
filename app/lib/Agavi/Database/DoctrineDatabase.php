<?php

namespace Bitalo\Market\Agavi\Database;

use Doctrine\Common\EventManager;

use Gedmo\Timestampable\TimestampableListener;
use Gedmo\Loggable\LoggableListener;

use AgaviContext;

class DoctrineDatabase extends \AgaviDoctrine2ormDatabase {

	protected function prepareEventManager(EventManager $eventManager) {
		$logger      = new LoggableListener();
		$sessionUser = AgaviContext::getInstance()->getUser(); /** @var \Bitalo\Market\Agavi\User\SessionUser */

		/*if($sessionUser->hasAttribute('userObjectId')) {
			$logger->setUsername((string) $sessionUser->getAttribute('userObjectId'));
		}*/


		$eventManager->addEventSubscriber($logger);
		$eventManager->addEventSubscriber(new TimestampableListener());
	}

}