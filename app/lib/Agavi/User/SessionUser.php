<?php

namespace Bitalo\Market\Agavi\User;

use Bitalo\Market\Logic\User\User;

use InvalidArgumentException;

class SessionUser extends \AgaviSecurityUser {

	/**
	 * @var User;
	 */
	protected $userObject;

	/**
	 * @return User
	 */
	public function getUserObject() {
		if(!$this->userObject && $this->hasAttribute('userObjectId')) {
			$this->userObject = new User($this->getAttribute('userObjectId'));
		}
		return $this->userObject;
	}

	/**
	 * @param \Bitalo\Market\Logic\User\User $user
	 * @return bool
	 */
	public function loginAs(User $user) {
		// check if there is already someone logged in and log him off!
		if($this->getUserObject()) {
			$this->logout();
		}

		$this->userObject = $user;
		$this->setAttribute('userObjectId', $user->getId());
		$this->setAuthenticated(true);

		return true;
	}

	/**
	 * @return bool
	 */
	public function logout() {
		$this->removeAttribute('userObjectId');
		$this->userObject = null;
		$this->setAuthenticated(false);

		$this->clearAttributes();

		return true;
	}

	/**
	 * @param $password
	 * @return string
	 * @throws \InvalidArgumentException
	 */
	public function hashPassword($password) {
		if(empty($password)) {
			throw new InvalidArgumentException('Don\'t hash empty passwords!');
		}

		return password_hash($password, PASSWORD_BCRYPT);
	}
}

?>