<?php

namespace Bitalo\Market\Agavi\Validator;

class PageValidator extends \AgaviValidator {

	const DEFAULT_PAGE_SIZE = 10;

	public function validate() {
		$pageNumber = (int) $this->getData($this->getArgument());

		// page number must be a positive value
		if(is_int($pageNumber) && $pageNumber > 0) {
			$pageSize = $this->getParameter('page_size', self::DEFAULT_PAGE_SIZE);
			// negative or zero page size is not allowed
			if($pageSize <= 0) {
				$this->throwError();
				return false;
			}
			$limit = (int) $pageSize;
			$offset = (int) ($pageNumber - 1) * $pageSize;
			$this->export($limit, $this->getParameter('export_limit'));
			$this->export($offset, $this->getParameter('export_offset'));
			return true;
		} else {
			$this->throwError();
			return false;
		}
	}
}