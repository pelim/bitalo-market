<?php

namespace Bitalo\Market\Agavi\Validator;


use AgaviArrayPathDefinition;

/**
 * Mark the argument (and if its an array all children, unlike the IssetValidator) as succeeded.
 */
class PassthruValidator extends \AgaviValidator {

	/**
	 * @return array
	 */
	protected function getFullArgumentNames() {
		$arguments = array();
		foreach($this->getArguments() as $argument) {
			if($argument) {
				$base = $this->curBase->pushRetNew($argument);
			} else {
				$base = $this->curBase;
			}
			$array =& $this->validationParameters->getAll($this->getParameter('source'));
			$value = $base->getValue($array);
			if(is_array($value)) {
				$arguments = array_merge($arguments, AgaviArrayPathDefinition::getFlatKeyNames($value, $base->__toString()));
			} else {
				$arguments[] = $base->__toString();
			}
		}

		return $arguments;
	}

	/**
	 * @param string $throwError
	 * @param bool
	 */
	protected function checkAllArgumentsSet($throwError = true) {
		return true;
	}

	/**
	 * @return bool
	 */
	protected function validate() {
		return true;
	}
}
