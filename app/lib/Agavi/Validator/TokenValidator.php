<?php

namespace Bitalo\Market\Agavi\Validator;

use Bitalo\Market\Logic\User\Token;

class TokenValidator extends \AgaviValidator {

	protected function validate() {
		$id = (string) $this->getData($this->getArgument());

		$token = new Token($id);

		if($token->isValid()) {
			$this->export($token);
			return true;
		} else {
			$this->throwError();
			return false;
		}
	}
}