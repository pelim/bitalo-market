<?php

namespace Bitalo\Market\Agavi\Validator;


use BitWasp\BitcoinLib\BitcoinLib;
use BitWasp\BitcoinLib\BIP32;

class BitcoinAddressValidator extends \AgaviValidator {

	protected function validate() {
		$address = (string) $this->getData($this->getArgument());

		if(BitcoinLib::validate_address($address, BIP32::$bitcoin_testnet_public)) {
			return true;
		} else {
			return false;
		}
	}
}