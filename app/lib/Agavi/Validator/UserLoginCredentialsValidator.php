<?php

namespace Bitalo\Market\Agavi\Validator;

use Bitalo\Market\Logic\User\UserFinder;

use AgaviValidator;

class UserLoginCredentialsValidator extends AgaviValidator {

	protected function validate() {
		$email    = (string) trim($this->getData('email'));
		$password = (string) $this->getData('password');

		$user = UserFinder::findForEmail($email);


		if($user && password_verify($password, $user->getPassword())) {
			$this->export($user);
			return true;

		} else {
			$this->throwError('invalid');
			return false;
		}
	}

}

?>
