<?php

namespace Bitalo\Market\Agavi\Validator;


class ObjectValidator extends \AgaviValidator {

	protected function validate() {
		$id = (string) $this->getData($this->getArgument());

		$objectClassname = $this->getParameter('objectClassname');

		try {
			$object = new $objectClassname($id);
		} catch(\Exception $e) {
			// we handle this later
		}

		if(isset($object)) {
			$this->export($object);
			return true;
		} else {
			$this->throwError();
			return false;
		}
	}
}
?>