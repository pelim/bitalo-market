<?php

namespace Bitalo\Market\Agavi\Validator;

use Bitalo\Market\Logic\User\UserFinder;

class UniqueEmailValidator extends \AgaviValidator {

	protected function validate() {
		$email = (string) $this->getData($this->getArgument());

		$user = UserFinder::findForEmail($email);

		if(!$user) {
			$this->export($user);
			return true;
		} else {
			$this->throwError('unique');
			return false;
		}
	}
}