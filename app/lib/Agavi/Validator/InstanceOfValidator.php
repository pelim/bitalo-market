<?php

namespace Bitalo\Market\Agavi\Validator;

class InstanceOfValidator extends \AgaviValidator {

	protected function validate() {
		$object    = $this->getData($this->getArgument());
		$className = $this->getParameter('class');

		if($object instanceof $className) {
			return true;

		} else {
			$this->throwError();
			return false;
		}
	}

}

?>