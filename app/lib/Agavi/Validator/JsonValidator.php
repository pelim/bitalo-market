<?php

namespace Bitalo\Market\Agavi\Validator;


class JsonValidator extends \AgaviValidator {

	protected function validate() {
		$json = (string) $this->getData($this->getArgument());

		$assoc = $this->getParameter('assoc', true);

		try {
			$object = json_decode($json, $assoc);
		} catch(\Exception $e) {
			// we handle this later
		}

		if(isset($object)) {
			$this->export($object);
			return true;
		} else {
			$this->throwError();
			return false;
		}
	}
}