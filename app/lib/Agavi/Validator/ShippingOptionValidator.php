<?php

namespace Bitalo\Market\Agavi\Validator;


use Bitalo\Market\Logic\Item\ItemShippingOption;

class ShippingOptionValidator extends \AgaviValidator {

	protected function validate() {
		$id = (string) $this->getData($this->getArgument());

		$itemParameter = $this->getParameter('item_parameter');

		$item = $this->getData($itemParameter); /** @var $item \Bitalo\Market\Logic\Item\Item */

		$shippingOptions = $item->getShippingOptions();

		if(isset($shippingOptions[$id])) {
			$this->export(new ItemShippingOption(array_merge(array('id' => $id), $shippingOptions[$id])));
			return true;
		} else {
			$this->throwError();
			return false;
		}

	}
}