<?php

namespace Bitalo\Market\Agavi\Model;

/**
 * The base model from which all project models inherit.
 */
abstract class TaskModel extends BaseModel {

	abstract public function perform(array $arguments = array());
}