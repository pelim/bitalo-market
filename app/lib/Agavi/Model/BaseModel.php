<?php

namespace Bitalo\Market\Agavi\Model;

use Bitalo\Market\Agavi\Core\BaseTrait;

/**
 * The base model from which all project models inherit.
 */
class BaseModel extends \AgaviModel {

	use BaseTrait;

	private $singletonInitialized = false;


	public function initialize(\AgaviContext $context, array $parameters = array()) {
		parent::initialize($context, $parameters);

		// Add support for an initializeOnce() method that will get called
		// on the first initialization of a singleton model (this is different
		// to just using __construct() as there would be no AgaviContext given)
		if($this instanceof \AgaviISingletonModel && !$this->singletonInitialized) {
			$this->singletonInitialized = true;
			$this->initializeOnce($context, $parameters);
		}
	}

	public function initializeOnce(\AgaviContext $context, array $parameters = array()) {
		// Nothing implemented here
	}

}