<?php

namespace Bitalo\Market\Agavi\Routing;

class ConsoleRouting extends \AgaviConsoleRouting {


	public function gen($route, array $params = array(), $options = array()) {
		return $this->getContext()->getInstance('web-from-console')->getRouting()->gen($route, $params, $options);
	}

}