<?php

namespace Bitalo\Market\Agavi\Request;

use AgaviWebRequest;
use AgaviContext;

class RestRequest extends AgaviWebRequest {
	public function initialize(AgaviContext $context, array $parameters = array()) {
		parent::initialize($context, $parameters);
		$rd = $this->getRequestData();

		//handle XML requests
		if(stristr($rd->getHeader('Content-Type'), 'text/xml')) {
			switch ($_SERVER['REQUEST_METHOD']) {
				case 'PUT': {
					$xml = $rd->removeFile('put_file')->getContents();
					break;
				}
				case 'POST': {
					$xml = $rd->removeFile('post_file')->getContents();
					break;
				}
				default: {
				$xml = '';
				}
			}
			$rd->setParameters((array)simplexml_load_string($xml));
		}
		//handle JSON requests
		if(stristr($rd->getHeader('Content-Type'), 'application/json')) {
			switch ($_SERVER['REQUEST_METHOD']) {
				case 'PUT': {
					$json = $rd->removeFile('put_file')->getContents();
					break;
				}
				case 'POST': {
					$json = $rd->removeFile('post_file')->getContents();
					break;
				}
				default: {
				$json = '{}';
				}
			}
			$rd->setParameters(json_decode($json, true));
		}

		//handle multipart request
		if(stristr($rd->getHeader('Content-Type'), 'multipart/form-data')) {
			foreach($rd->getFiles() as $name => $data) {
				if(is_object($data) && $data->getType() == 'application/json'){
					$json = $rd->removeFile($name)->getContents();
					$rd->setParameters(json_decode($json, true));

				}
			}
		}

	}
}
?>