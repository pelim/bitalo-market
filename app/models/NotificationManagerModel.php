<?php

use Bitalo\Market\Agavi\Model;
use Bitalo\Market\Logic\User\User;
use Bitalo\Market\Agavi\Config\Config;
class NotificationManagerModel extends Model\BaseModel implements AgaviISingletonModel {

	const USER_NOTIFICATIONS_KEY = 'user:%s:notifications:%s';

	const NOTIFICATION_TYPE_CONVERSATION = 'conversation';

	const NOTIFICATION_TYPE_ORDER = 'order';

	const NOTIFICATION_TYPE_PURCHASE = 'purchase';

	protected $config;

	/**
	 * @param AgaviContext $context
	 * @param array        $parameters
	 */
	public function initialize(AgaviContext $context, array $parameters = array())  {
		parent::initialize($context, $parameters);
		$this->config = include(AgaviConfigCache::checkConfig(AgaviConfig::get('core.config_dir') . '/custom/emails.xml'));
	}


	/**
	 * @return \Predis\Client
	 */
	protected function getRedis() {
		return $this->getContext()->getDatabaseConnection('redis');
	}

	/**
	 * @return array
	 */
	protected function getUserNotificationTypes() {
		return array(
			self::NOTIFICATION_TYPE_CONVERSATION,
			self::NOTIFICATION_TYPE_ORDER,
			self::NOTIFICATION_TYPE_PURCHASE
		);
	}

	/**
	 * @param User $user
	 * @param      $type
	 */
	public function increaseUserNotificationForType(User $user, $type) {
		$this->getRedis()->zincrby(sprintf(self::USER_NOTIFICATIONS_KEY, $user->getId()), 1, $type);
	}

	/**
	 * @param User                                         $user
	 * @param \Bitalo\Market\Logic\Object\BaseNotification $object
	 */
	public function setUnreadNotificationForObject(User $user, \Bitalo\Market\Logic\Object\BaseNotification $object) {
		$this->getRedis()->zadd(sprintf(self::USER_NOTIFICATIONS_KEY, $user->getId(), $object->getNotificationType()), time(), $object->getNotificationKey());
	}

	/**
	 * @param User                                        $user
	 * @param Bitalo\Market\Logic\Object\BaseNotification $object
	 *
	 * @internal param $type
	 *
	 * @return int
	 */
	public function getUserNotificationCountForObject(User $user, \Bitalo\Market\Logic\Object\BaseNotification $object) {
		return $this->getRedis()->zscore(sprintf(self::USER_NOTIFICATIONS_KEY, $user->getId()), $type);
	}

	/**
	 * @param User $user
	 *
	 * @return int
	 */
	public function getUserNotificationCount(User $user) {
		$cnt = 0;
		foreach($this->getUserNotificationTypes() as $type) {
			$cnt += $this->getUserNotificationCountForType($user, $type);
		}

		return $cnt;
	}

	/**
	 * @param User  $user
	 * @param       $id
	 * @param array $attributes
	 */
	public function enqueueEmail(User $user, $id, array $attributes = array()) {
		if(isset($this->config['emails'][$id])) {
			try {

				$emailConfig = $this->config['emails'][$id];

				// check parameters
				foreach($attributes as $name => &$value) {
					if(isset($emailConfig['attributes'][$name])) {
						if(isset($emailConfig['attributes'][$name]['class'])) {
							$className = $emailConfig['attributes'][$name]['class'];
							if(!$value instanceof $className) {
								throw new InvalidArgumentException(sprintf('Expected %s but got %s', $className, get_class($value)));
							} else {
								$value = serialize($value);
							}
						}
					} else {
						throw new InvalidArgumentException(sprintf('Invalid attribute %s', $name));
					}
				}


				$taskPayload = array(
					'id'         => $id,
					'user'       => $user->getId(),
					'attributes' => $attributes,
				);

				$this->getContext()->getModel('TaskManager')->schedule('notification.email.send', $taskPayload, TaskManagerModel::PRIORITY_NORMAL);


			} catch(Exception $e) {
				$this->getContext()->getLoggerManager()->log($e->getMessage() . $e->getTraceAsString(), AgaviILogger::ERROR);

			}

		}
	}

	/**
	 * @param array $parameters
	 *
	 * @return bool
	 */
	public function sendMail(array $parameters = array()) {
		if(isset($parameters['id']) &&  isset( $parameters['user']) && isset( $parameters['attributes'])) {
			$id         = $parameters['id'];
			$user       = new User($parameters['user']);
			$attributes = $parameters['attributes'];

			foreach($attributes as $name => &$value) {
				$value = unserialize($value);
			}

			$container = $this->getContext()->getController()->createExecutionContainerWithWebRequestData( /** @var $container \AgaviExecutionContainer */
				'Notification',
				'Email.Render',
				array(
					'template'   => $this->config['emails'][$id]['template'],
					'user'       => $user,
					'parameters' => $attributes,
				),
				'notification'
			);

			try {
				$container->execute();
			} catch(Exception $e) {
				$this->getContext()->getLoggerManager()->log($e->getMessage() . $e->getTraceAsString(), AgaviILogger::ERROR);

				return false;
			}

			$transport = Swift_SmtpTransport::newInstance(BitaloConfig::get('mail.smtp.host'), BitaloConfig::get('mail.smtp.port'), BitaloConfig::get('mail.smtp.security'))
				->setUsername(BitaloConfig::get('mail.smtp.user'))
				->setPassword(BitaloConfig::get('mail.smtp.password'));

			$mailer = new Swift_Mailer($transport);

			// Create a message
			$message = Swift_Message::newInstance($container->getAttribute('subject'))
				->setFrom(BitaloConfig::get('mail.sender.address'))
				->setTo($user->getEmail())
				->setBody($container->getResponse()->getContent());

			// Send the message
			$mailer->send($message);


		} else {
			// @TODO: add logging

		}

	}
}
