<?php

use Bitalo\Market\Agavi\Model;

use Bitalo\Market\Logic\User\User;
use Bitalo\Market\Logic\Item\ItemPurchase;

class EventHandler_ProcessItemPurchaseModel extends  Model\BaseModel {

	const EVENT_ITEM_PURCHASED = 'item.purchase.status.purchased';

	const EVENT_ITEM_RECEIVED  = 'item.purchase.status.received';

	const EVENT_ITEM_PAYED     = 'item.purchase.status.payed';

	const EVENT_ITEM_SHIPPED   = 'item.purchase.status.shipped';

	public static $ALLOWED_EVENTS = array(
		self::EVENT_ITEM_PURCHASED,
		self::EVENT_ITEM_RECEIVED,
		self::EVENT_ITEM_PAYED,
		self::EVENT_ITEM_SHIPPED
	);

	/**
	 * @param                      $eventId
	 * @param User                 $user
	 * @param                      $emitter
	 * @param ItemPurchase         $acceptor
	 * @param AgaviParameterHolder $additionalParameters
	 */
	public function process($eventId, User $user, $emitter, $acceptor = null, AgaviParameterHolder $additionalParameters = null) {
		if(in_array($eventId, self::$ALLOWED_EVENTS)) {
			$purchase = $acceptor; /** @var $purchase ItemPurchase */
			$processOwner = $purchase->getProcessOwner();

			$this->getNotificationManager()->enqueueEmail($processOwner, $eventId, array(
				'user'     => $processOwner,
				'purchase' => $purchase
			));

			$this->getNotificationManager()->setUnreadNotificationForObject($processOwner, $purchase);


		}
	}


}