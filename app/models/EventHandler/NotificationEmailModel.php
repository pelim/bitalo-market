<?php

use Bitalo\Market\Agavi\Model;

use Bitalo\Market\Logic\User\User;
use Bitalo\Market\Logic\Item\ItemPurchase;

class EventHandler_NotificationEmailModel extends  Model\BaseModel {


	/**
	 * @param                      $eventId
	 * @param User                 $user
	 * @param                      $emitter
	 * @param ItemPurchase         $acceptor
	 * @param AgaviParameterHolder $additionalParameters
	 */
	public function process($eventId, User $user, $emitter, $acceptor = null, AgaviParameterHolder $additionalParameters = null) {


	}


}