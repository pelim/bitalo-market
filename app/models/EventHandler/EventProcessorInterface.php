<?php

use Bitalo\Market\Logic\User\User;

interface EventHandler_EventProcessorInterface {

	public function process($eventId, User $user, $emitter, $acceptor = null, AgaviParameterHolder $additionalParameters = null);

}