<?php

use Bitalo\Market\Agavi\Model;

use Bitalo\Market\Logic\Payment\PaymentAddressFinder;
use Bitalo\Market\Logic\Item\ItemPurchaseFinder;
use Bitalo\Market\Agavi\Config\Config;

class Task_FetchAddressBalanceModel extends Model\TaskModel {

	const TASK_NAME = 'fetch.address.balance';

	public function perform(array $arguments = array()) {

		if($data = $this->getPaymentManger()->fetchAddress($arguments['address'])) {
			$paymentAddress = PaymentAddressFinder::findPaymentAddressForHex($data->addrStr); /** @var $paymentAddress \Bitalo\Market\Logic\Payment\PaymentAddress */

			foreach($data->transactions as $transaction) {
				$transactionData = $this->getPaymentManger()->fetchTransactionDetails($transaction);
				$paymentAddress->setConfirmationCount($transactionData->confirmations);
			}

			$paymentAddress->setBalance($data->balance);
			$paymentAddress->setUnconfirmedBalance($data->unconfirmedBalance);
			$paymentAddress->save();

			if($paymentAddress->getConfirmationCount() >= Config::get('bitalo.transaction.confirmation.count')) {

				$this->getPaymentManger()->removeConfirmationSubscription($paymentAddress->getAddress());

				$itemPurchase = ItemPurchaseFinder::findPurchaseByPaymentAddress($paymentAddress); /** @var $itemPurchase \Bitalo\Market\Logic\Item\ItemPurchase */
				$itemPurchase->setStatus(\Bitalo\Market\Logic\Item\ItemPurchase::STATUS_PAYED);
				$itemPurchase->save();

				$eventManager = \AgaviContext::getInstance()->getModel('EventManager'); /** @var $eventManager \EventManagerModel */
				$eventManager->fire($itemPurchase->getUser(), \Bitalo\Market\Logic\Item\ItemPurchase::EVENT_ITEM_PAYED, $itemPurchase->getUser(), $itemPurchase);
			} else {

			}

		}
	}
}