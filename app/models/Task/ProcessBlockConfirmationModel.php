<?php

use Bitalo\Market\Agavi\Model;

class Task_ProcessBlockConfirmationModel extends  Model\TaskModel {

	const TASK_NAME = 'process.block.confirmation';

	const SUBSCRIBED_ADDRESSES_LIST = 'payment:subscribed_addresses';

	public function perform(array $arguments = array()) {

		$redis = $this->getContext()->getDatabaseConnection('redis'); /** @var $redis Predis\Client $data */

		$data = $this->getContext()->getModel('PaymentManager')->fetchBlock($arguments['blockHash']);

		if($data) {
			$addresses = array_filter($redis->mget($data->tx));
			if($addresses) {
				$this->getPaymentManger()->subscribeConfirmations($addresses);
			}

			$addresses = array_merge($addresses, $this->getPaymentManger()->listSubscribedConfirmations());

			foreach($addresses as $address) {
				if($address) {
					$this->getContext()->getModel('TaskManager')->schedule('fetch.address.balance',  array('address' => $address), TaskManagerModel::PRIORITY_HIGH);
				}
			}
		}
	}
}