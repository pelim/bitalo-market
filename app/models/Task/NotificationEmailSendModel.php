<?php

use Bitalo\Market\Agavi\Model;

use Bitalo\Market\Logic\Payment\PaymentAddressFinder;
use Bitalo\Market\Logic\Item\ItemPurchaseFinder;

class Task_NotificationEmailSendModel extends Model\TaskModel {

	const TASK_NAME = 'notification.email.send';

	public function perform(array $arguments = array()) {

		AgaviConfig::set('core.default_context', 'console-doctrine');
		try {
			$notificationManager = $this->getContext()->getModel('NotificationManager'); /** @var NotificationManagerModel $notificationManager */
			$notificationManager->sendMail($arguments);
		} catch(Exception $e) {
			// @TODO: add logging
		}
	}
}