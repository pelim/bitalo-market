<?php

use Bitalo\Market\Agavi\Model;

use Bitalo\Market\Logic\User\User;

class EventManagerModel extends Model\BaseModel implements AgaviISingletonModel {

	protected $events;

	public function initializeOnce(AgaviContext $context, array $parameters = array()) {
		parent::initializeOnce($context, $parameters);

		$config = include(AgaviConfigCache::checkConfig(AgaviConfig::get('core.config_dir') . '/custom/events.xml'));
		$this->events = $config['events'];
	}

	public function fire(User $user, $eventName, $emitter = null, $acceptor = null, AgaviParameterHolder $additionalParameters = null) {
		if(isset($this->events[$eventName])) {
			$eventConfig = $this->events[$eventName];

			if(!$emitter instanceof $eventConfig['emitter']['class']) {
				throw new InvalidArgumentException('Wrong emitter supplied. Expected "' . $eventConfig['emitter']['class'] . '" got "' . get_class($emitter) . '"');
			}
			if($acceptor && !$acceptor instanceof $eventConfig['acceptor']['class']) {
				throw new InvalidArgumentException('Wrong acceptor supplied. Expected "' . $eventConfig['acceptor']['class'] . '" got "' . get_class($acceptor) . '"');
			}

			if(isset($eventConfig['handlers'])) {
				$handlers = $eventConfig['handlers'];
				if(is_array($handlers)) {
					foreach($handlers as $handler) {
						$this->callHandler($handler, $eventName, $user, $emitter, $acceptor, $additionalParameters);
					}
				}
			}
		}
	}

	public function callHandler($handler, $eventName, $user, $emitter, $acceptor, $additionalParameters) {
		try {
			$model = $this->getContext()->getModel('EventHandler/' . $handler);

			$model->process($eventName, $user, $emitter, $acceptor, $additionalParameters);
		} catch(Exception $e) {
			$this->getContext()->getLoggerManager()->log($e->getMessage() . $e->getTraceAsString(), AgaviILogger::ERROR);

		}
	}
}