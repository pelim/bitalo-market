<?php

use Bitalo\Market\Agavi\Model;

class PaymentManagerModel extends Model\BaseModel implements AgaviISingletonModel {


	const BITALO_API_ENDPOINT = 'http://localhost:3001/api/';

	/**
	 * @var \Predis\Client
	 */
	protected $redis;

	/**
	 * @param AgaviContext $context
	 * @param array        $parameters
	 */
	public function initializeOnce(AgaviContext $context, array $parameters = array()) {
		parent::initializeOnce($context, $parameters);
		$this->redis = $this->getContext()->getDatabaseConnection('subscribe');

	}

	/**
	 * @param $address
	 */
	public function subscribeAddress($address) {
		$this->redis->publish('address', $address);
	}

	/**
	 * @param array $addresses
	 */
	public function subscribeConfirmations(array $addresses) {
		$this->redis->sadd('tx:confirmation', $addresses);
	}

	/**
	 * @return array
	 */
	public function listSubscribedConfirmations() {
		return $this->redis->smembers('tx:confirmation');
	}

	/**
	 * @param $address
	 */
	public function removeConfirmationSubscription($address) {
		$this->redis->srem('tx:confirmation', $address);
	}

	/**
	 * @param $address
	 *
	 * @return mixed
	 */
	public function fetchAddress($address) {
		$response = Httpful\Request::get(sprintf('http://localhost:3001/api/addr/%s', $address))->send();
		if(!$response->hasErrors()) {
			return $response->body;
		}
	}

	/**
	 * @param $address
	 *
	 * @return mixed
	 */
	public function getUnspentTransactions($address) {
		$response = Httpful\Request::get(sprintf('http://localhost:3001/api/addr/%s/utxo', $address))->send();
		if(!$response->hasErrors()) {
			#$this->redis->set($key, json_encode($response->body));
			return $response->body;
		}
	}

	/**
	 * @param $blockHash
	 *
	 * @return mixed
	 */
	public function fetchBlock($blockHash) {
		$response = Httpful\Request::get(sprintf('http://localhost:3001/api/block/%s', $blockHash))->send(); /** @var $sub \Predis\Client */
		if(!$response->hasErrors()) {
			return $response->body;
		}
	}

	/**
	 * @param $tx
	 *
	 * @return mixed
	 */
	public function fetchTransactionDetails($tx) {
		$response = Httpful\Request::get(sprintf('http://localhost:3001/api/tx/%s', $tx))->send(); /** @var $sub \Predis\Client */
		if(!$response->hasErrors()) {
			return $response->body;
		}
	}


	protected function getApiEndpoint() {
		return self::BITALO_API_ENDPOINT;
	}

	public function createTransactionFromWalletAddress($address, array $outputs) {
		$response = Httpful\Request::post('http://localhost:3001/api/tx/create', array('address' => $address, 'output' => $outputs))->sendsJson()->send();
		if(!$response->hasErrors()) {
			return $response->body;
		}
	}

	/**
	 * @param array $keys
	 * @param       $number
	 *
	 * @return mixed
	 */
	public function createMultiSignatureAddress(array $keys, $number) {
		$response = Httpful\Request::post('http://localhost:3001/api/addr/create', array('keys' => $keys, 'number' => $number))->sendsJson()->send();
		if(!$response->hasErrors()) {
			return $response->body;
		}
	}

	/**
	 * @param $transaction
	 *
	 * @return mixed
	 */
	public function sendTransaction($transaction) {
		$response = Httpful\Request::post('http://localhost:3001/api/tx/send', array('rawtx' => $transaction))->sendsJson()->send();
		if(!$response->hasErrors()) {
			return $response->body;
		}
	}

}