<?php

use Bitalo\Market\Agavi\Model;

use Bitalo\Market\Utility\StringToolbox;

class TaskManagerModel extends Model\BaseModel implements AgaviISingletonModel {

	/**
	 * @var \Predis\Client
	 */
	protected $redis;

	/**
	 * @const string
	 *
	 * Low priority queue
	 */
	const PRIORITY_HIGH   = 'queue.priority.high';

	/**
	 * @const string
	 *
	 * Normal priority queue
	 */
	const PRIORITY_NORMAL = 'queue.priority.normal';

	/**
	 * @const string
	 *
	 * High priority queue
	 */
	const PRIORITY_LOW    = 'queue.priority.low';

	/**
	 * @const int
	 *
	 * Blocking timeout (ttl)
	 */
	const BLOCK_TIMEOUT = 10;

	/**
	 * @const string
	 *
	 * Pattern for TaskModel
	 */
	const TASK_MODEL_PATTERN = 'Task.%s';


	/**
	 * @param AgaviContext $context
	 * @param array        $parameters
	 */
	public function initializeOnce(AgaviContext $context, array $parameters = array()) {
		parent::initializeOnce($context, $parameters);
		$this->redis = $this->getContext()->getDatabaseConnection('redis');

	}

	/**
	 * @param        $name
	 * @param array  $arguments
	 * @param string $priority
	 */
	public function schedule($name, array $arguments = array(), $priority = self::PRIORITY_NORMAL) {

		$model = sprintf(self::TASK_MODEL_PATTERN, StringToolbox::convertDotToCamelcase($name));

		$this->redis->rpush($priority, json_encode((object) array(
			'name' => $name,
			'model' => $model,
			'arguments' => $arguments
		)));
	}

	/**
	 * @return mixed
	 */
	public function getNext() {
		return $this->redis->blpop(self::PRIORITY_HIGH, self::PRIORITY_NORMAL, self::PRIORITY_LOW, self::BLOCK_TIMEOUT);
	}
}