<?php

use Bitalo\Market\Agavi\Model;

use Bitalo\Market\Utility\StringToolbox;

class ExchangeRatesModel extends Model\BaseModel implements AgaviISingletonModel {

	/**
	 * @var \Predis\Client
	 */
	protected $redis;

	/**
	 * @var array
	 */
	protected $rates;

	const EXCHANGE_RATES_CACHE_TIME = 900; // 60s * 15m

	const EXCHANGE_RATES_CACHE_KEY = 'payment:currency:exchange';

	/**
	 * @param AgaviContext $context
	 * @param array        $parameters
	 */
	public function initializeOnce(AgaviContext $context, array $parameters = array()) {
		parent::initializeOnce($context, $parameters);
		$this->redis = $this->getContext()->getDatabaseConnection('redis');

	}


	public function updateExchangeRates() {

		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, 'http://blockchain.info/ticker');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$result = (array) json_decode(curl_exec($curl));


		foreach($result as $currency => &$rates) {
			$rates = $rates->last;
		}

		$this->redis->hmset(self::EXCHANGE_RATES_CACHE_KEY, $result);

		$this->redis->expire(self::EXCHANGE_RATES_CACHE_KEY, self::EXCHANGE_RATES_CACHE_TIME);


		return $result;
	}

	public function getExchangeRates() {
		if(!$this->redis->exists(self::EXCHANGE_RATES_CACHE_KEY)) {
			$this->rates = self::updateExchangeRates();
		} elseif(!count($this->rates)) {
			$this->rates = $this->redis->hgetall(self::EXCHANGE_RATES_CACHE_KEY);
		}

		return $this->redis->hgetall(self::EXCHANGE_RATES_CACHE_KEY);

	}

	public function getExchangeRate($currency) {
		if(!$this->redis->exists(self::EXCHANGE_RATES_CACHE_KEY)) {
			self::updateExchangeRates();
		}
		return $this->redis->hget(self::EXCHANGE_RATES_CACHE_KEY, strtolower($currency));
	}

	public function getConfiguredCurrencies() {
		return explode(',', \Bitalo\Market\Agavi\Config\Config::get('bitalo.currencies'));
	}
}

