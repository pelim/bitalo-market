<?php

use Bitalo\Market\Agavi\View;
use Bitalo\Market\Logic\Item\CategoryFinder;

class Marketplace_ItemsSuccessView extends View\MarketplaceBaseView {

	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);


		$this->appendLayer($this->createLayer('AgaviFileTemplateLayer', 'wrapped'), $this->getLayer('content'));
		$wrapped = $this->getLayer('wrapped');
		$wrapped->setParameter('template',  'Items.wrapper');
		$this->setAttribute('withoutContainer', true);

		$this->setJavascriptAttribute('items', $this->getItemsArray());

		$sortOptions = array(
			array(
				'label' => 'newly listed',
				'group' => 'Time',
				'order' => 'Item.createdAt',
				'sort' => 'DESC'
			),
			array(
				'label' => 'ending soon',
				'group' => 'Time',
				'order' => 'Item.createdAt',
				'sort' => 'ASC'
			),
			array(
				'label' => 'lowest',
				'group' => 'Price',
				'order' => 'Item.price',
				'sort' => 'ASC'
			),
			array(
				'label' => 'highest',
				'group' => 'Price',
				'order' => 'Item.price',
				'sort' => 'DESC'
			),
		);

		$this->setJavascriptAttribute('sortOptions', $sortOptions);

		$this->setAttribute('categories', CategoryFinder::findCategories());

	}


	public function executeAjaxHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);
	}

	protected function getItemsArray() {

		$items = $this->getAttribute('items');

		$jsonItems = array();

		$currencies = $this->getContext()->getModel('ExchangeRates')->getConfiguredCurrencies();

		foreach($items as $item) {
			foreach($currencies as $currency) {
				$price[strtolower($currency)] = $item->getPriceForCurrency($currency);
			}

			$price['btc'] = $item->getPriceBtc();

			$json = array(
				'id'     => $item->getId(),
				'title'  => $item->getTitle(),
				'prices' => $price,
				'url' => $this->ro->gen('items.item', array('itemId' => $item->getId())),
				'image'  => array(
					'200x200' => $item->getImage()->getThumbnail(200,200)
				)
			);

			$jsonItems[] = $json;
		}

		return $jsonItems;
	}


	public function executeAjaxJson(AgaviRequestDataHolder $rd) {
		return json_encode($this->getItemsArray());
	}
}