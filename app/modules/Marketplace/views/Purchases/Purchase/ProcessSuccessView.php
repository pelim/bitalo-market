<?php

use Bitalo\Market\Agavi\View;

class Marketplace_Purchases_Purchase_ProcessSuccessView extends View\MarketplaceBaseView {

	/**
	 * @param AgaviRequestDataHolder $rd
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$purchase = $this->getAttribute('purchase');
		$this->getResponse()->setRedirect($this->getContext()->getRouting()->gen('purchases.purchase', array('purchaseId' => $purchase->getId())));

	}

	/**
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return string
	 */
	public function executeAjaxJson(AgaviRequestDataHolder $rd) {

		$purchase = $this->getAttribute('purchase');

		return json_encode(array(
			'success' => true,
			'redirect' => $this->getContext()->getRouting()->gen('purchases.purchase', array('purchaseId' => $purchase->getId())),
		));
	}
}