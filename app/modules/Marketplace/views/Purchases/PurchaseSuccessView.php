<?php

use Bitalo\Market\Agavi\View;
use Bitalo\Market\Logic\Item\ItemPurchase;

class Marketplace_Purchases_PurchaseSuccessView extends View\MarketplaceBaseView {

	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {

		$processTemplateMap = array(
			ItemPurchase::STATUS_PURCHASED  => 'ProcessPayment',
			ItemPurchase::STATUS_PAYED      => 'ProcessShipping',
			ItemPurchase::STATUS_SHIPPED    => 'ProcessReceived'
		);

		$purchase = $this->getAttribute('purchase'); /** @var $purchase \Bitalo\Market\Logic\Item\ItemPurchase */


		$this->setupHtml($rd);

		if(isset($processTemplateMap[$purchase->getStatus()])) {
			//if($purchase->isProcessAllowedByUser($this->getContext()->getUser()->getUserObject())) {
				$this->getLayer('content')->setTemplate('Purchases/Purchase/' . $processTemplateMap[$purchase->getStatus()]);
			//} else {
			//	$this->getLayer('content')->setTemplate('Purchases/Purchase/ProcessWaiting');
			//}

		} else {
			$this->getLayer('content')->setTemplate('Purchases/Purchase/ProcessWaiting');

		}
	}

	public function isProcessOwner() {
		return ( $this->getAttribute('purchase')->isProcessAllowedByUser($this->getContext()->getUser()->getUserObject()));
	}

	public function getPaymentAddressLink(ItemPurchase $purchase) {
		return sprintf('bitcoin:%s?amount=%s&label=%s', $purchase->getWallet()->getAddress(), $purchase->getPurchaseTotalBtc(), $purchase->getItem()->getTitle());

	}
}