<?php

use Bitalo\Market\Agavi\View;
use Bitalo\Market\Logic\Item\Item;

class Marketplace_ListingsSuccessView extends View\MarketplaceBaseView {

	/**
	 * @param AgaviRequestDataHolder $rd
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);
	}

	/**
	 * @param Item $item
	 *
	 * @return float
	 */
	public function getStockProgressForItem(Item $item) {
		return ($item->getAvailableQuantity() / $item->getQuantity()) * 100;
	}

	public function mapProgressToCssClass($progress) {
		if($progress <= 25) {
			return 'progress-bar-danger';
		} elseif($progress <= 50) {
			return 'progress-bar-info';
		} elseif($progress <= 75) {
			return 'progress-bar-warning';
		} else {
			return 'progress-bar-success';
		}
	}

}