<?php

use Bitalo\Market\Agavi\View;

class Marketplace_Items_ItemSuccessView extends View\MarketplaceBaseView {

	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {

		$item = $this->getAttribute('item'); /** @var $item \Bitalo\Market\Logic\Item\Item */

		$this->setupHtml($rd);

		$price = array();

		foreach($this->getContext()->getModel('ExchangeRates')->getConfiguredCurrencies() as $currency) {
			$price[strtolower($currency)] = $item->getPriceForCurrency($currency);
		}

		$price['btc'] = $item->getPriceBtc();

		$images = array();

		foreach($item->getImages() as $image) {
			$images[] = array(
				'400x400' => $image->getThumbnail(400,400),
				'60x60'   => $image->getThumbnail(60,60)
			);
		}

		$shippingOptions = $item->getShippingOptions();


		$this->setJavascriptAttributes(array(
			'item' => array(
				'images'   => $images,
				'price'    => $price,
				'quantity' => array(
					'min' => 1,
					'max' => $item->getAvailableQuantity()
				),
				'user' => array(
					'id' => $item->getUser()->getId()
				),
				'shipping' => array(
					'options' => $shippingOptions,
					'regions' => []
				)
			)
		));

	}

	public function getItemArray() {

		$item = $this->getAttribute('item'); /** @var $item \Bitalo\Market\Logic\Item\Item */

		$images = array();

		foreach($item->getImages() as $image) {
			$images[] = array(
				'200x200' => $image->getThumbnail(200,200),
				'400x400' => $image->getThumbnail(400,400),
				'60x60'   => $image->getThumbnail(60,60)
			);
		}

		$shippingOptions = $item->getShippingOptions();

		return array(
				'id'               => $item->getId(),
				'title'            => $item->getTitle(),
				'description'      => $item->getDescription(),
				'shortDescription' => $item->getShortDescription(),
				'category'         => $item->getCategory()->getId(),
				'images'           => $images,
				'price'            => $item->getPrice(),
				'currency'         => $item->getCurrency(),
				'quantity'         => $item->getAvailableQuantity(),
				'shippingOptions'  => $shippingOptions,
		);
	}


	/**
	 * @param AgaviRequestDataHolder $rd
	 * @return string
	 */
	public function executeAjaxJson(AgaviRequestDataHolder $rd) {


		return json_encode(array(
			'success'    => true,
			'redirect'   => $this->getContext()->getRouting()->gen('listings')
		));
	}
}