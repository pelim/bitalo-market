<?php

use Bitalo\Market\Agavi\View;

class Marketplace_Items_Item_PurchaseSuccessView extends View\MarketplaceBaseView {


	public function executeAjaxJson(AgaviRequestDataHolder $rd) {

		$purchase = $this->getAttribute('purchase');

		return json_encode(array(
			'success' => true,
			'redirect' => $this->getContext()->getRouting()->gen('purchases.purchase', array('purchaseId' => $purchase->getId())),
		));
	}
}