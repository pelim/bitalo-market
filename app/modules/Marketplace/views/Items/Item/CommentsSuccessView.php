<?php

use Bitalo\Market\Agavi\View;

class Marketplace_Items_Item_CommentsSuccessView extends View\MarketplaceBaseView {
	
	/**
	 * @param  AgaviRequestDataHolder $rd
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$item = $this->getAttribute('item');
		$this->getResponse()->setRedirect($this->getContext()->getRouting()->gen('items.item', array('itemId' => $item->getId())));

	}
}
