<?php

use BitaloMarket\Agavi\View;

class Marketplace_Items_Item_DeleteSuccessView extends View\MarketplaceBaseView {
	
	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);
	}
}

?>