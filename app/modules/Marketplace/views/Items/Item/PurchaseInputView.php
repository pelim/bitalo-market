<?php

use Bitalo\Market\Agavi\View;

class Marketplace_Items_Item_PurchaseInputView extends View\MarketplaceBaseView {

	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);

		$countries = array();

		$item = $this->getAttribute('item');

		foreach(\Bitalo\Market\Agavi\Translation\TranslationManager::$DEFAULT_COUNTRIES as $code => $name) {
				$countries[] = ['name' => $name, 'code' => $code];
		}

		$price = array();

		foreach($this->getContext()->getModel('ExchangeRates')->getConfiguredCurrencies() as $currency) {
			$price[strtolower($currency)] = $item->getPriceForCurrency($currency);
		}

		$price['btc'] = $item->getPriceBtc();

		$user = $this->getContext()->getUser()->getUserObject();

		$shippingOptions = $item->getShippingOptions();

		foreach($shippingOptions as $i => &$shippingOption) {
			$shippingOption['id'] = $i;
		}

		$this->setJavascriptAttributes(
			[
				'locale' => [
					'countries' => $countries
				],
				'purchase' => [
					'quantity'  => $rd->getParameter('quantity', 1),
					'firstName' => $user->getFirstName(),
					'lastName'  => $user->getLastName(),
				],
				'item' => [
					'price'    => $price,
					'quantity' => array(
						'min' => 1,
						'max' => $item->getAvailableQuantity()
					),
					'shipping' => [
						'options' => $shippingOptions,
						'regions' => []
					]
				],
				'user'=> [
					'wallet' => [
						'derive' => $this->getContext()->getUser()->getUserObject()->getWalletDerive()
					],
					'firstName' => $user->getFirstName(),
					'lastName' => $user->getLastName()
				]
			]
		);
	}
}