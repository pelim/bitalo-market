<?php

use Bitalo\Market\Agavi\View;

class Marketplace_Items_Item_PurchaseErrorView extends View\MarketplaceBaseView {


	public function executeAjaxJson(AgaviRequestDataHolder $rd) {

		$this->getResponse()->setHttpStatusCode(409);
		return json_encode($this->getAttribute('errors'));

	}
}