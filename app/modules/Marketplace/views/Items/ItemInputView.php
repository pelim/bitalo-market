<?php

use Bitalo\Market\Agavi\View;

class Marketplace_Items_ItemInputView extends View\MarketplaceBaseView {

	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {

		$this->setupHtml($rd);

		if($item = $this->getAttribute('item')) {  /** @var $item \Bitalo\Market\Logic\Item\Item */
			$price = array();

			foreach($this->getContext()->getModel('ExchangeRates')->getConfiguredCurrencies() as $currency) {
				$price[strtolower($currency)] = $item->getPriceForCurrency($currency);
			}

			$price['btc'] = $item->getPriceBtc();

			$images = array();

			foreach($item->getImages() as $image) {
				$images[] =  array('id'=> $image->getId(), 'src' => $image->getThumbnail(200,200));
			}

			$shippingOptions = $item->getShippingOptions();


			$this->setJavascriptAttributes(array(
				'routes' => array(
					'item' => $this->ro->gen('api.items.item.edit', array('itemId' => $item->getId()))
				),
				'item' => array(
					'id'               => $item->getId(),
					'title'            => $item->getTitle(),
					'description'      => $item->getDescription(),
					'shortDescription' => $item->getShortDescription(),
					'category'         => $item->getCategory()->getId(),
					'images'           => array(
						'active'  => $images,
						'deleted' => array(),
						'new'     => array()
					),
					'price'            => $item->getPrice(),
					'currency'         => $item->getCurrency(),
					'quantity'         => $item->getAvailableQuantity(),
					'shippingOptions'  => $shippingOptions,

				)
			));
		} else {
			$this->setJavascriptAttributes(array(
				'routes' => array(
					'item' => $this->ro->gen('api.items.create')
				)
			));
		}

	}


	public function executeAjaxJson(AgaviRequestDataHolder $rd) {


		return json_encode($this->getItemArray());
	}
}