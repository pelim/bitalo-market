<?php

use Bitalo\Market\Agavi\View;

class Marketplace_Items_CreateErrorView extends View\MarketplaceBaseView {
	
	/**
	 * @param  AgaviRequestDataHolder $rd
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {

		/*
		 * @TODO: implement user filter for this case
		 */
		$this->us->setAttribute('redirect', $this->rq->getUrl(), 'org.agavi.bitalo.market.item.create');

		$this->getResponse()->setRedirect($this->getContext()->getRouting()->gen('account.settings'));
	}

}
