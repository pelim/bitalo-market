<?php

use Bitalo\Market\Agavi\View;

class Marketplace_Items_CreateSuccessView extends View\MarketplaceBaseView {
	
	/**
	 * @param AgaviRequestDataHolder $rd
	 * @return mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$item = $this->getAttribute('item');
		$this->getResponse()->setRedirect($this->getContext()->getRouting()->gen('items.item', array('itemId' => $item->getId())));
	}

	/**
	 * @param AgaviRequestDataHolder $rd
	 * @return string
	 */
	public function executeAjaxJson(AgaviRequestDataHolder $rd) {

		$item = $this->getAttribute('item');

		return json_encode(array(
			'success'    => true,
			'redirect'   => $this->getContext()->getRouting()->gen('items.item', array('itemId' => $item->getId()))
		));
	}

}