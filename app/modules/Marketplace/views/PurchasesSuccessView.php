<?php

use Bitalo\Market\Agavi\View;

class Marketplace_PurchasesSuccessView extends View\MarketplaceBaseView {

	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);
	}

}

?>