<?php

use Bitalo\Market\Agavi\Action;
use Bitalo\Market\Logic\Image\Image;


class Marketplace_Items_Item_EditAction extends Action\MarketplaceBaseAction {

	public function executeRead(AgaviRequestDataHolder $rd) {
		$item = $rd->getParameter('item');

		if($item->getUser()->getId() == $this->getAuthenticatedUser()->getId()) {
			$this->setAttribute('item', $item);

			return array('Marketplace', 'Items.ItemInput');

		} else {
			return array('Default', 'Error404Success');

		}
	}

	public function executeWrite(AgaviRequestDataHolder $rd) {

		$item = $rd->getParameter('item'); /** @var $item \Bitalo\Market\Logic\Item\Item */

		if($item->getUser()->getId() == $this->getAuthenticatedUser()->getId()) {

			$item->setTitle($rd->getParameter('title'));
			$item->setDescription($rd->getParameter('description'));


			$item->addQuantity($rd->getParameter('quantity') - $item->getAvailableQuantity());
			$item->setCurrency($rd->getParameter('currency'));
			$item->setPrice($rd->getParameter('price'));
			$item->setCategory($rd->getParameter('category'));
			$item->setShippingOptions($rd->getParameter('shippingOptions'));

			foreach($rd->getFile('images[new]', array()) as $file) {
				$image = Image::createFromUploadedImage($file);
				$item->addImage($image);
			}

			foreach($rd->getParameter('images[deleted]', array()) as $image) {
				$item->removeImage(new Image($image));
			}

			$item->save(true);

			$this->setAttribute('item', $item);

			return array('Marketplace', 'Items.ItemSuccess');

		} else {

			return array('Default', 'Error404Success');
		}



	}
}