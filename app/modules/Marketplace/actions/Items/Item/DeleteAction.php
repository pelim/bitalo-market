<?php

use Bitalo\Market\Agavi\Action;

class Marketplace_Items_Item_DeleteAction extends Action\MarketplaceBaseAction {

	public function executeWrite(AgaviRequestDataHolder $rd) {
		$item = $rd->getParameter('item'); /** @var \Bitalo\Market\Logic\Item\Item $item */

		if($item->getUser()->getId() == $this->getAuthenticatedUser()->getId()) {
			$item->setStatus(\Bitalo\Market\Logic\Item\Item::STATUS_DELETED);
			$item->save();

			return 'Success';
		}

	}
}