<?php

use Bitalo\Market\Agavi\Action;
use Bitalo\Market\Logic\Item\ItemComment;

class Marketplace_Items_Item_CommentsAction extends Action\MarketplaceBaseAction {

	public function executeWrite(AgaviRequestDataHolder $rd) {
		$item = $rd->getParameter('item'); /** @var \Bitalo\Market\Logic\Item\Item $item */

		$comment = ItemComment::createComment($this->getAuthenticatedUser(), $rd->getParameter('comment'));
		$item->addComment($comment);

		$this->setAttribute('item', $item);

		return 'Success';
	}
}
