<?php

use Bitalo\Market\Agavi\Action;

use Bitalo\Market\Agavi\Config\Config as BitaloConfig;
use Bitalo\Market\Logic\Item\ItemPurchase;
use Bitalo\Market\Logic\Payment\PaymentAddress;
use Bitalo\Market\Utility\ExchangeRates;


class Marketplace_Items_Item_PurchaseAction extends Action\MarketplaceBaseAction {

	const DEFAULT_PURCHASE_QUANTITY = 1;
	/**
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return string
	 */
	public function executeRead(AgaviRequestDataHolder $rd) {
		$item = $rd->getParameter('item');

		$this->setAttribute('item', $item);

		return 'Input';
	}

	/**
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return string
	 */
	public function executeWrite(AgaviRequestDataHolder $rd) {
		$item           = $rd->getParameter('item'); /** @var $item \Bitalo\Market\Logic\Item\Item */
		$quantity       = $rd->getParameter('quantity', self::DEFAULT_PURCHASE_QUANTITY);
		$shippingOption = $rd->getParameter('shippingOption');
		$country        = $rd->getParameter('country');

		$user           = $this->getAuthenticatedUser();


		if($item->getAvailableQuantity() >= $quantity) {
			$purchase = new ItemPurchase();

			$purchase->setUser($user);
			$purchase->setItem($item);
			$purchase->setItemVersion($item->getLastVersion());
			$purchase->setStatus($purchase::STATUS_PURCHASED);
			$purchase->setQuantity($quantity);

			$rate = ExchangeRates::getExchangeRate($item->getCurrency());
			$total = ($quantity * $item->getPrice()) + $shippingOption->getPrice();

			$paymentAmount = $total / $rate;

			$purchase->setPaymentAmount(round($paymentAmount, 8));

			$paymentAddress = PaymentAddress::createMultiSignatureAddress(2, array(
				$user, $item->getUser()
			));

			$purchase->setWallet($paymentAddress);

			$purchase->setShippingAddress(array(
				'firstName' => $rd->getParameter('firstName'),
				'lastName'  => $rd->getParameter('lastName'),
				'address'   => $rd->getParameter('address'),
				'city'      => $rd->getParameter('city'),
				'zip'       => $rd->getParameter('zip'),
				'country'   => $country['code']
			));

			$purchase->setShippingOption($shippingOption->getId());

			$purchase->save();

			$eventManager = $this->getEventManager(); /** @var $eventManager \EventManagerModel */
			$eventManager->fire($user, ItemPurchase::EVENT_ITEM_PURCHASED, $user, $purchase);

			// subscribe to payment address
			$this->getPaymentManger()->subscribeAddress($paymentAddress->getAddress());

			$user->incrementWalletDerive();
			$item->getUser()->incrementWalletDerive();

			$this->setAttribute('purchase', $purchase);

			return 'Success';
		} else {

			$this->setAttribute('errors', array('errors' => ['quantity' => [null]]));
			return 'Error';
		}


	}

}