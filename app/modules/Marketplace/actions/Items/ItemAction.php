<?php

use Bitalo\Market\Agavi\Action;

class Marketplace_Items_ItemAction extends Action\MarketplaceBaseAction {


	public function executeRead(AgaviRequestDataHolder $rd) {
		$item = $rd->getParameter('item');

		$this->setAttribute('item', $item);

		return 'Success';
	}

	public function isSecure() {
		return false;
	}

}