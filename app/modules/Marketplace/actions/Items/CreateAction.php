<?php

use Bitalo\Market\Agavi\Action;
use Bitalo\Market\Logic\Item\Item;
use Bitalo\Market\Logic\Image\Image;

class Marketplace_Items_CreateAction extends Action\MarketplaceBaseAction {

	public function executeRead(AgaviRequestDataHolder $rd) {

		if(!$this->getAuthenticatedUser()->getPayoutAddress()) {
			return 'Error';
		} else {
			return array('Marketplace', 'Items.ItemInput');
		}
	}

	public function isJavascriptValidated() {
		return true;
	}

	public function executeWrite(AgaviRequestDataHolder $rd) {

		$item = Item::createItemForUser($this->getContext()->getUser()->getUserObject());

		$item->setTitle($rd->getParameter('title'));
		$item->setDescription($rd->getParameter('description'));
		$item->setQuantity($rd->getParameter('quantity'));
		$item->setCurrency($rd->getParameter('currency'));
		$item->setPrice($rd->getParameter('price'));
		$item->setCategory($rd->getParameter('category'));
		$item->setShippingOptions($rd->getParameter('shippingOptions'));

		foreach($rd->getFile('images[new]', array()) as $file) {
			$image = Image::createFromUploadedImage($file);
			$item->addImage($image);
		}

		$item->save(true);

		$this->setAttribute('item', $item);

		return 'Success';
	}
}