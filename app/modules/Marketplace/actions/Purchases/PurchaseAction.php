<?php

use Bitalo\Market\Agavi\Action;

use Bitalo\Market\Logic\Payment\PaymentTransactionFinder;
use BitWasp\BitcoinLib\BIP32;

class Marketplace_Purchases_PurchaseAction extends Action\MarketplaceBaseAction {


	public function executeRead(AgaviRequestDataHolder $rd) {

		$purchase = $rd->getParameter('purchase'); /** @var $purchase \Bitalo\Market\Logic\Item\ItemPurchase */
		$item     = $purchase->getItem();
		$vendor   = $item->getUser(); /** @var $vendor \Bitalo\Market\Logic\User\User */

		$this->setAttribute('purchase', $purchase);


		if($purchase->getStatus() == $purchase::STATUS_PAYED || $purchase->getStatus() == $purchase::STATUS_SHIPPED) {

			if($outputTransaction = $purchase->getWallet()->getOutputTransaction()) {
				$this->setAttribute('transaction', $outputTransaction);
				$this->setJavascriptAttribute('transaction', json_decode($outputTransaction));

			} else {
				$this->setJavascriptAttribute('transaction',
					$this->getPaymentManger()->createTransactionFromWalletAddress(
						$purchase->getWallet()->getAddress(), [
						['address' => $vendor->getPayoutAddress(), 'amount' => $purchase->getWallet()->getBalance() - 0.001]
					])
				);
			}

			$attributes = $purchase->getWallet()->getAdditionalAttributes();

			$scriptData = [
				'script' => $attributes['script'],
				'address' => $purchase->getWallet()->getAddress()
			];

			foreach($attributes['keys'] as $key) {
				if(isset($key['user']) && $key['user'] == $this->getAuthenticatedUser()->getId()) {
					$scriptData['derive'] = $key['derive'];
				}
			}

			$this->setJavascriptAttribute('script', $scriptData);

		} elseif($purchase->getStatus() == $purchase::STATUS_PAYMENT_AWAITING) {
			/*if($address = $purchase->getWallet()->getAddress()) {
				$this->getContext()->getModel('TaskManager')->schedule('fetch.address.balance',  array('address' => $address), TaskManagerModel::PRIORITY_HIGH);
			}*/
		}
		return 'Success';
	}

}