<?php

use Bitalo\Market\Agavi\Action;

class Marketplace_Purchases_Purchase_ProcessAction extends Action\MarketplaceBaseAction {


	public function executeWrite(AgaviRequestDataHolder $rd) {

		$purchase = $rd->getParameter('purchase'); /** @var $purchase \Bitalo\Market\Logic\Item\ItemPurchase */

		if($purchase->processStep($this->getContext()->getUser()->getUserObject())) {

			if($outputTransaction = $rd->getParameter('output_transaction')) {
				$purchase->getWallet()->setOutputTransaction($outputTransaction);

				if($rd->getParameter('fully_signed')) {

					$transactionObject = json_decode($outputTransaction);

					$this->getPaymentManger()->sendTransaction($transactionObject->tx);
				}
			}

			$this->setAttribute('purchase', $purchase);

			return 'Success';
		} else {
			return 'Error';
		}

	}

}