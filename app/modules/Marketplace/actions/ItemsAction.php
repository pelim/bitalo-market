<?php

use Bitalo\Market\Agavi\Action;

use Bitalo\Market\Logic\Item\ItemFinder;
use Bitalo\Market\Logic\Object\BaseFinder;
use Bitalo\Market\Logic\Payment\PaymentAddressFinder;


class Marketplace_ItemsAction extends Action\MarketplaceBaseAction {

	public function executeRead(AgaviRequestDataHolder $rd) {

		$category = $rd->getParameter('category');
		$order    = $rd->getParameter('order', ItemFinder::ORDER_BY_CREATION);
		$sort     = $rd->getParameter('sort', BaseFinder::SORT_ASC);
		$offset   = $rd->getParameter('offset',  BaseFinder::DEFAULT_OFFSET);
		$limit    = $rd->getParameter('limit', BaseFinder::DEFAULT_LIMIT);

		if($category) {
			$items = ItemFinder::findActiveItemsForCategoryOrderBy($category, $order, $sort, $offset, $limit);
		} else {
			$items = ItemFinder::findActiveItemsOrderBy($order, $sort, $offset, $limit);
		}

		$this->setAttribute('items', $items);

		return 'Success';
	}

	public function isSecure() {
		return false;
	}
}