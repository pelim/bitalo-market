<?php

use Bitalo\Market\Agavi\Action;

use Bitalo\Market\Logic\Item\ItemPurchaseFinder;

class Marketplace_PurchasesAction extends Action\MarketplaceBaseAction {

	const FILTER_ORDERS   = 'orders';

	const FILTER          = 'purchases';

	const FILTER_ARCHIVED = 'archived';

	public function executeRead(AgaviRequestDataHolder $rd) {
		$offset = $rd->getParameter('offset', ItemPurchaseFinder::DEFAULT_OFFSET);
		$limit  = $rd->getParameter('limit', ItemPurchaseFinder::DEFAULT_LIMIT);

		$filter = $rd->getParameter('filter', self::FILTER_ORDERS);

		if($filter == 'orders') {
			//$purchases = ItemPurchaseFinder::findActivePurchasesForVendor($this->getAuthenticatedUser(), $offset, $limit);
			$purchases = ItemPurchaseFinder::findPurchasesForItemUser($this->getAuthenticatedUser());

		} elseif($filter == 'purchases') {
			//$purchases = ItemPurchaseFinder::findActivePurchasesForUser($this->getAuthenticatedUser(), $offset, $limit);
			$purchases = ItemPurchaseFinder::findPurchasesForUser($this->getAuthenticatedUser());
		} elseif($filter == 'archived') {
			$purchases = array();
		}

		$this->setAttribute('purchases', $purchases);

		return 'Success';
	}
}