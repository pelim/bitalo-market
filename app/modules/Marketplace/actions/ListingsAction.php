<?php

use Bitalo\Market\Agavi\Action;

use Bitalo\Market\Logic\Item\ItemFinder;

class Marketplace_ListingsAction extends Action\MarketplaceBaseAction {

	public function executeRead(AgaviRequestDataHolder $rd) {

		$items = ItemFinder::findItemsForUser($this->getContext()->getUser()->getUserObject());

		$this->setAttribute('listings', $items);

		return 'Success';
	}
}