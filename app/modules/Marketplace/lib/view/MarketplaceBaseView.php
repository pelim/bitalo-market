<?php

namespace Bitalo\Market\Agavi\View;

use Bitalo\Market\Logic\Item\ItemPurchase;

/**
 * The base view from which all Marketplace module views inherit.
 */
class MarketplaceBaseView extends BaseView {

	public function getProgressForStatus($status) {

		if($status == ItemPurchase::STATUS_PURCHASED) {
			return array('class' => 'progress-bar-danger', 'percent' => 15, 'description' => 'waiting for payment');
		}

		if($status == ItemPurchase::STATUS_PAYED) {
			return array('class' => 'progress-bar-info', 'percent' => 50, 'description' => 'payment verified, waiting for seller confirmation');
		}


		if($status == ItemPurchase::STATUS_SHIPPED) {
			return array('class' => 'progress-bar-warning', 'percent' => 75, 'description' => 'order shipped, waiting for delivery');

		}

		if($status == ItemPurchase::STATUS_RECEIVED) {
			return array('class' => 'progress-bar-success', 'percent' => 100, 'description' => 'order received');

		}

		return 0;
	}
}

?>