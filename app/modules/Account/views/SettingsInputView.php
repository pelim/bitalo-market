<?php

use Bitalo\Market\Agavi\View;

class Account_SettingsInputView extends View\AccountBaseView {

	/**
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);
	}
}
