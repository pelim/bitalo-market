<?php

use Bitalo\Market\Agavi\View;

class Account_LoginInputView extends View\AccountBaseView {

	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);

		if($this->getContainer()->hasAttributeNamespace('org.agavi.controller.forwards.login')) {
			// we were redirected to the login form by the controller because the requested action required security
			// so store the input URL in the session for a redirect after login
			$this->us->setAttribute('redirect', $this->rq->getUrl(), 'org.agavi.bitalo.market.login');
		} else {
			// clear the redirect URL just to be sure
			// but only if request method is "read", i.e. if the login form is served via GET!
			$this->us->removeAttribute('redirect', 'org.agavi.bitalo.market.login');
		}
	}
}