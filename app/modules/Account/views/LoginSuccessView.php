<?php

use Bitalo\Market\Agavi\View;

class Account_LoginSuccessView extends View\AccountBaseView {

	/**
	 * @param AgaviRequestDataHolder $rd
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->getResponse()->setRedirect($this->getRedirect());
	}

	/**
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return string
	 */
	public function executeAjaxJson(AgaviRequestDataHolder $rd) {

		$user =  $this->us->getUserObject();

		return json_encode(array(
			'success'    => true,
			'redirect'   => $this->getRedirect(),
			'master_key' => $user->getMasterKey(),
			'public_key' => $user->getPublicKey()
		));
	}
}