<?php

use Bitalo\Market\Agavi\View;

class Account_LogoutSuccessView extends View\AccountBaseView {

	/**
	 * @param AgaviRequestDataHolder $rd
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->getResponse()->setRedirect($this->getContext()->getRouting()->gen('items'));

	}
}