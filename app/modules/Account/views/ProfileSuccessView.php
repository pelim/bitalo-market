<?php

use Bitalo\Market\Agavi\View;

class Account_ProfileSuccessView extends View\AccountBaseView {

	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);
	}

}

?>