<?php

use Bitalo\Market\Agavi\View;

class Account_RegisterInputView extends View\AccountBaseView {
	
	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);
	}
}