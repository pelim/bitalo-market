<?php

use Bitalo\Market\Agavi\View;

class Account_RegisterSuccessView extends View\AccountBaseView {

	/**
	 * @param AgaviRequestDataHolder $rd
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->getResponse()->setRedirect($this->getRedirect());
	}

	public function executeAjaxJson(AgaviRequestDataHolder $rd) {
		return json_encode(array(
			'success' => true,
			'next'    => $this->getRedirect()
		));
	}

}