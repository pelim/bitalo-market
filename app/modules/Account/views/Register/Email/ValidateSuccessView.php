<?php

use Bitalo\Market\Agavi\View;

class Account_Register_Email_ValidateSuccessView extends View\AccountBaseView {
	
	/**
	 * @param  AgaviRequestDataHolder $rd
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);
	}
}

?>