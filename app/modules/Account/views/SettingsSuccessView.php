<?php

use Bitalo\Market\Agavi\View;

class Account_SettingsSuccessView extends View\AccountBaseView {

	/**
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {

		if($this->us->hasAttribute('redirect', 'org.agavi.bitalo.market.item.create')) {
			$this->getResponse()->setRedirect($this->us->removeAttribute('redirect', 'org.agavi.bitalo.market.item.create'));
		} else {
			$this->getResponse()->setRedirect($this->ro->gen('index'));
		}
	}
}