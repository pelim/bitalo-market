<?php

use Bitalo\Market\Agavi\Action;
use Bitalo\Market\Logic\User\User;
use Bitalo\Market\Logic\User\Token;

class Account_RegisterAction extends Action\AccountBaseAction {

	public function executeWrite(AgaviRequestDataHolder $rd) {

		$sessionUser = $this->getContext()->getUser(); /** @var $sessionUser \Bitalo\Market\Agavi\User\SessionUser  */

		$user = new User();

		$user->setFirstName($rd->getParameter('firstName'));
		$user->setLastName($rd->getParameter('lastName'));
		$user->setNick($rd->getParameter('nick'));
		$user->setEmail($rd->getParameter('email'));
		$user->setPassword($sessionUser->hashPassword($rd->getParameter('password')));
		$user->setPublicKey($rd->getParameter('publicKey'));
		$user->setMasterKey($rd->getParameter('masterKey'));
		$user->setCreatedAt(new \DateTime());
		$user->setModifiedAt(new \DateTime());
		$user->setStatus(User::STATUS_EMAIL_VALIDATION_REQUIRED);

		$user->save();

		$token = Token::createToken($user, Token::TOKEN_TYPE_EMAIL);

		$this->getNotificationManager()->enqueueEmail($user, 'user.account.validate.email', array('user' => $user, 'token' => $token));


		$sessionUser->loginAs($user);

		return 'Success';
	}

	public function isJavascriptValidated() {
		return true;
	}

	public function isSecure() {
		return false;
	}
}