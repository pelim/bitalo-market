<?php

use Bitalo\Market\Agavi\Action;
use Bitalo\Market\Logic\User\User;

class Account_Register_Email_ValidateAction extends Action\AccountBaseAction {

	public function executeRead(AgaviRequestDataHolder $rd) {
		$token = $rd->getParameter('token'); /** @var $token \Bitalo\Market\Logic\User\Token */
		$user  = $token->getUser();

		if($token->process($user)) {

			$user->setStatus(User::STATUS_ACTIVE);
			$user->save();
			return 'Success';
		} else {
			return 'Error';
		}

	}

	public function isSecure() {
		return false;
	}

}