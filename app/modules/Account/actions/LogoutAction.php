<?php

use Bitalo\Market\Agavi\Action;

class Account_LogoutAction extends Action\AccountBaseAction {

	public function executeRead(AgaviRequestDataHolder $rd) {
		$this->getContext()->getUser()->logout();
		return 'Success';
	}
}