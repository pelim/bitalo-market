<?php

use Bitalo\Market\Agavi\Action;
use Bitalo\Market\Logic\User\Token;

class Account_SettingsAction extends Action\AccountBaseAction {

	public function executeRead(AgaviRequestDataHolder $rd) {
		$this->setAttribute('user', $this->getAuthenticatedUser());

		return 'Input';
	}

	public function executeWrite(AgaviRequestDataHolder $rd) {

		$user = $this->getAuthenticatedUser();
		if($payoutAddress = $rd->getParameter('payoutAddress')) {
			$user->setPayoutAddress($payoutAddress);
		}

		if($firstName = $rd->getParameter('firstName')) {
			$user->setFirstName($firstName);
		}

		if($lastName = $rd->getParameter('lastName')) {
			$user->setLastName($lastName);
		}

		if($nick = $rd->getParameter('nick')) {
			$user->setNick($nick);
		}

		$user->save();

		return 'Success';
	}
}