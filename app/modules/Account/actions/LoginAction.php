<?php

use Bitalo\Market\Agavi\Action;

class Account_LoginAction extends Action\AccountBaseAction {

	public function executeWrite(AgaviRequestDataHolder $rd) {
		$user = $rd->getParameter('user');
		if($user) {
			$this->getContext()->getUser()->loginAs($user);
			return 'Success';
		}
	}

	public function isSecure() {
		return false;
	}

}