<?php

use Bitalo\Market\Agavi\Action;
use Bitalo\Market\Logic\User\Token;

class Account_ProfileAction extends Action\AccountBaseAction {

	public function executeRead(AgaviRequestDataHolder $rd) {
		$this->setAttribute('user', $this->getAuthenticatedUser());

		return 'Success';
	}
}