<?php

namespace Bitalo\Market\Agavi\View;

/**
 * The base view from which all Account module views inherit.
 */
class AccountBaseView extends BaseView {


	protected function getRedirect() {
		return $this->ro->gen('index');

/*		if($this->us->hasAttribute('redirect', 'org.agavi.bitalo.market.login')) {
			return $this->us->removeAttribute('redirect', 'org.agavi.bitalo.market.login');
		} else {
			return $this->ro->gen('index');
		}
*/
	}
}

?>