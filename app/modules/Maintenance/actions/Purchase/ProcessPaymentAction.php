<?php

use Bitalo\Market\Agavi\Action;

use Bitalo\Market\Logic\Item\ItemPurchase;

class Maintenance_Purchase_ProcessPaymentAction extends Action\MaintenanceBaseAction {

	public function execute(AgaviRequestDataHolder $rd) {
		$purchase = $rd->getParameter('purchase'); /** @var $purchase \Bitalo\Market\Logic\Item\ItemPurchase */

		if($purchase->getNextProcessStep() == ItemPurchase::STATUS_PAYED) {
			$purchase->setStatus(ItemPurchase::STATUS_PAYED);
			if($purchase->save()) {
				echo sprintf('ItemPurchase #%s marked as payed', $purchase->getId()) . "\n";
			}
		}
	}
}