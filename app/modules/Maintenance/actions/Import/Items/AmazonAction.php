<?php

use Bitalo\Market\Agavi\Action\MaintenanceBaseAction;


use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Search;
use ApaiIO\ApaiIO;


class Maintenance_Import_Items_AmazonAction extends MaintenanceBaseAction {

	public function execute(AgaviRequestDataHolder $rd) {


		$conf = new GenericConfiguration();
		$conf->setCountry('de')
			->setAccessKey(\BitaloConfig::get('services.amazon.api_key'))
			->setSecretKey(\BitaloConfig::get('services.amazon.api_secret'))
			->setRequest('\ApaiIO\Request\Soap\Request')
			->setAssociateTag('bitamark-20');

		$next = true;

		$api = new ApaiIO($conf);

		$actualPage = 1;

		while($next) {
			$search = new Search();
			$search->setCategory('All');
			$search->setKeywords('tag heuer');
			$search->setResponseGroup(array('Images', 'ItemAttributes', 'OfferSummary', 'EditorialReview'));
			$search->setPage($actualPage);

			$formattedResponse = $api->runOperation($search);

			$em = AgaviContext::getInstance()->getDatabaseConnection(); /** @var $em \Doctrine\ORM\EntityManager */

			foreach($formattedResponse->Items->Item as $item) {

				$user = $em->getRepository('\Bitalo\Market\Orm\User')->findOneByEmail('peter.limbach@gmail.com');
				if(isset($item->EditorialReviews) && isset($item->OfferSummary->LowestNewPrice)) {

					$bitaloItem = new \Bitalo\Market\Orm\Item();

					$bitaloItem->setTitle(substr($item->ItemAttributes->Title, 0, 50));
					$bitaloItem->setDescription('');
					$bitaloItem->setPrice($item->OfferSummary->LowestNewPrice->Amount / 100);
					$bitaloItem->setCurrency('EUR');
					$bitaloItem->setStatus(0);
					$bitaloItem->setQuantity(1);
					$bitaloItem->setUser($user);

					$em->persist($bitaloItem);

					$bitaloImage = new \Bitalo\Market\Orm\Image();
					$bitaloImage->setHeight($item->LargeImage->Height->_);
					$bitaloImage->setWidth($item->LargeImage->Width->_);
					$bitaloImage->setImageUrl($item->LargeImage->URL);
					$bitaloImage->setCaption('');
					$bitaloItem->addImage($bitaloImage);

					$em->persist($bitaloImage);
					$em->flush();

					echo $item->ItemAttributes->Title . "\n";
				}
			}

			if(!isset($formattedResponse->Items->TotalPages) || $formattedResponse->Items->TotalPages == $actualPage || $actualPage == 3) {
				$next = false;
			} else {
				$actualPage++;
			}
		}

	}
}