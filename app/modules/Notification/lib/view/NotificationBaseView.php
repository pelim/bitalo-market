<?php

namespace Bitalo\Market\Agavi\View;

use Bitalo\Market\Agavi\Config\Config;
use Bitalo\Market\Logic\User\Message;
use Bitalo\Market\Logic\User\User;
use AgaviRequestDataHolder;
use DateTime;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;

/**
 * The base view from which all Notification module views inherit.
 */
class NotificationBaseView extends BaseView {

	protected function formatMessageArray($arrayOrObject) {

		if(is_array($arrayOrObject)) {
			$messagesArray = array();

			foreach($arrayOrObject as $message) {
				$messagesArray[] = $this->formatMessageArray($message);
			}

			return $messagesArray;
		} elseif($arrayOrObject instanceof Message) {
			$message = $arrayOrObject;
			return array(
				'user' => array(
					'id' => $message->getUser()->getId(),
					'firstName' => $message->getUser()->getFirstName(),
					'lastName'  => $message->getUser()->getLastName(),
				),
				'body'     => $message->getBody(),
				'creation' => $message->getCreatedAt()->format(DateTime::ISO8601)
			);
		} else {
			throw new InvalidArgumentException('awaiting Array or Object of type Bitalo\Market\Logic\User\Message');
		}


	}

	/**
	 * @param \AgaviRequestDataHolder $rd
	 * @param null $layoutName
	 */
	public function setupNotification(AgaviRequestDataHolder $rd, $layoutName = null) {
		$this->loadLayout($layoutName);
	}

	/**
	 * @param \AgaviRequestDataHolder $rd
	 */
	public function executeNotification(AgaviRequestDataHolder $rd) {
		$this->setupNotification($rd, 'email');

		// set default sender
		$this->setSender(Config::get('mail.sender.address'), Config::get('mail.sender.name'));

		// set recipients
		if($rd->getParameter('user')) {
			$this->setRecipientFromUser($rd->getParameter('user'));
		} else if($rd->getParameter('email')) {
			$this->setRecipient($rd->getParameter('email'));
		}

		$this->getLayer('content')->setTemplate('Email/Render/' . $rd->getParameter('template'));


		$this->setAttribute('template', $rd->getParameter('template'));
	}


	/**
	 * @param $subject
	 */
	public function setSubject($subject) {
		$this->setAttribute('subject', $subject);
	}

	/**
	 * @param string $email
	 * @param string $name
	 */
	public function setRecipient($email, $name = null) {
		$this->setAttribute('recipientEmail', $email);
		if($name) {
			$this->setAttribute('recipientName',  $name);
		}
	}

	public function setRecipientFromUser(User $user) {
		$this->setRecipient($user->getEmail(), $user->getFullName());
	}

	/**
	 * @param string $email
	 * @param string $name
	 */
	public function setSender($email, $name) {
		$this->setAttribute('senderEmail', $email);
		$this->setAttribute('senderName',  $name);
	}

	/**
	 * @param string $header
	 */
	public function setHeader($header) {
		$this->setAttribute('header', $header);
	}

	/**
	 * @param string $content
	 */
	public function setTextContent($content) {
		$this->setAttribute('textContent', $content);
	}

}