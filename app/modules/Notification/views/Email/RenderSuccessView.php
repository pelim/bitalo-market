<?php

use Bitalo\Market\Agavi\View;

class Notification_Email_RenderSuccessView extends View\NotificationBaseView {

	public function executeNotification(AgaviRequestDataHolder $rd) {
		parent::executeNotification($rd);
	}
}