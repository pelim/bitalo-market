<?php

use Bitalo\Market\Agavi\View;

class Notification_ConversationsSuccessView extends View\NotificationBaseView {
	
	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);


	}

	public function executeAjaxJson(AgaviRequestDataHolder $rd) {

		return json_encode($this->getConversationArray());

	}

	public function getConversationArray() {
		$conversations = $this->getAttribute('conversations');

		$conversationsArray = array();

		foreach($conversations as $conversationObject) {
			$lastMassage = $conversationObject->getLastMessage();
			$id = $conversationObject->getId();
			$conversation = array(
				'id' => $id,
				'user' => array(
					'id' => $lastMassage->getUser()->getId(),
					'firstName' => $lastMassage->getUser()->getFirstName(),
					'lastName'  => $lastMassage->getUser()->getLastName(),
				),
				'messages' => $this->formatMessageArray($conversationObject->getMessages()),
				'message'  => $lastMassage->getBody()
			);

			$conversationsArray[] = $conversation;
		}

		return $conversationsArray;
	}
}