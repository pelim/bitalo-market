<?php

use Bitalo\Market\Agavi\View;

class Notification_Conversations_CreateSuccessView extends View\NotificationBaseView {

	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return     mixed
	 */
	public function executeAjaxJson(AgaviRequestDataHolder $rd) {
		$conversation = $this->getAttribute('conversation');

		return json_encode(array(
			'id'       => $conversation->getId(),
			'messages' => $this->formatMessageArray($conversation->getMessages()),
		));
	}
}