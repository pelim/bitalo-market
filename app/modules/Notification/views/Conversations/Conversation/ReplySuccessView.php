<?php

use Bitalo\Market\Agavi\View;

class Notification_Conversations_Conversation_ReplySuccessView extends View\NotificationBaseView {
	
	/**
	 * @param  AgaviRequestDataHolder $rd
	 * @return     mixed
	 */
	public function executeAjaxJson(AgaviRequestDataHolder $rd) {

		$message = $this->getAttribute('message');
		
		return json_encode($this->formatMessageArray($message));

	}
}