<?php

use Bitalo\Market\Agavi\View;

class Notification_Conversations_ConversationSuccessView extends View\NotificationBaseView {

	public function executeAjaxJson(AgaviRequestDataHolder $rd) {

		$conversation = $this->getAttribute('conversation');

		return json_encode(array(
			'id'       => $conversation->getId(),
			'messages' => $this->formatMessageArray($conversation->getMessages()),
		));
	}
}