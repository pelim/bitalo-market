<?php

use Bitalo\Market\Agavi\Action;

class Notification_Email_RenderAction extends Action\NotificationBaseAction {

	public function execute(AgaviRequestDataHolder $rd) {
		return 'Success';
	}

	public function isSecure() {
		return false;
	}
}