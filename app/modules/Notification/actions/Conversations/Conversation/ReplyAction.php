<?php

use Bitalo\Market\Agavi\Action;
use Bitalo\Market\Logic\User\Message;

class Notification_Conversations_Conversation_ReplyAction extends Action\NotificationBaseAction {

	public function executeWrite(AgaviRequestDataHolder $rd) {
		/* $conversation = $rd->getParameter('conversations'); /** @var $conversation \Bitalo\Market\Logic\User\Conversation */
		$conversation = $rd->getParameter('conversationId');
		$conversation = new \Bitalo\Market\Logic\User\Conversation($conversation);

		$message = new Message();
		$message->setUser($this->getAuthenticatedUser());
		$message->setBody($rd->getParameter('body'));
		$message->setCreatedAt(new DateTime());

		$conversation->appendMessage($message);

		$this->setAttribute('message', $message);
		$this->setAttribute('conversation', $conversation);
		return 'Success';
	}
}