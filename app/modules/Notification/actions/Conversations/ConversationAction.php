<?php

use Bitalo\Market\Agavi\Action;

class Notification_Conversations_ConversationAction extends Action\NotificationBaseAction {

	public function executeRead(AgaviRequestDataHolder $rd) {

		/* $conversation = $rd->getParameter('conversations'); /** @var $conversation \Bitalo\Market\Logic\User\Conversation */
		$conversation = $rd->getParameter('conversationId');
		$conversation = new \Bitalo\Market\Logic\User\Conversation($conversation);
		$this->setAttribute('conversation', $conversation);
		return 'Success';
	}
}