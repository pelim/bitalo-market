<?php

use Bitalo\Market\Agavi\Action;

class Notification_Conversations_CreateAction extends Action\NotificationBaseAction {

	public function executeWrite(AgaviRequestDataHolder $rd) {
		$toUser   = $rd->getParameter('user'); /** @var \Bitalo\Market\Logic\User\User  $toUser */
		$fromUser = $this->getAuthenticatedUser();

		if($toUser->getId() !== $fromUser->getId()) {
			$message = $rd->getParameter('message');

			$conversation = $toUser->startConversation($fromUser, $message['body']);

			$this->setAttribute('conversation', $conversation);

			return 'Success';
		} else {
			return 'Error';
		}

	}
}