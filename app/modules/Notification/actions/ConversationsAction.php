<?php

use Bitalo\Market\Agavi\Action;
use Bitalo\Market\Logic\User\Token;

class Notification_ConversationsAction extends Action\NotificationBaseAction {

	public function executeRead(AgaviRequestDataHolder $rd) {

		$this->setAttribute('conversations', $this->getAuthenticatedUser()->getConversations());

		return 'Success';
	}

}