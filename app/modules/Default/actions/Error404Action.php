<?php

use Bitalo\Market\Agavi\Action;

class Default_Error404Action extends Action\DefaultBaseAction {

	public function getDefaultViewName() {
		return 'Success';
	}
}