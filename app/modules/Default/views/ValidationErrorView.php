<?php

use Bitalo\Market\Agavi\View;

class Default_ValidationErrorView extends View\DefaultBaseView {


	/**
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return string
	 */
	public function executeAjaxJson(AgaviRequestDataHolder $rd) {
		$this->getResponse()->setHttpStatusCode(409);
		return json_encode($this->getErrorMessages());
	}

	/**
	 * @param AgaviRequestDataHolder $rd
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		// @hack: display validation errors with var_dump
		if(strpos(AgaviConfig::get('core.environment'), 'development') !== false) {
			var_dump($this->getErrorMessages());
		}
	}

	/**
	 * @return array
	 */
	public function getErrorMessages() {
		$report = $this->getContainer()->getValidationManager()->getReport();
		$errors = $report->byMinSeverity(\AgaviValidator::ERROR)->getErrors();
		$errorStack = array();
		foreach($errors as $error) {
			if(!$name = $error->getName()) {
				$name = 'required';
			}
			foreach($error->getFields() as $field) {
				$errorStack[$field] = $name;
			};
		}
		return array('errors' => $errorStack);
	}
}