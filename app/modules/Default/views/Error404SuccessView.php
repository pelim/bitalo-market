<?php

use Bitalo\Market\Agavi\View;

class Default_Error404SuccessView extends View\DefaultBaseView {

	/**
	 * @parameter  AgaviRequestDataHolder $rd
	 * @param AgaviRequestDataHolder $rd
	 *
	 * @return     mixed
	 */
	public function executeHtml(AgaviRequestDataHolder $rd) {
		$this->setupHtml($rd);
	}

	public function executeAjaxJson(AgaviRequestDataHolder $rd) {

		return json_encode(array('success' => false));
	}

	public function isSecure() {
		return false;
	}
}